describe('TimeSpecConverter - other time specification', () => {
  // ---------------------------------------------------------------------------
  it('should parse other time specification - good weather', () => {
    const timeSpec = require('../assets/time-spec/other-time-spec/00.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec, 'cs');
    result.start = result.start.toString();

    const expected = {
      'start': OFNCAL.TimeSpecConverter.INITIAL_DATE.toString(),
      'desc': ['Platí za dobrého počasí.'],
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  it('should parse other time specification - bad weather', () => {
    const timeSpec = require('../assets/time-spec/other-time-spec/01.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec, 'cs');
    result.start = result.start.toString();

    const expected = {
      'start': OFNCAL.TimeSpecConverter.INITIAL_DATE.toString(),
      'desc': ['Platí za špatného počasí.'],
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  it('should parse other time specification - default lang', () => {
    const timeSpec = require('../assets/time-spec/other-time-spec/00.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec, 'de');
    result.start = result.start.toString();

    const expected = {
      'start': OFNCAL.TimeSpecConverter.INITIAL_DATE.toString(),
      'desc': ['Valid in good weather.'],
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  it('should reject other time specification - invalid value', () => {
    const timeSpec = require('../assets/time-spec/other-time-spec/02.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  // ---------------------------------------------------------------------------
  it('should parse other time specification and interval', () => {
    const timeSpec = require('../assets/time-spec/other-time-spec/03.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec, 'cs');
    result.start = result.start.toString();
    result.end = result.end.toString();

    const interval = timeSpec['časový_interval'];
    const start = parseDateTime(interval['začátek']['datum']);
    const end = parseDateTime(interval['konec']['datum']);
    const expected = {
      'start': start.toString(),
      'end': end.toString(),
      'desc': ['Platí za dobrého počasí.'],
    };
    assert.deepEqual(result, objectToEvent(expected));
  });
});
