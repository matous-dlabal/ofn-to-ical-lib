describe('TimeSpecConverter - number of repeats', () => {
  // ---------------------------------------------------------------------------
  it('should parse 3x repeat', () => {
    const timeSpec = require('../assets/time-spec/number-of-repeats/00.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec, 'cs');
    result.start = result.start.toString();

    const expected = {
      'start': OFNCAL.TimeSpecConverter.INITIAL_DATE.toString(),
      'desc': ['Událost se uskuteční 3x během vyznačené doby.'],
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  it('should parse 3x repeat (default lang)', () => {
    const timeSpec = require('../assets/time-spec/number-of-repeats/00.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();

    const expected = {
      'start': OFNCAL.TimeSpecConverter.INITIAL_DATE.toString(),
      'desc': ['The event takes place 3 times during the indicated time.'],
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse interval and 11x repeat (en)', () => {
    const timeSpec = require('../assets/time-spec/number-of-repeats/01.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec, 'en');
    result.start = result.start.toString();
    result.end = result.end.toString();

    const interval = timeSpec['časový_interval'];
    const start = parseDateTime(interval['začátek']['datum']);
    const end = parseDateTime(interval['konec']['datum']);
    const expected = {
      'start': start.toString(),
      'end': end.toString(),
      'desc': ['The event takes place 11 times during the indicated time.'],
    };
    assert.deepEqual(result, objectToEvent(expected));
  });
});
