describe('TimeSpecConverter - specific frequency', () => {
  it('should parse empty specific frequency', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/00.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();

    const start = timeSpec['časový_okamžik']['datum'];
    const expected = {
      'start': start,
      'end': start,
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should reject specific frequency - minute', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/min/00.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  it('should reject specific frequency - minute', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/min/01.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  it('should parse specific frequency - minute', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/min/02.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();

    const expected = {
      'start': `${OFNCAL.TimeSpecConverter.INITIAL_DATE.toString()}T00:00:00`,
      'recur': {
        'freq': 'DAILY',
        'byminute': [0],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  it('should parse specific frequency - minute', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/min/03.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();

    const expected = {
      'start': `${OFNCAL.TimeSpecConverter.INITIAL_DATE.toString()}T00:00:00`,
      'recur': {
        'freq': 'DAILY',
        'byminute': [59],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  it('should parse date interval and specific frequency - minute', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/min/04_d.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.recur.until = result.recur.until.toString();

    const expected = {
      'start': '2021-01-01T00:00:00',
      'recur': {
        'freq': 'DAILY',
        'until': '2021-01-31T23:59:59',
        'byminute': [0, 1, 2, 3, 4],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  it('should parse datetime interval and specific frequency - minute', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/min/04_dt.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.recur.until = result.recur.until.toString();

    const expected = {
      'start': '2021-01-01T12:00:00',
      'recur': {
        'freq': 'DAILY',
        'until': '2021-01-31T20:30:00',
        'byminute': [0, 1, 2, 3, 4],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should reject specific frequency - hour', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/hour/00.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  it('should reject specific frequency - hour', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/hour/01.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  it('should parse specific frequency - hour', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/hour/02.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();

    const expected = {
      'start': `${OFNCAL.TimeSpecConverter.INITIAL_DATE.toString()}T00:00:00`,
      'recur': {
        'freq': 'DAILY',
        'byhour': [0],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  it('should parse specific frequency - hour', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/hour/03.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();

    const expected = {
      'start': `${OFNCAL.TimeSpecConverter.INITIAL_DATE.toString()}T00:00:00`,
      'recur': {
        'freq': 'DAILY',
        'byhour': [23],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  it('should parse date interval and specific frequency - hour', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/hour/04_d.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.recur.until = result.recur.until.toString();

    const expected = {
      'start': '2021-01-01T00:00:00',
      'recur': {
        'freq': 'DAILY',
        'until': '2021-01-31T23:59:59',
        'byhour': [0, 1, 2, 3, 4],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  it('should parse datetime interval and specific frequency - hour', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/hour/04_dt.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.recur.until = result.recur.until.toString();

    const expected = {
      'start': '2021-01-01T12:00:00',
      'recur': {
        'freq': 'DAILY',
        'until': '2021-01-31T20:30:00',
        'byhour': [0, 1, 2, 3, 4],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should reject specific frequency - dayOfMonth', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/dayOfMonth/00.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  it('should reject specific frequency - dayOfMonth', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/dayOfMonth/01.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  it('should parse specific frequency - dayOfMonth', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/dayOfMonth/02.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();

    const expected = {
      'start': OFNCAL.TimeSpecConverter.INITIAL_DATE.toString(),
      'recur': {
        'freq': 'DAILY',
        'bymonthday': [1],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  it('should parse specific frequency - dayOfMonth', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/dayOfMonth/03.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();

    const expected = {
      'start': OFNCAL.TimeSpecConverter.INITIAL_DATE.toString(),
      'recur': {
        'freq': 'DAILY',
        'bymonthday': [31],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  it('should parse date interval and specific frequency - dayOfMonth', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/dayOfMonth/04_d.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.recur.until = result.recur.until.toString();

    const expected = {
      'start': '2021-01-01',
      'recur': {
        'freq': 'DAILY',
        'until': '2021-01-31',
        'bymonthday': [1, 2, 3, 4, 5],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  it('should parse datetime interval and specific frequency - dayOfMonth', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/dayOfMonth/04_dt.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.recur.until = result.recur.until.toString();

    const expected = {
      'start': '2021-01-01T12:00:00',
      'recur': {
        'freq': 'DAILY',
        'until': '2021-01-31T20:30:00',
        'bymonthday': [1, 2, 3, 4, 5],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should reject specific frequency - weekOfMonth', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/weekOfMonth/00.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  it('should reject specific frequency - weekOfMonth', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/weekOfMonth/01.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  it('should parse specific frequency - weekOfMonth (cs)', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/weekOfMonth/02.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec, 'cs');
    result.start = result.start.toString();

    const expected = {
      'start': OFNCAL.TimeSpecConverter.INITIAL_DATE.toString(),
      'desc': ['Událost se opakuje každý 1. týden měsíce.'],
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  it('should parse specific frequency - weekOfMonth (default lang)', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/weekOfMonth/03.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();

    const expected = {
      'start': OFNCAL.TimeSpecConverter.INITIAL_DATE.toString(),
      'desc': ['The event repeats every 5. week of month.'],
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  it('should parse date interval and specific frequency - weekOfMonth', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/weekOfMonth/04.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec, 'cs');
    result.start = result.start.toString();
    result.end = result.end.toString();

    const expected = {
      'start': '2021-01-01',
      'end': '2021-01-31',
      'desc': ['Událost se opakuje každý 1., 2., 3. týden měsíce.'],
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should reject specific frequency - weekOfYear', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/weekOfYear/00.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  it('should reject specific frequency - weekOfYear', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/weekOfYear/01.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  it('should parse specific frequency - weekOfYear', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/weekOfYear/02.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();

    const expected = {
      'start': OFNCAL.TimeSpecConverter.INITIAL_DATE.toString(),
      'recur': {
        'freq': 'YEARLY',
        'byweekno': [1],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  it('should parse specific frequency - weekOfYear', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/weekOfYear/03.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();

    const expected = {
      'start': OFNCAL.TimeSpecConverter.INITIAL_DATE.toString(),
      'recur': {
        'freq': 'YEARLY',
        'byweekno': [53],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  it('should parse date interval and specific frequency - weekOfYear', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/weekOfYear/04_d.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.recur.until = result.recur.until.toString();

    const expected = {
      'start': '2021-01-01',
      'recur': {
        'freq': 'YEARLY',
        'until': '2021-01-31',
        'byweekno': [1, 2, 3],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  it('should parse datetime interval and specific frequency - weekOfYear', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/weekOfYear/04_dt.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.recur.until = result.recur.until.toString();

    const expected = {
      'start': '2021-01-01T12:00:00',
      'recur': {
        'freq': 'YEARLY',
        'until': '2021-01-31T20:30:00',
        'byweekno': [1, 2, 3],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should reject specific frequency - yearOfDecade', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/yearOfDecade/00.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  it('should reject specific frequency - yearOfDecade', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/yearOfDecade/01.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  it('should parse specific frequency - yearOfDecade', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/yearOfDecade/02.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();

    const expected = {
      'start': '2000-01-01',
      'recur': {
        'freq': 'YEARLY',
        'interval': 10,
        'byday': ['MO', 'TU', 'WE', 'TH', 'FR', 'SA', 'SU'],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  it('should parse specific frequency - yearOfDecade', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/yearOfDecade/03.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();

    const expected = {
      'start': '2009-01-01',
      'recur': {
        'freq': 'YEARLY',
        'interval': 10,
        'byday': ['MO', 'TU', 'WE', 'TH', 'FR', 'SA', 'SU'],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  it('should parse date interval and specific frequency - yearOfDecade (cs)', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/yearOfDecade/04_d.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec, 'cs');
    result.start = result.start.toString();
    result.end = result.end.toString();

    const expected = {
      'start': '2000-01-01',
      'end': '2021-12-31',
      'desc': ['Událost se opakuje každý 0., 1., 2. rok desetiletí.'],
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  it('should parse datetime interval and specific frequency - yearOfDecade (default lang)', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/yearOfDecade/04_dt.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();

    const expected = {
      'start': '2000-01-01T12:00:00',
      'end': '2021-12-31T20:30:00',
      'desc': ['The event repeats every 0., 1., 2. year of decade.'],
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should reject specific frequency - yearOfCentury', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/yearOfCentury/00.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  it('should reject specific frequency - yearOfCentury', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/yearOfCentury/01.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  it('should parse specific frequency - yearOfCentury', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/yearOfCentury/02.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();

    const expected = {
      'start': '2100-01-01',
      'recur': {
        'freq': 'YEARLY',
        'interval': 100,
        'byday': ['MO', 'TU', 'WE', 'TH', 'FR', 'SA', 'SU'],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  it('should parse specific frequency - yearOfCentury', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/yearOfCentury/03.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();

    const expected = {
      'start': '2099-01-01',
      'recur': {
        'freq': 'YEARLY',
        'interval': 100,
        'byday': ['MO', 'TU', 'WE', 'TH', 'FR', 'SA', 'SU'],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  it('should parse date interval and specific frequency - yearOfCentury (cs)', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/yearOfCentury/04_d.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec, 'cs');
    result.start = result.start.toString();

    const expected = {
      'start': '2000-01-01',
      'desc': ['Událost se opakuje každý 3., 11., 42. rok století.'],
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  it('should parse datetime interval and specific frequency - yearOfCentury (default lang)', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/yearOfCentury/04_dt.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();

    const expected = {
      'start': '2000-01-01T12:00:00',
      'desc': ['The event repeats every 3., 11., 42. year of century.'],
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

});
