describe('TimeSpecConverter - time period duration', () => {
  it('should parse time period duration - year', () => {
    const timeSpec = require('../assets/time-spec/time-period-duration/00.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec, 'cs');
    result.start = result.start.toString();

    const interval = timeSpec['časový_interval'];
    const start = parseDateTime(interval['začátek']['datum']);
    const expected = {
      'start': start.toString(),
      'desc': ['Událost trvá rok.'],
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  it('should parse time period duration - year - default language', () => {
    const timeSpec = require('../assets/time-spec/time-period-duration/00.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec, 'de');
    result.start = result.start.toString();

    const interval = timeSpec['časový_interval'];
    const start = parseDateTime(interval['začátek']['datum']);
    const expected = {
      'start': start.toString(),
      'desc': ['The event lasts a year.'],
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  it('should parse time period duration - week', () => {
    const timeSpec = require('../assets/time-spec/time-period-duration/01.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec, 'cs');
    result.start = result.start.toString();

    const interval = timeSpec['časový_interval'];
    const start = parseDateTime(interval['začátek']['datum']);
    const expected = {
      'start': start.toString(),
      'desc': ['Událost trvá týden.'],
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  it('should parse time period duration - year, day', () => {
    const timeSpec = require('../assets/time-spec/time-period-duration/02.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec, 'cs');
    result.start = result.start.toString();

    const interval = timeSpec['časový_interval'];
    const start = parseDateTime(interval['začátek']['datum']);
    const expected = {
      'start': start.toString(),
      'desc': ['Událost trvá 24 hodin.'],
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  it('should parse time period duration - year, day, year_half', () => {
    const timeSpec = require('../assets/time-spec/time-period-duration/03.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec, 'cs');
    result.start = result.start.toString();

    const interval = timeSpec['časový_interval'];
    const start = parseDateTime(interval['začátek']['datum']);
    const expected = {
      'start': start.toString(),
      'desc': ['Událost trvá 24 hodin.'],
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  it('should parse time period duration - year, day, year_half - default language', () => {
    const timeSpec = require('../assets/time-spec/time-period-duration/03.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec, 'de');
    result.start = result.start.toString();

    const interval = timeSpec['časový_interval'];
    const start = parseDateTime(interval['začátek']['datum']);
    const expected = {
      'start': start.toString(),
      'desc': ['The event lasts 24 hours.'],
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  it('should parse time period duration - year, day, unknown, year_half', () => {
    const timeSpec = require('../assets/time-spec/time-period-duration/04.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec, 'cs');
    result.start = result.start.toString();

    const interval = timeSpec['časový_interval'];
    const start = parseDateTime(interval['začátek']['datum']);
    const expected = {
      'start': start.toString(),
      'desc': ['Událost trvá 24 hodin.'],
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  it('should parse time period duration - unknown', () => {
    const timeSpec = require('../assets/time-spec/time-period-duration/05.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec, 'cs');
    result.start = result.start.toString();

    const interval = timeSpec['časový_interval'];
    const start = parseDateTime(interval['začátek']['datum']);
    const expected = {
      'start': start.toString(),
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse time period duration, month and day - calendar_day, fri, jan', () => {
    const timeSpec = require('../assets/time-spec/time-period-duration/06.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec, 'cs');
    result.start = result.start.toString();

    const interval = timeSpec['časový_interval'];
    const start = parseDateTime(interval['začátek']['datum']);
    const expected = {
      'start': start.toString(),
      'recur': {
        'byday': ['FR'],
        'bymonth': [1],
        'freq': 'DAILY',
      },
      'desc': ['Událost trvá den (00:00 - 23:59).'],
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  it('should parse time period duration and month- unknown, jan', () => {
    const timeSpec = require('../assets/time-spec/time-period-duration/07.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec, 'cs');
    result.start = result.start.toString();

    const interval = timeSpec['časový_interval'];
    const start = parseDateTime(interval['začátek']['datum']);
    const expected = {
      'start': start.toString(),
      'recur': {
        'bymonth': [1],
        'freq': 'DAILY',
      },
    };
    assert.deepEqual(result, objectToEvent(objectToEvent(expected)));
  });

  // ---------------------------------------------------------------------------
  it('should reject time period duration - missing details', () => {
    const timeSpec = require('../assets/time-spec/time-period-duration/08.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  // ---------------------------------------------------------------------------
  it('should reject time period - invalid value', () => {
    const timeSpec = require('../assets/time-spec/time-period-duration/09.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  // ---------------------------------------------------------------------------
  it('should parse time period with SEC and all longer durations', () => {
    const timeSpec = require('../assets/time-spec/time-period-duration/10.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec, 'cs');
    result.start = result.start.toString();

    const interval = timeSpec['časový_interval'];
    const start = parseDateTime(interval['začátek']['datum']);
    const expected = {
      'start': start.toString(),
      'desc': [
        'Událost trvá sekundu.',
      ],
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse time period with MIN and all longer durations', () => {
    const timeSpec = require('../assets/time-spec/time-period-duration/11.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec, 'cs');
    result.start = result.start.toString();

    const interval = timeSpec['časový_interval'];
    const start = parseDateTime(interval['začátek']['datum']);
    const expected = {
      'start': start.toString(),
      'desc': [
        'Událost trvá minutu.',
      ],
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse time period with HOUR and all longer durations', () => {
    const timeSpec = require('../assets/time-spec/time-period-duration/12.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec, 'cs');
    result.start = result.start.toString();

    const interval = timeSpec['časový_interval'];
    const start = parseDateTime(interval['začátek']['datum']);
    const expected = {
      'start': start.toString(),
      'desc': [
        'Událost trvá hodinu.',
      ],
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse time period with MONTH and all longer durations', () => {
    const timeSpec = require('../assets/time-spec/time-period-duration/13.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec, 'cs');
    result.start = result.start.toString();

    const interval = timeSpec['časový_interval'];
    const start = parseDateTime(interval['začátek']['datum']);
    const expected = {
      'start': start.toString(),
      'desc': [
        'Událost trvá měsíc.',
      ],
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse time period with QUARTER and all longer durations', () => {
    const timeSpec = require('../assets/time-spec/time-period-duration/14.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec, 'cs');
    result.start = result.start.toString();

    const interval = timeSpec['časový_interval'];
    const start = parseDateTime(interval['začátek']['datum']);
    const expected = {
      'start': start.toString(),
      'desc': [
        'Událost trvá čtvrt roku.',
      ],
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse time period with YEAR_HALF and all longer durations', () => {
    const timeSpec = require('../assets/time-spec/time-period-duration/15.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec, 'cs');
    result.start = result.start.toString();

    const expected = {
      'start': OFNCAL.TimeSpecConverter.INITIAL_DATE.toString(),
      'desc': [
        'Událost trvá půl roku.',
      ],
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

});
