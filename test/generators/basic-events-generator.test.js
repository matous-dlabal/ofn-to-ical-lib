const eventData = require('../assets/udalost-01.json');

describe('BasicEventsGenerator', () => {
  // ---------------------------------------------------------------------------
  it('should throw an error - TimeSpecificationNotFoundError', () => {
    // clone eventData
    const data = JSON.parse(JSON.stringify(eventData));
    delete data['doba_trvání'];

    const langCode = 'cs';
    const eGen = new OFNCAL.BasicEventsGenerator();
    assert.throws(
        () => eGen.generate(data, langCode),
        {name: 'TimeSpecificationNotFoundError'}
    );
  });

  // ---------------------------------------------------------------------------
  it('should return one event', () => {
    // clone eventData
    const data = JSON.parse(JSON.stringify(eventData));
    data['doba_trvání'].pop();

    const langCode = 'cs';
    const eGen = new OFNCAL.BasicEventsGenerator();
    const result = eGen.generate(data, langCode);

    const newEvent = new OFNCAL.Event();
    newEvent.id = `${data['iri']}#${langCode}0`;
    newEvent.title = data['název'][langCode];
    newEvent.desc = [data['popis'][langCode]];
    const interval = data['doba_trvání'][0]['časový_interval'];
    newEvent.start = parseDateTime(interval['začátek']['datum_a_čas']);
    newEvent.end = parseDateTime(interval['konec']['datum_a_čas']);
    newEvent.start.toUnixTime();
    newEvent.end.toUnixTime();

    assert.deepEqual(result, [newEvent]);
  });

  // ---------------------------------------------------------------------------
  it('should return two cs events', () => {
    const langCode = 'cs';
    const eGen = new OFNCAL.BasicEventsGenerator();
    const result = eGen.generate(eventData, langCode);

    const iri = eventData['iri'];
    const title = eventData['název'][langCode];
    const desc = eventData['popis'][langCode];

    const newEvent0 = new OFNCAL.Event();
    newEvent0.id = `${iri}#${langCode}0`;
    newEvent0.title = title;
    newEvent0.desc.push(desc);
    const interval0 = eventData['doba_trvání'][0]['časový_interval'];
    newEvent0.start = parseDateTime(interval0['začátek']['datum_a_čas']);
    newEvent0.end = parseDateTime(interval0['konec']['datum_a_čas']);
    newEvent0.start.toUnixTime();
    newEvent0.end.toUnixTime();

    const newEvent1 = new OFNCAL.Event();
    newEvent1.id = `${iri}#${langCode}1`;
    newEvent1.title = title;
    newEvent1.desc.push(desc);
    const interval1 = eventData['doba_trvání'][1]['časový_interval'];
    newEvent1.start = parseDateTime(interval1['začátek']['datum_a_čas']);
    newEvent1.end = parseDateTime(interval1['konec']['datum_a_čas']);
    newEvent1.start.toUnixTime();
    newEvent1.end.toUnixTime();

    assert.deepEqual(result, [newEvent0, newEvent1]);
  });

  // ---------------------------------------------------------------------------
  it('should return two en events', () => {
    const langCode = 'en';
    const eGen = new OFNCAL.BasicEventsGenerator();
    const result = eGen.generate(eventData, langCode);

    const iri = eventData['iri'];
    const title = eventData['název'][langCode];
    const desc = eventData['popis'][langCode];

    const newEvent0 = new OFNCAL.Event();
    newEvent0.id = `${iri}#${langCode}0`;
    newEvent0.title = title;
    newEvent0.desc.push(desc);
    const interval0 = eventData['doba_trvání'][0]['časový_interval'];
    newEvent0.start = parseDateTime(interval0['začátek']['datum_a_čas']);
    newEvent0.end = parseDateTime(interval0['konec']['datum_a_čas']);
    newEvent0.start.toUnixTime();
    newEvent0.end.toUnixTime();

    const newEvent1 = new OFNCAL.Event();
    newEvent1.id = `${iri}#${langCode}1`;
    newEvent1.title = title;
    newEvent1.desc.push(desc);
    const interval1 = eventData['doba_trvání'][1]['časový_interval'];
    newEvent1.start = parseDateTime(interval1['začátek']['datum_a_čas']);
    newEvent1.end = parseDateTime(interval1['konec']['datum_a_čas']);
    newEvent1.start.toUnixTime();
    newEvent1.end.toUnixTime();

    assert.deepEqual(result, [newEvent0, newEvent1]);
  });

  // ---------------------------------------------------------------------------
  it('should throw an error - LanguageNotFoundError', () => {
    const langCode = 'nl';
    const eGen = new OFNCAL.BasicEventsGenerator();
    assert.throws(
        () => eGen.generate(eventData, langCode),
        {
          name: 'LanguageNotFoundError',
          message: `Language "${langCode}" not found in data.`,
        }
    );
  });

  // ---------------------------------------------------------------------------
  it('should return one event with timeSpec description', () => {
    // clone eventData
    const data = JSON.parse(JSON.stringify(eventData));
    data['doba_trvání'].pop();
    const timeSpecDesc = 'Popis časové specifikace';
    data['doba_trvání'][0]['popis'] = {};
    data['doba_trvání'][0]['popis']['cs'] = timeSpecDesc;

    const langCode = 'cs';
    const eGen = new OFNCAL.BasicEventsGenerator();
    const result = eGen.generate(data, langCode);

    const newEvent = new OFNCAL.Event();
    newEvent.id = `${data['iri']}#${langCode}0`;
    newEvent.title = data['název'][langCode];
    newEvent.desc = [timeSpecDesc, data['popis'][langCode]];
    const interval = data['doba_trvání'][0]['časový_interval'];
    newEvent.start = parseDateTime(interval['začátek']['datum_a_čas']);
    newEvent.end = parseDateTime(interval['konec']['datum_a_čas']);
    newEvent.start.toUnixTime();
    newEvent.end.toUnixTime();

    assert.deepEqual(result, [newEvent]);
  });

  // ---------------------------------------------------------------------------
  it('should throw an error - LanguageNotFoundError (timeSpec desc lang)', () => {
    // clone eventData
    const data = JSON.parse(JSON.stringify(eventData));
    data['doba_trvání'].pop();
    const timeSpecDesc = 'Time specification description';
    data['doba_trvání'][0]['popis'] = {};
    data['doba_trvání'][0]['popis']['en'] = timeSpecDesc;

    const langCode = 'cs';
    const eGen = new OFNCAL.BasicEventsGenerator();
    assert.throws(
        () => eGen.generate(data, langCode),
        {
          name: 'LanguageNotFoundError',
          message: `Language "${langCode}" not found in data.`,
        }
    );
  });
});

