describe('TimeSpecConverter', () => {
  // ---------------------------------------------------------------------------
  it('should parse datetime interval', () => {
    const timeSpec = require('../assets/time-spec/interval_00.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();

    const interval = timeSpec['časový_interval'];
    const start = parseDateTime(interval['začátek']['datum_a_čas']);
    const end = parseDateTime(interval['konec']['datum_a_čas']);
    const expected = {
      'start': start.toString(),
      'end': end.toString(),
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse date interval', () => {
    const timeSpec = require('../assets/time-spec/interval_01.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();

    const interval = timeSpec['časový_interval'];
    const start = parseDateTime(interval['začátek']['datum']);
    const end = parseDateTime(interval['konec']['datum']);
    const expected = {
      'start': start.toString(),
      'end': end.toString(),
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse date interval with unspecified start', () => {
    const timeSpec = require('../assets/time-spec/interval_02.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();

    const interval = timeSpec['časový_interval'];
    const end = parseDateTime(interval['konec']['datum']);
    const expected = {
      'start': `${end.year}-01-01`,
      'end': end.toString(),
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse date interval with missing start', () => {
    const timeSpec = require('../assets/time-spec/interval_03.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();

    const interval = timeSpec['časový_interval'];
    const end = parseDateTime(interval['konec']['datum']);
    const expected = {
      'start': `${end.year}-01-01`,
      'end': end.toString(),
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse date interval with unspecified end', () => {
    const timeSpec = require('../assets/time-spec/interval_04.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();

    const interval = timeSpec['časový_interval'];
    const start = parseDateTime(interval['začátek']['datum']);
    const expected = {
      'start': start.toString(),
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse date interval with missing end', () => {
    const timeSpec = require('../assets/time-spec/interval_05.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();

    const interval = timeSpec['časový_interval'];
    const start = parseDateTime(interval['začátek']['datum']);
    const expected = {
      'start': start.toString(),
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should reject mixed date and datetime interval', () => {
    const timeSpec = require('../assets/time-spec/interval_06.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  // ---------------------------------------------------------------------------
  it('should reject mixed date and datetime interval', () => {
    const timeSpec = require('../assets/time-spec/interval_07.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  // ---------------------------------------------------------------------------
  it('should parse datetime interval with missing start', () => {
    const timeSpec = require('../assets/time-spec/interval_08.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();

    const interval = timeSpec['časový_interval'];
    const end = parseDateTime(interval['konec']['datum_a_čas']);

    const expected = {
      'start': `${end.year}-01-01T00:00:00`,
      'end': end.toString(),
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should reject date interval whete start > end', () => {
    const timeSpec = require('../assets/time-spec/interval_09.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  // ---------------------------------------------------------------------------
  it('should reject datetime interval whete start > end', () => {
    const timeSpec = require('../assets/time-spec/interval_10.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  // ---------------------------------------------------------------------------
  it('should parse datetime moment', () => {
    const timeSpec = require('../assets/time-spec/moment_00.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();

    const moment = timeSpec['časový_okamžik'];
    const start = parseDateTime(moment['datum_a_čas']);
    const end = start;
    const expected = {
      'start': start.toString(),
      'end': end.toString(),
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse date moment', () => {
    const timeSpec = require('../assets/time-spec/moment_01.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();

    const moment = timeSpec['časový_okamžik'];
    const start = parseDateTime(moment['datum']);
    const end = start;
    const expected = {
      'start': start.toString(),
      'end': end.toString(),
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse datetime moment and ignore day of week (not weekday of moment)', () => {
    const timeSpec = require('../assets/time-spec/moment_02.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();

    const moment = timeSpec['časový_okamžik'];
    const start = parseDateTime(moment['datum_a_čas']);
    const end = start;
    const expected = {
      'start': start.toString(),
      'end': end.toString(),
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse datetime moment and ignore day of week (weekday of moment)', () => {
    const timeSpec = require('../assets/time-spec/moment_03.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();

    const moment = timeSpec['časový_okamžik'];
    const start = parseDateTime(moment['datum_a_čas']);
    const end = start;
    const expected = {
      'start': start.toString(),
      'end': end.toString(),
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse unspecified moment and convert day of week', () => {
    const timeSpec = require('../assets/time-spec/moment_04.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();

    const expected = {
      'start': OFNCAL.TimeSpecConverter.INITIAL_DATE.toString(),
      'recur': {
        'freq': 'DAILY',
        'byday': ['WE', 'TH', 'FR'],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse datetime moment inside datetime interval (start)', () => {
    const timeSpec = require('../assets/time-spec/interval_moment_00_0.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();

    const moment = timeSpec['časový_okamžik'];
    const start = parseDateTime(moment['datum_a_čas']);
    const end = start;
    const expected = {
      'start': start.toString(),
      'end': end.toString(),
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse datetime moment inside datetime interval (end)', () => {
    const timeSpec = require('../assets/time-spec/interval_moment_00_1.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();

    const moment = timeSpec['časový_okamžik'];
    const start = parseDateTime(moment['datum_a_čas']);
    const end = start;
    const expected = {
      'start': start.toString(),
      'end': end.toString(),
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse datetime moment inside datetime interval [start; inf)', () => {
    const timeSpec = require('../assets/time-spec/interval_moment_00_3.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();

    const moment = timeSpec['časový_okamžik'];
    const start = parseDateTime(moment['datum_a_čas']);
    const end = start;
    const expected = {
      'start': start.toString(),
      'end': end.toString(),
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should reject datetime moment outside datetime interval (preceding)', () => {
    const timeSpec = require('../assets/time-spec/interval_moment_01_0.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  // ---------------------------------------------------------------------------
  it('should reject datetime moment outside datetime interval (succeeding)', () => {
    const timeSpec = require('../assets/time-spec/interval_moment_01_1.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  // ---------------------------------------------------------------------------
  it('should reject datetime moment outside datetime interval [start; inf)', () => {
    const timeSpec = require('../assets/time-spec/interval_moment_01_3.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  // ---------------------------------------------------------------------------
  it('should parse datetime moment inside date interval (start)', () => {
    const timeSpec = require('../assets/time-spec/interval_moment_02_0.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();

    const moment = timeSpec['časový_okamžik'];
    const start = parseDateTime(moment['datum_a_čas']);
    const end = start;
    const expected = {
      'start': start.toString(),
      'end': end.toString(),
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse datetime moment inside date interval (end)', () => {
    const timeSpec = require('../assets/time-spec/interval_moment_02_1.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();

    const moment = timeSpec['časový_okamžik'];
    const start = parseDateTime(moment['datum_a_čas']);
    const end = start;
    const expected = {
      'start': start.toString(),
      'end': end.toString(),
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse datetime moment inside date interval [start; inf)', () => {
    const timeSpec = require('../assets/time-spec/interval_moment_02_3.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();

    const moment = timeSpec['časový_okamžik'];
    const start = parseDateTime(moment['datum_a_čas']);
    const end = start;
    const expected = {
      'start': start.toString(),
      'end': end.toString(),
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should reject datetime moment outside date interval (preceding)', () => {
    const timeSpec = require('../assets/time-spec/interval_moment_03_0.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  // ---------------------------------------------------------------------------
  it('should reject datetime moment outside date interval (succeeding)', () => {
    const timeSpec = require('../assets/time-spec/interval_moment_03_1.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  // ---------------------------------------------------------------------------
  it('should reject datetime moment outside date interval [start; inf)', () => {
    const timeSpec = require('../assets/time-spec/interval_moment_03_3.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  // ---------------------------------------------------------------------------
  it('should parse date moment inside datetime interval (start)', () => {
    const timeSpec = require('../assets/time-spec/interval_moment_04_0.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();

    const moment = timeSpec['časový_okamžik'];
    const start = parseDateTime(moment['datum']);
    const end = start;
    const expected = {
      'start': start.toString() + 'T00:00:00',
      'end': end.toString() + 'T23:59:59',
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse date moment inside datetime interval (end)', () => {
    const timeSpec = require('../assets/time-spec/interval_moment_04_1.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();

    const moment = timeSpec['časový_okamžik'];
    const start = parseDateTime(moment['datum']);
    const end = start;
    const expected = {
      'start': start.toString() + 'T00:00:00',
      'end': end.toString() + 'T23:59:59',
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse date moment inside datetime interval (middle)', () => {
    const timeSpec = require('../assets/time-spec/interval_moment_04_2.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();

    const moment = timeSpec['časový_okamžik'];
    const start = parseDateTime(moment['datum']);
    const end = start;
    const expected = {
      'start': start.toString() + 'T00:00:00',
      'end': end.toString() + 'T23:59:59',
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse date moment inside datetime interval [start; inf)', () => {
    const timeSpec = require('../assets/time-spec/interval_moment_04_3.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();

    const moment = timeSpec['časový_okamžik'];
    const start = parseDateTime(moment['datum']);
    const end = start;
    const expected = {
      'start': start.toString() + 'T00:00:00',
      'end': end.toString() + 'T23:59:59',
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse date moment inside date interval (start)', () => {
    const timeSpec = require('../assets/time-spec/interval_moment_05_0.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();

    const moment = timeSpec['časový_okamžik'];
    const start = parseDateTime(moment['datum']);
    const end = start;
    const expected = {
      'start': start.toString(),
      'end': end.toString(),
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse date moment inside date interval (end)', () => {
    const timeSpec = require('../assets/time-spec/interval_moment_05_1.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();

    const moment = timeSpec['časový_okamžik'];
    const start = parseDateTime(moment['datum']);
    const end = start;
    const expected = {
      'start': start.toString(),
      'end': end.toString(),
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse date moment inside date interval (middle)', () => {
    const timeSpec = require('../assets/time-spec/interval_moment_05_2.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();

    const moment = timeSpec['časový_okamžik'];
    const start = parseDateTime(moment['datum']);
    const end = start;
    const expected = {
      'start': start.toString(),
      'end': end.toString(),
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse date moment inside date interval [start; inf)', () => {
    const timeSpec = require('../assets/time-spec/interval_moment_05_3.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();

    const moment = timeSpec['časový_okamžik'];
    const start = parseDateTime(moment['datum']);
    const end = start;
    const expected = {
      'start': start.toString(),
      'end': end.toString(),
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should reject date moment outside datetime interval (preceding)', () => {
    const timeSpec = require('../assets/time-spec/interval_moment_06_0.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  // ---------------------------------------------------------------------------
  it('should reject date moment outside datetime interval (succeeding)', () => {
    const timeSpec = require('../assets/time-spec/interval_moment_06_1.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  // ---------------------------------------------------------------------------
  it('should reject date moment outside datetime interval [start; inf)', () => {
    const timeSpec = require('../assets/time-spec/interval_moment_06_3.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  // ---------------------------------------------------------------------------
  it('should reject date moment outside date interval (preceding)', () => {
    const timeSpec = require('../assets/time-spec/interval_moment_07_0.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  // ---------------------------------------------------------------------------
  it('should reject date moment outside date interval (succeeding)', () => {
    const timeSpec = require('../assets/time-spec/interval_moment_07_1.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  // ---------------------------------------------------------------------------
  it('should reject date moment outside date interval [start; inf)', () => {
    const timeSpec = require('../assets/time-spec/interval_moment_07_3.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  // ---------------------------------------------------------------------------
  it('should parse date moment intersecting datetime interval (start)', () => {
    const timeSpec = require('../assets/time-spec/interval_moment_08_0.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();

    const interval = timeSpec['časový_interval'];
    const start = parseDateTime(interval['začátek']['datum_a_čas']);
    const end = start.clone();
    end.hour = 23;
    end.minute = 59;
    end.second = 59;
    const expected = {
      'start': start.toString(),
      'end': end.toString(),
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse date moment intersecting datetime interval (end)', () => {
    const timeSpec = require('../assets/time-spec/interval_moment_08_1.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();

    const interval = timeSpec['časový_interval'];
    const end = parseDateTime(interval['konec']['datum_a_čas']);
    const start = end.clone();
    start.hour = 0;
    start.minute = 0;
    start.second = 0;
    const expected = {
      'start': start.toString(),
      'end': end.toString(),
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse date moment intersecting datetime interval [start; inf)', () => {
    const timeSpec = require('../assets/time-spec/interval_moment_08_3.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();

    const interval = timeSpec['časový_interval'];
    const start = parseDateTime(interval['začátek']['datum_a_čas']);
    const end = start.clone();
    end.hour = 23;
    end.minute = 59;
    end.second = 59;
    const expected = {
      'start': start.toString(),
      'end': end.toString(),
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse datetime interval inside date moment (start)', () => {
    const timeSpec = require('../assets/time-spec/interval_moment_09_0.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();

    const interval = timeSpec['časový_interval'];
    const start = parseDateTime(interval['začátek']['datum_a_čas']);
    const end = parseDateTime(interval['konec']['datum_a_čas']);
    const expected = {
      'start': start.toString(),
      'end': end.toString(),
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse datetime interval inside date moment (end)', () => {
    const timeSpec = require('../assets/time-spec/interval_moment_09_1.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();

    const interval = timeSpec['časový_interval'];
    const start = parseDateTime(interval['začátek']['datum_a_čas']);
    const end = parseDateTime(interval['konec']['datum_a_čas']);
    const expected = {
      'start': start.toString(),
      'end': end.toString(),
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse datetime interval inside date moment (middle)', () => {
    const timeSpec = require('../assets/time-spec/interval_moment_09_2.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();

    const interval = timeSpec['časový_interval'];
    const start = parseDateTime(interval['začátek']['datum_a_čas']);
    const end = parseDateTime(interval['konec']['datum_a_čas']);
    const expected = {
      'start': start.toString(),
      'end': end.toString(),
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse datetime interval and unspecified moment', () => {
    const timeSpec = require('../assets/time-spec/interval_moment_10.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();

    const interval = timeSpec['časový_interval'];
    const start = parseDateTime(interval['začátek']['datum_a_čas']);
    const end = parseDateTime(interval['konec']['datum_a_čas']);
    const expected = {
      'start': start.toString(),
      'end': end.toString(),
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should reject empty time specification', () => {
    const timeSpec = {
      'typ': 'Časová specifikace',
    };
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  it('should reject invalid time specification', () => {
    const timeSpec = {
      'typ': 'Časová specifikace',
      'časový_interval': 'do 1. ledna do 3. února',
    };
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidDataStructureError'}
    );
  });
});

