describe('TimeSpecConverter - time lapse', () => {
  // ---------------------------------------------------------------------------
  it('should parse date interval and time lapse', () => {
    const timeSpec = require('../assets/time-spec/time-lapse/00_d.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();
    result.recur.until = result.recur.until.toString();

    const expected = {
      'start': '2021-01-01T09:15:00',
      'end': '2021-01-01T09:15:00',
      'recur': {
        'freq': 'DAILY',
        'until': '2021-01-31T09:15:00',
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse datetime interval and time lapse', () => {
    const timeSpec = require('../assets/time-spec/time-lapse/00_dt.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();
    result.recur.until = result.recur.until.toString();

    const expected = {
      'start': '2021-01-01T09:15:00',
      'end': '2021-01-01T09:15:00',
      'recur': {
        'freq': 'DAILY',
        'until': '2021-01-31T09:15:00',
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse datetime interval and time lapse', () => {
    const timeSpec = require('../assets/time-spec/time-lapse/01_dt.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();
    result.recur.until = result.recur.until.toString();

    const expected = {
      'start': '2021-01-02T09:15:00',
      'end': '2021-01-02T09:15:00',
      'recur': {
        'freq': 'DAILY',
        'until': '2021-01-31T09:15:00',
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse datetime interval and time lapse', () => {
    const timeSpec = require('../assets/time-spec/time-lapse/02_dt.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();
    result.recur.until = result.recur.until.toString();

    const expected = {
      'start': '2021-01-01T09:15:00',
      'end': '2021-01-01T09:15:00',
      'recur': {
        'freq': 'DAILY',
        'until': '2021-01-30T09:15:00',
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse datetime interval and time lapse', () => {
    const timeSpec = require('../assets/time-spec/time-lapse/03_dt.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();
    result.recur.until = result.recur.until.toString();

    const expected = {
      'start': '2021-01-02T08:00:00',
      'end': '2021-01-02T09:15:00',
      'recur': {
        'freq': 'DAILY',
        'until': '2021-01-31T09:15:00',
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse datetime interval and time lapse', () => {
    const timeSpec = require('../assets/time-spec/time-lapse/04_dt.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();
    result.recur.until = result.recur.until.toString();

    const expected = {
      'start': '2021-01-01T08:00:00',
      'end': '2021-01-01T09:15:00',
      'recur': {
        'freq': 'DAILY',
        'until': '2021-01-30T09:15:00',
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse datetime interval and time lapse', () => {
    const timeSpec = require('../assets/time-spec/time-lapse/05_dt.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();
    result.recur.until = result.recur.until.toString();

    const expected = {
      'start': '2021-01-01T08:00:00',
      'end': '2021-01-01T19:15:00',
      'recur': {
        'freq': 'DAILY',
        'until': '2021-01-31T19:15:00',
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse date interval and time lapse', () => {
    const timeSpec = require('../assets/time-spec/time-lapse/06_d.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();
    result.recur.until = result.recur.until.toString();

    const expected = {
      'start': '2021-01-01T10:00:00',
      'end': '2021-01-01T22:45:00',
      'recur': {
        'freq': 'DAILY',
        'until': '2021-01-31T22:45:00',
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse datetime interval and time lapse', () => {
    const timeSpec = require('../assets/time-spec/time-lapse/06_dt.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();
    result.recur.until = result.recur.until.toString();

    const expected = {
      'start': '2021-01-01T10:00:00',
      'end': '2021-01-01T22:45:00',
      'recur': {
        'freq': 'DAILY',
        'until': '2021-01-31T22:45:00',
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should reject datetime interval and time lapse', () => {
    const timeSpec = require('../assets/time-spec/time-lapse/07_dt.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  // ---------------------------------------------------------------------------
  it('should reject datetime interval and time lapse', () => {
    const timeSpec = require('../assets/time-spec/time-lapse/08_dt.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  // ---------------------------------------------------------------------------
  it('should parse time lapse', () => {
    const timeSpec = require('../assets/time-spec/time-lapse/09.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();

    const initialDate = OFNCAL.TimeSpecConverter.INITIAL_DATE.toString();
    const expected = {
      'start': `${initialDate}T09:15:00`,
      'end': `${initialDate}T20:00:00`,
      'recur': {
        'freq': 'DAILY',
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse time lapse - more time lapses', () => {
    const timeSpec = require('../assets/time-spec/time-lapse/10.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();

    const initialDate = OFNCAL.TimeSpecConverter.INITIAL_DATE.toString();
    const expected = {
      'start': `${initialDate}T09:15:00`,
      'end': `${initialDate}T20:00:00`,
      'recur': {
        'freq': 'DAILY',
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse time lapse and interval and day of week', () => {
    const timeSpec = require('../assets/time-spec/time-lapse/11.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();
    result.recur.until = result.recur.until.toString();

    const expected = {
      'start': '2021-02-02T09:15:00',
      'end': '2021-02-02T20:00:00',
      'recur': {
        'freq': 'DAILY',
        'until': '2021-03-31T20:00:00',
        'byday': ['TU'],
        'bymonth': [2],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse empty time lapse and interval and day of week', () => {
    const timeSpec = require('../assets/time-spec/time-lapse/12.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.recur.until = result.recur.until.toString();

    const expected = {
      'start': '2021-02-02T00:00:00',
      'recur': {
        'freq': 'DAILY',
        'until': '2021-03-31T23:59:59',
        'byday': ['TU'],
        'bymonth': [2],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

});
