describe('TimeSpecConverter - months', () => {
  // ---------------------------------------------------------------------------
  it('should parse date interval and one month', () => {
    const timeSpec = require('../assets/time-spec/month/00_d.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.recur.until = result.recur.until.toString();

    const interval = timeSpec['časový_interval'];
    const start = parseDateTime(interval['začátek']['datum']);
    const until = parseDateTime(interval['konec']['datum']);
    const expected = {
      'start': start.toString(),
      'recur': {
        'freq': 'DAILY',
        'until': until.toString(),
        'bymonth': [1],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse datetime interval and one month', () => {
    const timeSpec = require('../assets/time-spec/month/00_dt.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.recur.until = result.recur.until.toString();

    const interval = timeSpec['časový_interval'];
    const start = parseDateTime(interval['začátek']['datum_a_čas']);
    const until = parseDateTime(interval['konec']['datum_a_čas']);
    const expected = {
      'start': start.toString(),
      'recur': {
        'freq': 'DAILY',
        'until': until.toString(),
        'bymonth': [1],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse date interval and one month (shift needed)', () => {
    const timeSpec = require('../assets/time-spec/month/01_d.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.recur.until = result.recur.until.toString();

    const interval = timeSpec['časový_interval'];
    const until = parseDateTime(interval['konec']['datum']);
    const expected = {
      'start': '2021-04-01',
      'recur': {
        'freq': 'DAILY',
        'until': until.toString(),
        'bymonth': [4],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse datetime interval and one month (shift needed)', () => {
    const timeSpec = require('../assets/time-spec/month/01_dt.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.recur.until = result.recur.until.toString();

    const interval = timeSpec['časový_interval'];
    const until = parseDateTime(interval['konec']['datum_a_čas']);
    const expected = {
      'start': '2021-04-01T00:00:00',
      'recur': {
        'freq': 'DAILY',
        'until': until.toString(),
        'bymonth': [4],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should reject date interval and one month', () => {
    const timeSpec = require('../assets/time-spec/month/02_d.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  // ---------------------------------------------------------------------------
  it('should parse date interval and months', () => {
    const timeSpec = require('../assets/time-spec/month/03_d.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.recur.until = result.recur.until.toString();

    const interval = timeSpec['časový_interval'];
    const start = parseDateTime(interval['začátek']['datum']);
    const until = parseDateTime(interval['konec']['datum']);
    const expected = {
      'start': start.toString(),
      'recur': {
        'freq': 'DAILY',
        'until': until.toString(),
        'bymonth': [12, 1, 4, 5],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse date interval and winter', () => {
    const timeSpec = require('../assets/time-spec/month/04_d.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.recur.until = result.recur.until.toString();

    const interval = timeSpec['časový_interval'];
    const start = parseDateTime(interval['začátek']['datum']);
    const until = parseDateTime(interval['konec']['datum']);
    const expected = {
      'start': start.toString(),
      'recur': {
        'freq': 'DAILY',
        'until': until.toString(),
        'bymonth': [12, 1, 2],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse date interval and spring (shift needed)', () => {
    const timeSpec = require('../assets/time-spec/month/05_d.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.recur.until = result.recur.until.toString();

    const interval = timeSpec['časový_interval'];
    const until = parseDateTime(interval['konec']['datum']);
    const expected = {
      'start': '2021-03-01',
      'recur': {
        'freq': 'DAILY',
        'until': until.toString(),
        'bymonth': [3, 4, 5],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should reject date interval and summer + autumn', () => {
    const timeSpec = require('../assets/time-spec/month/06_d.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  // ---------------------------------------------------------------------------
  it('should parse date interval and jan + autumn', () => {
    const timeSpec = require('../assets/time-spec/month/07_d.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.recur.until = result.recur.until.toString();

    const interval = timeSpec['časový_interval'];
    const start = parseDateTime(interval['začátek']['datum']);
    const until = parseDateTime(interval['konec']['datum']);
    const expected = {
      'start': start.toString(),
      'recur': {
        'freq': 'DAILY',
        'until': until.toString(),
        'bymonth': [1, 9, 10, 11],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse date interval and winter + sep', () => {
    const timeSpec = require('../assets/time-spec/month/08_d.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.recur.until = result.recur.until.toString();

    const interval = timeSpec['časový_interval'];
    const start = parseDateTime(interval['začátek']['datum']);
    const until = parseDateTime(interval['konec']['datum']);
    const expected = {
      'start': start.toString(),
      'recur': {
        'freq': 'DAILY',
        'until': until.toString(),
        'bymonth': [12, 1, 2, 9],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

});
