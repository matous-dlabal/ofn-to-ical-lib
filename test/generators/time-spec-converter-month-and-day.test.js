describe('TimeSpecConverter - months and days of week', () => {
  // ---------------------------------------------------------------------------
  it('should parse date interval and one month + day of week', () => {
    const timeSpec = require('../assets/time-spec/month/month-and-day/00_d.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.recur.until = result.recur.until.toString();

    const interval = timeSpec['časový_interval'];
    const start = parseDateTime(interval['začátek']['datum']);
    const until = parseDateTime(interval['konec']['datum']);
    const expected = {
      'start': start.toString(),
      'recur': {
        'freq': 'DAILY',
        'until': until.toString(),
        'byday': ['FR'],
        'bymonth': [1],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse datetime interval and one month + day of week', () => {
    const timeSpec = require('../assets/time-spec/month/month-and-day/00_dt.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.recur.until = result.recur.until.toString();

    const interval = timeSpec['časový_interval'];
    const start = parseDateTime(interval['začátek']['datum_a_čas']);
    const until = parseDateTime(interval['konec']['datum_a_čas']);
    const expected = {
      'start': start.toString(),
      'recur': {
        'freq': 'DAILY',
        'until': until.toString(),
        'byday': ['FR'],
        'bymonth': [1],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse date interval and one month + day of week (month shift needed)', () => {
    const timeSpec = require('../assets/time-spec/month/month-and-day/01_d.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.recur.until = result.recur.until.toString();

    const interval = timeSpec['časový_interval'];
    const until = parseDateTime(interval['konec']['datum']);
    const expected = {
      'start': '2021-04-01',
      'recur': {
        'freq': 'DAILY',
        'until': until.toString(),
        'byday': ['TH'],
        'bymonth': [4],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse datetime interval and one month + day of week (month shift needed)', () => {
    const timeSpec = require('../assets/time-spec/month/month-and-day/01_dt.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.recur.until = result.recur.until.toString();

    const interval = timeSpec['časový_interval'];
    const until = parseDateTime(interval['konec']['datum_a_čas']);
    const expected = {
      'start': '2021-04-01T00:00:00',
      'recur': {
        'freq': 'DAILY',
        'until': until.toString(),
        'byday': ['TH'],
        'bymonth': [4],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse date interval and winter + day of week (day shift needed)', () => {
    const timeSpec = require('../assets/time-spec/month/month-and-day/04_d.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.recur.until = result.recur.until.toString();

    const interval = timeSpec['časový_interval'];
    const until = parseDateTime(interval['konec']['datum']);
    const expected = {
      'start': '2021-01-03',
      'recur': {
        'freq': 'DAILY',
        'until': until.toString(),
        'byday': ['SU'],
        'bymonth': [12, 1, 2],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse date interval and spring + days of week (day and month shift needed)', () => {
    const timeSpec = require('../assets/time-spec/month/month-and-day/05_d.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.recur.until = result.recur.until.toString();

    const interval = timeSpec['časový_interval'];
    const until = parseDateTime(interval['konec']['datum']);
    const expected = {
      'start': '2021-03-03',
      'recur': {
        'freq': 'DAILY',
        'until': until.toString(),
        'byday': ['WE', 'TH', 'FR'],
        'bymonth': [3, 4, 5],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should reject date interval and summer + autumn + days of week', () => {
    const timeSpec = require('../assets/time-spec/month/month-and-day/06_d.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

});
