describe('TimeSpecConverter - frequency', () => {
  // ---------------------------------------------------------------------------
  it('should parse interval and frequency - biweekly (cs)', () => {
    const timeSpec = require('../assets/time-spec/freq/00.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec, 'cs');
    result.start = result.start.toString();
    result.end = result.end.toString();

    const expected = {
      'start': '2021-01-01',
      'end': '2021-01-31',
      'desc': ['Událost se opakuje jednou za dva týdny.'],
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse two frequencies - biweekly, daily (default lang)', () => {
    const timeSpec = require('../assets/time-spec/freq/01.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();

    const expected = {
      'start': OFNCAL.TimeSpecConverter.INITIAL_DATE.toString(),
      'desc': [
        'The event repeats every other week.',
        'The event repeats every day.',
      ],
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should reject frequency - wrong value', () => {
    const timeSpec = require('../assets/time-spec/freq/02.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  // ---------------------------------------------------------------------------
  it('should parse frequency and other specification (en)', () => {
    const timeSpec = require('../assets/time-spec/freq/03.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec, 'en');
    result.start = result.start.toString();

    const expected = {
      'start': OFNCAL.TimeSpecConverter.INITIAL_DATE.toString(),
      'desc': [
        'The event repeats every year.',
        'Valid in good weather.',
      ],
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse with empty frequency and empty other specification', () => {
    const timeSpec = require('../assets/time-spec/freq/04.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.end = result.end.toString();

    const expected = {
      'start': '2000-01-01',
      'end': '2000-01-01',
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

});
