const eventData = require('../assets/udalost-01.json');

describe('BasicCalendarsGenerator', () => {
  // ---------------------------------------------------------------------------
  it('should throw an error - TimeSpecificationNotFoundError', () => {
    // clone eventData
    const data = JSON.parse(JSON.stringify(eventData));
    delete data['doba_trvání'];
    const calendarData = [data, data, data];

    const langCode = 'cs';
    const cGen = new OFNCAL.BasicCalendarsGenerator();
    assert.throws(
        () => cGen.generate(calendarData, langCode),
        {name: 'TimeSpecificationNotFoundError'}
    );
  });

  // ---------------------------------------------------------------------------
  it('should return one calendar with one cs event', () => {
    // clone eventData
    const data = JSON.parse(JSON.stringify(eventData));
    data['doba_trvání'].pop();

    const langCode = 'cs';
    const cGen = new OFNCAL.BasicCalendarsGenerator();
    const result = cGen.generate(data, langCode);

    const newEvent = new OFNCAL.Event();
    newEvent.id = `${data['iri']}#${langCode}0`;
    newEvent.title = data['název'][langCode];
    newEvent.desc = [data['popis'][langCode]];
    const interval = data['doba_trvání'][0]['časový_interval'];
    newEvent.start = parseDateTime(interval['začátek']['datum_a_čas']);
    newEvent.end = parseDateTime(interval['konec']['datum_a_čas']);
    newEvent.start.toUnixTime();
    newEvent.end.toUnixTime();

    const expected = [
      new OFNCAL.Calendar([
        newEvent,
      ]),
    ];
    assert.deepEqual(result, expected);
  });

  // ---------------------------------------------------------------------------
  it('should throw an error (from list input) - TimeSpecificationNotFoundError', () => {
    // clone eventData
    const data = JSON.parse(JSON.stringify(eventData));
    data['doba_trvání'].pop();
    const emptyData = JSON.parse(JSON.stringify(eventData));
    delete emptyData['doba_trvání'];
    const calendarData = [emptyData, data, emptyData];

    const langCode = 'cs';
    const cGen = new OFNCAL.BasicCalendarsGenerator();
    assert.throws(
        () => cGen.generate(calendarData, langCode),
        {name: 'TimeSpecificationNotFoundError'}
    );
  });

  // ---------------------------------------------------------------------------
  it('should return two calendars each with two en events', () => {
    const calendarData = [eventData, eventData];
    const langCode = 'en';
    const cGen = new OFNCAL.BasicCalendarsGenerator();
    const result = cGen.generate(calendarData, langCode);

    const iri = eventData['iri'];
    const title = eventData['název'][langCode];
    const desc = eventData['popis'][langCode];

    const id0 = `${iri}#${langCode}0`;
    const interval0 = eventData['doba_trvání'][0]['časový_interval'];
    const start0 = parseDateTime(interval0['začátek']['datum_a_čas']);
    const end0 = parseDateTime(interval0['konec']['datum_a_čas']);
    start0.toUnixTime();
    end0.toUnixTime();

    const id1 = `${iri}#${langCode}1`;
    const interval1 = eventData['doba_trvání'][1]['časový_interval'];
    const start1 = parseDateTime(interval1['začátek']['datum_a_čas']);
    const end1 = parseDateTime(interval1['konec']['datum_a_čas']);
    start1.toUnixTime();
    end1.toUnixTime();

    const newEvent0 = new OFNCAL.Event();
    newEvent0.title = title;
    newEvent0.desc = [desc];
    newEvent0.id = id0;
    newEvent0.start = start0;
    newEvent0.end = end0;

    const newEvent1 = new OFNCAL.Event();
    newEvent1.title = title;
    newEvent1.desc = [desc];
    newEvent1.id = id1;
    newEvent1.start = start1;
    newEvent1.end = end1;

    const expected = [
      new OFNCAL.Calendar([
        newEvent0,
        newEvent1,
      ]),
      new OFNCAL.Calendar([
        newEvent0,
        newEvent1,
      ]),
    ];
    assert.deepEqual(result, expected);
  });
});

