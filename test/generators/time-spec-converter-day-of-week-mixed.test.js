describe('TimeSpecConverter - day of week - mixed', () => {
  // ---------------------------------------------------------------------------
  it('should parse date interval and days of week (weekday of start)', () => {
    const timeSpec = require('../assets/time-spec/day-of-week/mixed/02_d.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.recur.until = result.recur.until.toString();

    const interval = timeSpec['časový_interval'];
    const start = parseDateTime(interval['začátek']['datum']);
    const until = parseDateTime(interval['konec']['datum']);
    const expected = {
      'start': start.toString(),
      'recur': {
        'freq': 'DAILY',
        'until': until.toString(),
        'byday': ['FR', 'TH', 'MO'],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse datetime interval and days of week (weekday of start)', () => {
    const timeSpec = require('../assets/time-spec/day-of-week/mixed/02_dt.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.recur.until = result.recur.until.toString();

    const interval = timeSpec['časový_interval'];
    const start = parseDateTime(interval['začátek']['datum_a_čas']);
    const until = parseDateTime(interval['konec']['datum_a_čas']);
    const expected = {
      'start': start.toString(),
      'recur': {
        'freq': 'DAILY',
        'until': until.toString(),
        'byday': ['FR', 'TH', 'MO'],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse date interval and days of week (not weekday of start)', () => {
    const timeSpec = require('../assets/time-spec/day-of-week/mixed/03_d.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.recur.until = result.recur.until.toString();

    const interval = timeSpec['časový_interval'];
    const until = parseDateTime(interval['konec']['datum']);
    const expected = {
      'start': '2021-01-02',
      'recur': {
        'freq': 'DAILY',
        'until': until.toString(),
        'byday': ['MO', 'WE', 'SA', 'SU', 'TU', 'TH'],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse datetime interval and days of week (not weekday of start)', () => {
    const timeSpec = require('../assets/time-spec/day-of-week/mixed/03_dt.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.recur.until = result.recur.until.toString();

    const interval = timeSpec['časový_interval'];
    const until = parseDateTime(interval['konec']['datum_a_čas']);
    const expected = {
      'start': '2021-01-02T00:00:00',
      'recur': {
        'freq': 'DAILY',
        'until': until.toString(),
        'byday': ['MO', 'WE', 'SA', 'SU', 'TU', 'TH'],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse date interval [start; inf) and days of week (not weekday of start)', () => {
    const timeSpec = require('../assets/time-spec/day-of-week/mixed/04_d.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();

    const expected = {
      'start': '2021-01-02',
      'recur': {
        'freq': 'DAILY',
        'byday': ['MO', 'TH', 'SA', 'SU'],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse datetime interval [start; inf) and days of week (not weekday of start)', () => {
    const timeSpec = require('../assets/time-spec/day-of-week/mixed/04_dt.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();

    const expected = {
      'start': '2021-01-02T00:00:00',
      'recur': {
        'freq': 'DAILY',
        'byday': ['MO', 'TH', 'SA', 'SU'],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should reject date interval and days of week', () => {
    const timeSpec = require('../assets/time-spec/day-of-week/mixed/05_d.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  // ---------------------------------------------------------------------------
  it('should reject datetime interval and days of week', () => {
    const timeSpec = require('../assets/time-spec/day-of-week/mixed/05_dt.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  // ---------------------------------------------------------------------------
  it('should reject date moment and days of week', () => {
    const timeSpec = require('../assets/time-spec/day-of-week/mixed/06_d.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  // ---------------------------------------------------------------------------
  it('should parse date moment and days of week (weekday of moment)', () => {
    const timeSpec = require('../assets/time-spec/day-of-week/mixed/07_d.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.recur.until = result.recur.until.toString();

    const moment = timeSpec['časový_okamžik'];
    const start = parseDateTime(moment['datum']);
    const until = parseDateTime(moment['datum']);
    const expected = {
      'start': start.toString(),
      'recur': {
        'freq': 'DAILY',
        'until': until.toString(),
        'byday': ['WE', 'FR', 'TH'],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse date interval and working days', () => {
    const timeSpec = require('../assets/time-spec/day-of-week/mixed/08_0_d.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();
    result.recur.until = result.recur.until.toString();

    const interval = timeSpec['časový_interval'];
    const start = parseDateTime(interval['začátek']['datum']);
    const until = parseDateTime(interval['konec']['datum']);
    const expected = {
      'start': start.toString(),
      'recur': {
        'freq': 'DAILY',
        'until': until.toString(),
        'byday': ['MO', 'TU', 'WE', 'TH', 'FR'],
      },
      'desc': ['Event occures on working days.'],
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  it('should parse datetime interval and working days', () => {
    const timeSpec = require('../assets/time-spec/day-of-week/mixed/08_0_dt.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec, 'cs');
    result.start = result.start.toString();
    result.recur.until = result.recur.until.toString();

    const interval = timeSpec['časový_interval'];
    const start = parseDateTime(interval['začátek']['datum_a_čas']);
    const until = parseDateTime(interval['konec']['datum_a_čas']);
    const expected = {
      'start': start.toString(),
      'recur': {
        'freq': 'DAILY',
        'until': until.toString(),
        'byday': ['MO', 'TU', 'WE', 'TH', 'FR'],
      },
      'desc': ['Událost se koná v pracovní dny.'],
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  it('should reject date interval and working days + sunday', () => {
    const timeSpec = require('../assets/time-spec/day-of-week/mixed/08_1_d.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  it('should reject datetime interval and working days + sunday', () => {
    const timeSpec = require('../assets/time-spec/day-of-week/mixed/08_1_dt.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  it('should parse datetime interval and empty days arrays', () => {
    const timeSpec = require('../assets/time-spec/day-of-week/mixed/09_dt.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec, 'cs');
    result.start = result.start.toString();
    result.end = result.end.toString();

    const interval = timeSpec['časový_interval'];
    const start = parseDateTime(interval['začátek']['datum_a_čas']);
    const end = parseDateTime(interval['konec']['datum_a_čas']);
    const expected = {
      'start': start.toString(),
      'end': end.toString(),
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

});
