describe('TimeSpecConverter - specific frequency (mix)', () => {
  // ---------------------------------------------------------------------------
  it('should reject mix of specific frequencies', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/mix/00.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

  // ---------------------------------------------------------------------------
  it('should parse mix of specific frequencies with one yearOfDecade', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/mix/01.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();

    const expected = {
      'start': '2023-01-01T00:00:00',
      'recur': {
        'freq': 'YEARLY',
        'interval': 10,
        'byminute': [0, 1, 59],
        'byhour': [9, 10, 11, 12],
        'bymonthday': [10, 20, 30],
        'byweekno': [33, 34, 35, 36, 37],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse mix of specific frequencies with one yearOfCentury', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/mix/02.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec);
    result.start = result.start.toString();

    const expected = {
      'start': '2109-01-01T00:00:00',
      'recur': {
        'freq': 'YEARLY',
        'interval': 100,
        'byminute': [0, 1, 59],
        'byhour': [9, 10, 11, 12],
        'bymonthday': [10, 20, 30],
        'byweekno': [33, 34, 35, 36, 37],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse mix of specific frequencies with one yearOfDecade and one yearOfCentury', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/mix/03.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec, 'cs');
    result.start = result.start.toString();

    const expected = {
      'start': '2023-01-01',
      'recur': {
        'freq': 'YEARLY',
        'interval': 10,
        'byweekno': [34, 35, 36],
      },
      'desc': ['Událost se opakuje každý 37. rok století.'],
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse mix of specific frequencies with one yearOfDecade and multiple yearsOfCentury', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/mix/04.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec, 'cs');
    result.start = result.start.toString();

    const expected = {
      'start': '2023-01-01',
      'recur': {
        'freq': 'YEARLY',
        'interval': 10,
        'byweekno': [34, 35, 36],
      },
      'desc': ['Událost se opakuje každý 37., 38. rok století.'],
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse mix of specific frequencies with multiple yearsOfDecade and one yearOfCentury', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/mix/05.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec, 'cs');
    result.start = result.start.toString();

    const expected = {
      'start': '2111-01-01',
      'recur': {
        'freq': 'YEARLY',
        'interval': 100,
        'byweekno': [34, 35, 36],
      },
      'desc': ['Událost se opakuje každý 3., 4. rok desetiletí.'],
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse mix of specific frequencies with multiple yearsOfDecade and multiple yearsOfCentury', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/mix/06.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec, 'cs');
    result.start = result.start.toString();

    const expected = {
      'start': '2019-01-01',
      'recur': {
        'freq': 'YEARLY',
        'byweekno': [34, 35, 36],
      },
      'desc': [
        'Událost se opakuje každý 3., 4. rok desetiletí.',
        'Událost se opakuje každý 11., 89. rok století.',
      ],
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ===========================================================================
  it('should parse decade specific frequency with day of week and months', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/mix/07.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec, 'cs');
    result.start = result.start.toString();
    result.recur.until = result.recur.until.toString();

    const expected = {
      'start': '2020-02-07T00:00:00',
      'recur': {
        'freq': 'YEARLY',
        'interval': 10,
        'until': '2050-12-31T20:30:00',
        'byday': ['FR'],
        'bymonth': [8, 9, 2],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should parse century specific frequency with day of week and time lapse', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/mix/08.json');
    const parser = new OFNCAL.TimeSpecConverter();
    const result = parser.convert(timeSpec, 'cs');
    result.start = result.start.toString();
    result.end = result.end.toString();
    result.recur.until = result.recur.until.toString();

    const expected = {
      'start': '2042-01-03T09:15:00',
      'end': '2042-01-03T20:00:00',
      'recur': {
        'freq': 'YEARLY',
        'interval': 100,
        'until': '2050-12-31T20:00:00',
        'byday': ['FR'],
      },
    };
    assert.deepEqual(result, objectToEvent(expected));
  });

  // ---------------------------------------------------------------------------
  it('should reject specific frequency which never occures within given interval', () => {
    const timeSpec = require('../assets/time-spec/spec-freq/mix/09.json');
    const parser = new OFNCAL.TimeSpecConverter();

    assert.throws(
        () => parser.convert(timeSpec),
        {name: 'InvalidTimeSpecificationError'}
    );
  });

});
