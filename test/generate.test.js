const eventData01 = require('./assets/udalost-01.json');
const icalData01 = fs.readFileSync(__dirname + '/assets/udalost-01.cs.ics', 'utf-8');

const eventData02 = require('./assets/udalost-02.json');
const icalData02 = fs.readFileSync(__dirname + '/assets/udalost-02-one.cs.ics', 'utf-8');

const eventData03 = require('./assets/udalost-03.json');
const icalData03 = fs.readFileSync(__dirname + '/assets/udalost-03.en.ics', 'utf-8');

describe('generate', () => {
  it('should throw an error - JSON.parse syntax error', () => {
    const data = 'not a json';
    const langCode = 'cs';
    const asOneCalendar = false;
    assert.throws(
        () => OFNCAL.generate(data, langCode, asOneCalendar),
        {name: 'SyntaxError', message: /Unexpected token/}
    );
  });

  it('should throw an error - InvalidDataStructureError', () => {
    const data = '{"iri": "example.org"}';
    const langCode = 'cs';
    const asOneCalendar = false;
    assert.throws(
        () => OFNCAL.generate(data, langCode, asOneCalendar),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should throw an error - TimeSpecificationNotFoundError', () => {
    const clonedData = JSON.parse(JSON.stringify(eventData01));
    delete clonedData['doba_trvání'];
    const data = JSON.stringify(clonedData);
    const langCode = 'cs';
    const asOneCalendar = false;
    assert.throws(
        () => OFNCAL.generate(data, langCode, asOneCalendar),
        {name: 'TimeSpecificationNotFoundError'}
    );
  });

  it('should throw an error - InvalidLangCodeError', () => {
    const data = JSON.stringify(eventData01);
    const langCode = 'xx';
    const asOneCalendar = false;
    assert.throws(
        () => OFNCAL.generate(data, langCode, asOneCalendar),
        {
          name: 'InvalidLangCodeError',
          message: `Language code "${langCode}" not conformant with ISO 639-1.`,
        }
    );
  });

  it('should throw an error - LanguageNotFoundError', () => {
    const data = JSON.stringify(eventData01);
    const langCode = 'nl';
    const asOneCalendar = false;
    assert.throws(
        () => OFNCAL.generate(data, langCode, asOneCalendar),
        {
          name: 'LanguageNotFoundError',
          message: `Language "${langCode}" not found in data.`,
        }
    );
  });

  it('should generate correct iCal data - multiple calendars', () => {
    const data = JSON.stringify([eventData01, eventData01]);
    const langCode = 'cs';
    const asOneCalendar = false;
    let result = OFNCAL.generate(data, langCode, asOneCalendar);
    // remove last two chars (\r\n)
    let expected = [icalData01.slice(0, -2), icalData01.slice(0, -2)];

    result = result.map(
        (e) => e.replace(/DTSTAMP:.*\r\n/g, 'DTSTAMP:__stamp__\r\n')
    );
    expected = expected.map(
        (e) => e.replace(/DTSTAMP:.*\r\n/g, 'DTSTAMP:__stamp__\r\n')
    );

    assert.deepEqual(result, expected);
  });

  it('should generate correct iCal data - single calendar', () => {
    const data = JSON.stringify(eventData02);
    const langCode = 'cs';
    const asOneCalendar = true;
    let result = OFNCAL.generate(data, langCode, asOneCalendar);
    // remove last two chars (\r\n)
    let expected = [icalData02.slice(0, -2)];

    result = result.map(
        (e) => e.replace(/DTSTAMP:.*\r\n/g, 'DTSTAMP:__stamp__\r\n')
    );
    expected = expected.map(
        (e) => e.replace(/DTSTAMP:.*\r\n/g, 'DTSTAMP:__stamp__\r\n')
    );

    assert.deepEqual(result, expected);
  });

  it('should generate correct iCal data (en) - single calendar without description', () => {
    const data = JSON.stringify(eventData03);
    const langCode = 'en';
    const asOneCalendar = true;
    let result = OFNCAL.generate(data, langCode, asOneCalendar);
    // remove last two chars (\r\n)
    let expected = [icalData03.slice(0, -2)];

    result = result.map(
        (e) => e.replace(/DTSTAMP:.*\r\n/g, 'DTSTAMP:__stamp__\r\n')
    );
    expected = expected.map(
        (e) => e.replace(/DTSTAMP:.*\r\n/g, 'DTSTAMP:__stamp__\r\n')
    );

    assert.deepEqual(result, expected);
  });

});
