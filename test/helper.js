const files = require('../src-files-list.json');

files.forEach((file) => {
  require('../' + file);
});

assert = require('assert').strict;
fs = require('fs');

objectToEvent = function(obj) {
  const e = new OFNCAL.Event();
  if (obj.start) {
    e.start = obj.start;
  }
  if (obj.end) {
    e.end = obj.end;
  }
  if (obj.desc) {
    e.desc = obj.desc;
  }
  if (obj.recur) {
    e.recur = obj.recur;
  }
  return e;
};

parseDateTime = OFNCAL.TimeSpecConverter.parseDateTime;
