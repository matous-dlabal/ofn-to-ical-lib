describe('ICalCalendarExporter', () => {
  // -------------------------------------------------------------------------
  it('should convert given data to iCalendar format', () => {
    const e = new OFNCAL.Event();
    e.id = '13a8wfaf684';
    e.title = 'test title';
    e.desc = ['this is a test description'];
    e.start = parseDateTime('2021-02-06T12:05:19');
    e.end = parseDateTime('2021-02-09T12:05:19');
    const cal = new OFNCAL.Calendar([e]);
    const exporter = new OFNCAL.ICalCalendarExporter();
    const result = exporter.exportCalendar(cal);
    const timeNow = ICAL.Time.fromJSDate(new Date(), true).toICALString();
    const expected = `\
BEGIN:VCALENDAR\r
VERSION:2.0\r
PRODID:${OFNCAL.PRODID}\r
BEGIN:VEVENT\r
DTSTAMP:${timeNow}\r
UID:${e.id}\r
SUMMARY:${e.title}\r
DESCRIPTION:${e.desc}\r
DTSTART:${e.start.toICALString()}\r
DTEND:${e.end.toICALString()}\r
END:VEVENT\r
END:VCALENDAR\
`;
    assert.equal(result, expected);
  });

  // -------------------------------------------------------------------------
  it('should convert given data with recur to iCalendar format', () => {
    const e = new OFNCAL.Event();
    e.id = '13a8wfaf684';
    e.title = 'test title';
    e.desc = ['this is a test description'];
    e.start = parseDateTime('2021-02-06T12:05:19');
    e.recur = {};
    e.recur.freq = 'DAILY';
    e.recur.byday = ['MO', 'WE', 'FR'];

    const cal = new OFNCAL.Calendar([e]);
    const exporter = new OFNCAL.ICalCalendarExporter();
    const result = exporter.exportCalendar(cal);
    const timeNow = ICAL.Time.fromJSDate(new Date(), true).toICALString();
    const expected = `\
BEGIN:VCALENDAR\r
VERSION:2.0\r
PRODID:${OFNCAL.PRODID}\r
BEGIN:VEVENT\r
DTSTAMP:${timeNow}\r
UID:${e.id}\r
SUMMARY:${e.title}\r
DESCRIPTION:${e.desc}\r
DTSTART:${e.start.toICALString()}\r
RRULE:FREQ=DAILY;BYDAY=MO,WE,FR\r
END:VEVENT\r
END:VCALENDAR\
`;
    assert.equal(result, expected);
  });

});
