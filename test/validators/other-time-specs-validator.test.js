describe('OtherTimeSpecValidator', () => {
  it('should reject non array data', () => {
    const validator = new OFNCAL.OtherTimeSpecsValidator();
    const data = 'data';
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject null', () => {
    const validator = new OFNCAL.OtherTimeSpecsValidator();
    const data = null;
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject empty object', () => {
    const validator = new OFNCAL.OtherTimeSpecsValidator();
    const data = {};
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  // --------------------------------------------------------------------------
  it('should reject other time specification value without prefix', () => {
    const validator = new OFNCAL.OtherTimeSpecsValidator();
    const data = [
      'dobré-počasí',
    ];
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject other time specification value without prefix', () => {
    const validator = new OFNCAL.OtherTimeSpecsValidator();
    const data = [
      'https://data.mvcr.gov.cz/zdroj/číselníky/jiná-časová-specifikace/položky/dobré-počasí ',
      'dobré-počasí',
    ];
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject other time specification value without prefix', () => {
    const validator = new OFNCAL.OtherTimeSpecsValidator();
    const data = [
      'https://data.mvcr.gov.cz/zdroj/číselníky/jiná-časová-specifikace/dobré-počasí',
      'https://data.mvcr.gov.cz/zdroj/číselníky/jiná-časová-specifikace/položky/špatné-počasí',
    ];
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject other time specification number value ', () => {
    const validator = new OFNCAL.OtherTimeSpecsValidator();
    const data = [
      'https://data.mvcr.gov.cz/zdroj/číselníky/jiná-časová-specifikace/položky/dobré-počasí',
      42,
    ];
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject other time specification number value ', () => {
    const validator = new OFNCAL.OtherTimeSpecsValidator();
    const data = [
      'https://data.mvcr.gov.cz/zdroj/číselníky/jiná-časová-specifikace/položky/dobré-počasí',
      [
        'https://data.mvcr.gov.cz/zdroj/číselníky/jiná-časová-specifikace/položky/dobré-počasí',
      ],
    ];
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  // --------------------------------------------------------------------------
  it('should accept array with one other time specification', () => {
    const validator = new OFNCAL.OtherTimeSpecsValidator();
    const data = [
      'https://data.mvcr.gov.cz/zdroj/číselníky/jiná-časová-specifikace/položky/špatné-počasí',
    ];
    validator.validate(data);
  });

  it('should accept array with multiple other time specifications', () => {
    const validator = new OFNCAL.OtherTimeSpecsValidator();
    const data = [
      'https://data.mvcr.gov.cz/zdroj/číselníky/jiná-časová-specifikace/položky/dobré-počasí',
      'https://data.mvcr.gov.cz/zdroj/číselníky/jiná-časová-specifikace/položky/špatné-počasí',
    ];
    validator.validate(data);
  });

  it('should accept empty array', () => {
    const validator = new OFNCAL.OtherTimeSpecsValidator();
    const data = [];
    validator.validate(data);
  });

});
