describe('TimePeriodsValidator', () => {
  it('should reject non array data', () => {
    const validator = new OFNCAL.TimePeriodsValidator();
    const data = 'data';
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject null', () => {
    const validator = new OFNCAL.TimePeriodsValidator();
    const data = null;
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject empty object', () => {
    const validator = new OFNCAL.TimePeriodsValidator();
    const data = {};
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  // --------------------------------------------------------------------------
  it('should reject time period value without prefix', () => {
    const validator = new OFNCAL.TimePeriodsValidator();
    const data = [
      'OCT',
    ];
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject time period value without prefix', () => {
    const validator = new OFNCAL.TimePeriodsValidator();
    const data = [
      'http://publications.europa.eu/resource/authority/timeperiod/FRI',
      'OCT',
    ];
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject time period value without prefix', () => {
    const validator = new OFNCAL.TimePeriodsValidator();
    const data = [
      'http://publications.europa.eu/resource/authority/timeperiod/MON',
      'http://publications.europa.eu/resource/timeperiod/FRI',
      'http://publications.europa.eu/resource/authority/timeperiod/SUMMER',
    ];
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  // --------------------------------------------------------------------------
  it('should accept array with one time period', () => {
    const validator = new OFNCAL.TimePeriodsValidator();
    const data = [
      'http://publications.europa.eu/resource/authority/timeperiod/SUMMER',
    ];
    validator.validate(data);
  });

  it('should accept array with multiple time periods', () => {
    const validator = new OFNCAL.TimePeriodsValidator();
    const data = [
      'http://publications.europa.eu/resource/authority/timeperiod/SUN',
      'http://publications.europa.eu/resource/authority/timeperiod/SUMMER',
      'http://publications.europa.eu/resource/authority/timeperiod/DEC',
      'http://publications.europa.eu/resource/authority/timeperiod/OCT',
    ];
    validator.validate(data);
  });

  it('should accept empty array', () => {
    const validator = new OFNCAL.TimePeriodsValidator();
    const data = [];
    validator.validate(data);
  });

});
