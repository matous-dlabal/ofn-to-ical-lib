describe('FrequenciesValidator', () => {
  it('should reject non array data', () => {
    const validator = new OFNCAL.FrequenciesValidator();
    const data = 'data';
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject null', () => {
    const validator = new OFNCAL.FrequenciesValidator();
    const data = null;
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject empty object', () => {
    const validator = new OFNCAL.FrequenciesValidator();
    const data = {};
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  // --------------------------------------------------------------------------
  it('should reject frequency value without prefix', () => {
    const validator = new OFNCAL.FrequenciesValidator();
    const data = [
      'BIWEEKLY',
    ];
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject frequency value without prefix', () => {
    const validator = new OFNCAL.FrequenciesValidator();
    const data = [
      'http://publications.europa.eu/resource/authority/frequency/WEEKLY_3',
      'BIWEEKLY',
    ];
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject frequency value without prefix', () => {
    const validator = new OFNCAL.FrequenciesValidator();
    const data = [
      'http://publications.europa.eu/resource/authority/frequency/BYWEEKLY',
      'http://publications.europa.eu/resource/authority/frequency/WEEKLY_2',
      'http://publications.europa.eu/resource/frequency/WEEKLY_3',
    ];
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  // --------------------------------------------------------------------------
  it('should accept array with one frequency', () => {
    const validator = new OFNCAL.FrequenciesValidator();
    const data = [
      'http://publications.europa.eu/resource/authority/frequency/BYWEEKLY',
    ];
    validator.validate(data);
  });

  it('should accept array with multiple frequencies', () => {
    const validator = new OFNCAL.FrequenciesValidator();
    const data = [
      'http://publications.europa.eu/resource/authority/frequency/BYWEEKLY',
      'http://publications.europa.eu/resource/authority/frequency/WEEKLY_2',
      'http://publications.europa.eu/resource/authority/frequency/WEEKLY_3',
      'http://publications.europa.eu/resource/authority/frequency/MONTHLY',
      'http://publications.europa.eu/resource/authority/frequency/HOURLY',
    ];
    validator.validate(data);
  });

  it('should accept empty array', () => {
    const validator = new OFNCAL.FrequenciesValidator();
    const data = [];
    validator.validate(data);
  });

});
