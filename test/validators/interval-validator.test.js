describe('IntervalValidator', () => {
  it('should reject non object data', () => {
    const validator = new OFNCAL.IntervalValidator();
    const data = 'data';
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject array', () => {
    const validator = new OFNCAL.IntervalValidator();
    const data = ['data', 'xxx'];
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject null', () => {
    const validator = new OFNCAL.IntervalValidator();
    const data = null;
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject empty object', () => {
    const validator = new OFNCAL.IntervalValidator();
    const data = {};
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  // --------------------------------------------------------------------------
  it('should reject data without typ', () => {
    const validator = new OFNCAL.IntervalValidator();
    const data = {
      'začátek': {
        'typ': 'Časový okamžik',
        'datum': '2021-01-01',
      },
      'konec': {
        'typ': 'Časový okamžik',
        'datum': '2021-12-31',
      },
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject data when typ is a number', () => {
    const validator = new OFNCAL.IntervalValidator();
    const data = {
      'typ': 42,
      'začátek': {
        'typ': 'Časový okamžik',
        'datum': '2021-01-01',
      },
      'konec': {
        'typ': 'Časový okamžik',
        'datum': '2021-12-31',
      },
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject data with incorect type', () => {
    const validator = new OFNCAL.IntervalValidator();
    const data = {
      'typ': 'Časový Interval',
      'začátek': {
        'typ': 'Časový okamžik',
        'datum': '2021-01-01',
      },
      'konec': {
        'typ': 'Časový okamžik',
        'datum': '2021-12-31',
      },
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  // --------------------------------------------------------------------------
  it('should reject data without začátek and konec', () => {
    const validator = new OFNCAL.IntervalValidator();
    const data = {
      'typ': 'Časový interval',
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject data when začátek is a number', () => {
    const validator = new OFNCAL.IntervalValidator();
    const data = {
      'typ': 'Časový okamžik',
      'začátek': 42,
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject data when začátek is a string', () => {
    const validator = new OFNCAL.IntervalValidator();
    const data = {
      'typ': 'Časový okamžik',
      'začátek': '2020-01-01',
      'konec': {
        'typ': 'Časový okamžik',
        'datum': '2021-12-31',
      },
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject data when konec is a number', () => {
    const validator = new OFNCAL.IntervalValidator();
    const data = {
      'typ': 'Časový okamžik',
      'konec': 42,
      'začátek': {
        'typ': 'Časový okamžik',
        'datum': '2021-01-01',
      },
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject data when konec is a string', () => {
    const validator = new OFNCAL.IntervalValidator();
    const data = {
      'typ': 'Časový okamžik',
      'konec': '2021-01-01',
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });
  // --------------------------------------------------------------------------
  it('should reject data when začátek is a not časový okamžik', () => {
    const validator = new OFNCAL.IntervalValidator();
    const data = {
      'typ': 'Časový okamžik',
      'začátek': {
        'datum': '2021-01-01',
      },
      'konec': {
        'typ': 'Časový okamžik',
        'datum': '2021-01-01',
      },
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  // --------------------------------------------------------------------------
  it('should accept data with začátek and konec', () => {
    const validator = new OFNCAL.IntervalValidator();
    const data = {
      'typ': 'Časový interval',
      'začátek': {
        'typ': 'Časový okamžik',
        'datum': '2021-01-01',
      },
      'konec': {
        'typ': 'Časový okamžik',
        'datum': '2021-12-31',
      },
    };
    validator.validate(data);
  });

  it('should accept data with only začátek', () => {
    const validator = new OFNCAL.IntervalValidator();
    const data = {
      'typ': 'Časový interval',
      'začátek': {
        'typ': 'Časový okamžik',
        'datum': '2021-01-01',
      },
    };
    validator.validate(data);
  });

  it('should accept data with only konec', () => {
    const validator = new OFNCAL.IntervalValidator();
    const data = {
      'typ': 'Časový interval',
      'konec': {
        'typ': 'Časový okamžik',
        'datum': '2021-12-31',
      },
    };
    validator.validate(data);
  });

});
