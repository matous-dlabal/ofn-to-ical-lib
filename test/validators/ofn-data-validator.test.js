describe('OfnDataValidator', () => {
  // --------------------------------------------------------------------------
  it('should reject data array without "iri"', () => {
    const validator = new OFNCAL.OfnDataValidator();
    const data = [{
      'typ': 'typ',
      'iri': 'iri',
      'název': {cs: 'název', en: 'title'},
      'popis': {cs: 'popis', en: 'description'},
    }, {
      'typ': 'typ',
      'název': {cs: 'název', en: 'title'},
      'popis': {cs: 'popis', en: 'description'},
    }];
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });
  it('should reject data without "iri"', () => {
    const validator = new OFNCAL.OfnDataValidator();
    const data = {
      'typ': 'typ',
      'název': {cs: 'název', en: 'title'},
      'popis': {cs: 'popis', en: 'description'},
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });
  it('should reject data when "iri" is a number', () => {
    const validator = new OFNCAL.OfnDataValidator();
    const data = {
      'typ': 'typ',
      'iri': 42,
      'název': {cs: 'název', en: 'title'},
      'popis': {cs: 'popis', en: 'description'},
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });
  it('should reject data when "iri" is an object', () => {
    const validator = new OFNCAL.OfnDataValidator();
    const data = {
      'typ': 'typ',
      'iri': {x: 42},
      'název': {cs: 'název', en: 'title'},
      'popis': {cs: 'popis', en: 'description'},
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });
  // --------------------------------------------------------------------------
  it('should reject data array without "název"', () => {
    const validator = new OFNCAL.OfnDataValidator();
    const data = [{
      'typ': 'typ',
      'iri': 'iri',
      'popis': {cs: 'popis', en: 'description'},
    }, {
      'typ': 'typ',
      'iri': 'iri',
      'název': {cs: 'název', en: 'title'},
      'popis': {cs: 'popis', en: 'description'},
    }];
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });
  it('should reject data without "název"', () => {
    const validator = new OFNCAL.OfnDataValidator();
    const data = {
      'typ': 'typ',
      'iri': 'iri',
      'popis': {cs: 'popis', en: 'description'},
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });
  it('should reject data when "název" is a number', () => {
    const validator = new OFNCAL.OfnDataValidator();
    const data = {
      'typ': 'typ',
      'iri': 'iri',
      'název': 42,
      'popis': {cs: 'popis', en: 'description'},
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });
  it('should reject data when "název" is an array', () => {
    const validator = new OFNCAL.OfnDataValidator();
    const data = {
      'typ': 'typ',
      'iri': 'iri',
      'název': ['název'],
      'popis': {cs: 'popis', en: 'description'},
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });
  it('should reject data when "název" is a string', () => {
    const validator = new OFNCAL.OfnDataValidator();
    const data = {
      'typ': 'typ',
      'iri': 'iri',
      'název': 'název',
      'popis': {cs: 'popis', en: 'description'},
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });
  it('should reject data when "název" is null', () => {
    const validator = new OFNCAL.OfnDataValidator();
    const data = {
      'typ': 'typ',
      'iri': 'iri',
      'název': null,
      'popis': {cs: 'popis', en: 'description'},
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });
  // --------------------------------------------------------------------------
  it('should reject data when "popis" is a number', () => {
    const validator = new OFNCAL.OfnDataValidator();
    const data = {
      'typ': 'typ',
      'iri': 'iri',
      'název': {cs: 'název', en: 'title'},
      'popis': 42,
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });
  it('should reject data when "popis" is an array', () => {
    const validator = new OFNCAL.OfnDataValidator();
    const data = {
      'typ': 'typ',
      'iri': 'iri',
      'název': {cs: 'název', en: 'title'},
      'popis': ['popis'],
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });
  it('should reject data when "popis" is a string', () => {
    const validator = new OFNCAL.OfnDataValidator();
    const data = {
      'typ': 'typ',
      'iri': 'iri',
      'název': {cs: 'název', en: 'title'},
      'popis': 'popis',
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });
  it('should reject data when "popis" is null', () => {
    const validator = new OFNCAL.OfnDataValidator();
    const data = {
      'typ': 'typ',
      'iri': 'iri',
      'název': {cs: 'název', en: 'title'},
      'popis': null,
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });
  // --------------------------------------------------------------------------
  it('should reject data without "typ"', () => {
    const validator = new OFNCAL.OfnDataValidator();
    const data = {
      'iri': 'iri',
      'název': {cs: 'název', en: 'title'},
      'popis': {cs: 'popis', en: 'description'},
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });
  it('should reject data when "typ" is a number', () => {
    const validator = new OFNCAL.OfnDataValidator();
    const data = {
      'typ': 42,
      'iri': 'iri',
      'název': {cs: 'název', en: 'title'},
      'popis': {cs: 'popis', en: 'description'},
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });
  it('should reject data when "typ" is an array', () => {
    const validator = new OFNCAL.OfnDataValidator();
    const data = {
      'typ': [42],
      'iri': 'iri',
      'název': {cs: 'název', en: 'title'},
      'popis': {cs: 'popis', en: 'description'},
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });
  it('should reject data when "typ" is an object', () => {
    const validator = new OFNCAL.OfnDataValidator();
    const data = {
      'typ': {typ: 'typ'},
      'iri': 'iri',
      'název': {cs: 'název', en: 'title'},
      'popis': {cs: 'popis', en: 'description'},
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });
  // --------------------------------------------------------------------------
  it('should accept data', () => {
    const validator = new OFNCAL.OfnDataValidator();
    const data = {
      'typ': 'typ',
      'iri': 'iri',
      'název': {cs: 'název', en: 'title'},
      'popis': {cs: 'popis', en: 'description'},
    };
    validator.validate(data);
  });
  it('should accept data array', () => {
    const validator = new OFNCAL.OfnDataValidator();
    const data = [{
      'typ': 'typ',
      'iri': 'iri',
      'název': {cs: 'název', en: 'title'},
      'popis': {cs: 'popis', en: 'description'},
    }, {
      'typ': 'typ',
      'iri': 'iri',
      'název': {cs: 'název', en: 'title'},
      'popis': {cs: 'popis', en: 'description'},
    }];
    validator.validate(data);
  });
  it('should not accept data string', () => {
    const validator = new OFNCAL.OfnDataValidator();
    const data = 'data';
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });
  it('should not accept data number', () => {
    const validator = new OFNCAL.OfnDataValidator();
    const data = 666;
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });
});

