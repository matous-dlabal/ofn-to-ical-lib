describe('DaysOfWeekValidator', () => {
  it('should reject non array data', () => {
    const validator = new OFNCAL.DaysOfWeekValidator();
    const data = 'data';
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject null', () => {
    const validator = new OFNCAL.DaysOfWeekValidator();
    const data = null;
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject empty object', () => {
    const validator = new OFNCAL.DaysOfWeekValidator();
    const data = {};
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  // --------------------------------------------------------------------------
  it('should reject day of week value without prefix', () => {
    const validator = new OFNCAL.DaysOfWeekValidator();
    const data = [
      'pondělí',
    ];
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject day of week value without prefix', () => {
    const validator = new OFNCAL.DaysOfWeekValidator();
    const data = [
      'https://data.mvcr.gov.cz/zdroj/číselníky/dny-v-týdnu/položky/pátek',
      'sobota',
    ];
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject day of week value without prefix', () => {
    const validator = new OFNCAL.DaysOfWeekValidator();
    const data = [
      'https://data.mvcr.gov.cz/zdroj/číselníky/dny-v-týdnu/položky/pondělí',
      'https://data.mvcr.gov.cz/zdroj/číselníky/dny-v-týdnu/středa',
      'https://data.mvcr.gov.cz/zdroj/číselníky/dny-v-týdnu/položky/pátek',
    ];
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  // --------------------------------------------------------------------------
  it('should accept array with one day of week', () => {
    const validator = new OFNCAL.DaysOfWeekValidator();
    const data = [
      'https://data.mvcr.gov.cz/zdroj/číselníky/dny-v-týdnu/položky/pondělí',
    ];
    validator.validate(data);
  });

  it('should accept array with multiple days of week', () => {
    const validator = new OFNCAL.DaysOfWeekValidator();
    const data = [
      'https://data.mvcr.gov.cz/zdroj/číselníky/dny-v-týdnu/položky/pondělí',
      'https://data.mvcr.gov.cz/zdroj/číselníky/dny-v-týdnu/položky/středa',
      'https://data.mvcr.gov.cz/zdroj/číselníky/dny-v-týdnu/položky/pátek',
      'https://data.mvcr.gov.cz/zdroj/číselníky/dny-v-týdnu/položky/sobota',
    ];
    validator.validate(data);
  });

  it('should accept empty array', () => {
    const validator = new OFNCAL.DaysOfWeekValidator();
    const data = [];
    validator.validate(data);
  });

});
