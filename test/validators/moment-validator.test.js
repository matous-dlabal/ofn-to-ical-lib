describe('MomentValidator', () => {
  it('should reject non object data', () => {
    const validator = new OFNCAL.MomentValidator();
    const data = 'data';
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject array', () => {
    const validator = new OFNCAL.MomentValidator();
    const data = ['data', 'xxx'];
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject null', () => {
    const validator = new OFNCAL.MomentValidator();
    const data = null;
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject empty object', () => {
    const validator = new OFNCAL.MomentValidator();
    const data = {};
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  // --------------------------------------------------------------------------
  it('should reject data without typ', () => {
    const validator = new OFNCAL.MomentValidator();
    const data = {
      'datum': '2021-01-01',
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject data when typ is a number', () => {
    const validator = new OFNCAL.MomentValidator();
    const data = {
      'typ': 42,
      'datum': '2021-01-01',
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject data with incorect type', () => {
    const validator = new OFNCAL.MomentValidator();
    const data = {
      'typ': 'Časový okamžikaaa',
      'datum': '2021-01-01',
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  // --------------------------------------------------------------------------
  it('should reject data without date', () => {
    const validator = new OFNCAL.MomentValidator();
    const data = {
      'typ': 'Časový okamžik',
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject data when datum is a number', () => {
    const validator = new OFNCAL.MomentValidator();
    const data = {
      'typ': 'Časový okamžik',
      'datum': 42,
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject data when datum_a_čas is a number', () => {
    const validator = new OFNCAL.MomentValidator();
    const data = {
      'typ': 'Časový okamžik',
      'datum_a_čas': 42,
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject data when nespecifikovaný is a number', () => {
    const validator = new OFNCAL.MomentValidator();
    const data = {
      'typ': 'Časový okamžik',
      'nespecifikovaný': 0,
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject data when nespecifikovaný is a string', () => {
    const validator = new OFNCAL.MomentValidator();
    const data = {
      'typ': 'Časový okamžik',
      'nespecifikovaný': 'true',
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject data when nespecifikovaný is a string', () => {
    const validator = new OFNCAL.MomentValidator();
    const data = {
      'typ': 'Časový okamžik',
      'nespecifikovaný': 'true',
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  // --------------------------------------------------------------------------
  it('should reject data with datum a nespecifikovaný', () => {
    const validator = new OFNCAL.MomentValidator();
    const data = {
      'typ': 'Časový okamžik',
      'datum': '2021-01-01',
      'nespecifikovaný': true,
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject data with datum_a_čas a nespecifikovaný', () => {
    const validator = new OFNCAL.MomentValidator();
    const data = {
      'typ': 'Časový okamžik',
      'datum_a_čas': '2021-01-01T00:00:00+01:00',
      'nespecifikovaný': true,
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject data with datum a datum_a_čas', () => {
    const validator = new OFNCAL.MomentValidator();
    const data = {
      'typ': 'Časový okamžik',
      'datum': '2021-01-01',
      'datum_a_čas': '2021-01-01T08:30:00+01:00',
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  // --------------------------------------------------------------------------
  it('should reject data with other key', () => {
    const validator = new OFNCAL.MomentValidator();
    const data = {
      'typ': 'Časový okamžik',
      'other': '2021-01-01',
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  // --------------------------------------------------------------------------
  it('should accept data with datum', () => {
    const validator = new OFNCAL.MomentValidator();
    const data = {
      'typ': 'Časový okamžik',
      'datum': '2021-01-01',
    };
    validator.validate(data);
  });

  it('should accept data with datum_a_čas', () => {
    const validator = new OFNCAL.MomentValidator();
    const data = {
      'typ': 'Časový okamžik',
      'datum_a_čas': '2021-01-01T08:30:45+01:00',
    };
    validator.validate(data);
  });

  it('should accept data with nespecifikovaný', () => {
    const validator = new OFNCAL.MomentValidator();
    const data = {
      'typ': 'Časový okamžik',
      'nespecifikovaný': true,
    };
    validator.validate(data);
  });

});
