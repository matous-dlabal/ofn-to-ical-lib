describe('TimeLapseValidator', () => {
  it('should reject non object data', () => {
    const validator = new OFNCAL.TimeLapseValidator();
    const data = 'data';
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject array', () => {
    const validator = new OFNCAL.TimeLapseValidator();
    const data = ['data', 'xxx'];
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject null', () => {
    const validator = new OFNCAL.TimeLapseValidator();
    const data = null;
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject empty object', () => {
    const validator = new OFNCAL.TimeLapseValidator();
    const data = {};
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  // --------------------------------------------------------------------------
  it('should reject data without typ', () => {
    const validator = new OFNCAL.TimeLapseValidator();
    const data = {
      'čas': '08:30:00',
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject data without typ', () => {
    const validator = new OFNCAL.TimeLapseValidator();
    const data = {
      'od': '08:30:00',
      'do': '10:30:00',
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject data when typ is a number', () => {
    const validator = new OFNCAL.TimeLapseValidator();
    const data = {
      'typ': 42,
      'čas': '08:30:00',
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject data with incorect type', () => {
    const validator = new OFNCAL.TimeLapseValidator();
    const data = {
      'typ': 'Časová Doba',
      'čas': '08:30:00',
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  // --------------------------------------------------------------------------
  it('should reject data with čas and od', () => {
    const validator = new OFNCAL.TimeLapseValidator();
    const data = {
      'typ': 'Časová doba',
      'čas': '08:30:00',
      'od': '12:00:00',
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject data with čas and do', () => {
    const validator = new OFNCAL.TimeLapseValidator();
    const data = {
      'typ': 'Časová doba',
      'čas': '08:30:00',
      'do': '12:00:00',
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject data with čas and od and do', () => {
    const validator = new OFNCAL.TimeLapseValidator();
    const data = {
      'typ': 'Časová doba',
      'čas': '08:30:00',
      'od': '12:00:00',
      'do': '12:00:00',
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });
  // --------------------------------------------------------------------------
  it('should reject data with only typ', () => {
    const validator = new OFNCAL.TimeLapseValidator();
    const data = {
      'typ': 'Časová doba',
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject data with od but not do', () => {
    const validator = new OFNCAL.TimeLapseValidator();
    const data = {
      'typ': 'Časová doba',
      'od': '08:30:00',
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject data with do but not od', () => {
    const validator = new OFNCAL.TimeLapseValidator();
    const data = {
      'typ': 'Časová doba',
      'do': '12:00:00',
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });
  // --------------------------------------------------------------------------
  it('should reject data where čas is a number', () => {
    const validator = new OFNCAL.TimeLapseValidator();
    const data = {
      'typ': 'Časová doba',
      'čas': 12,
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject data where čas is an object', () => {
    const validator = new OFNCAL.TimeLapseValidator();
    const data = {
      'typ': 'Časová doba',
      'čas': {
        'typ': 'Časový okamžik',
        'datum_a_čas': '2021-01-01T12:00:00+01:00',
      },
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject data where od is a number', () => {
    const validator = new OFNCAL.TimeLapseValidator();
    const data = {
      'typ': 'Časová doba',
      'od': 12,
      'do': '12:00:00',
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject data where od is an object', () => {
    const validator = new OFNCAL.TimeLapseValidator();
    const data = {
      'typ': 'Časová doba',
      'od': {
        'typ': 'Časový okamžik',
        'datum_a_čas': '2021-01-01T12:00:00+01:00',
      },
      'do': '12:00:00',
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject data where do is a number', () => {
    const validator = new OFNCAL.TimeLapseValidator();
    const data = {
      'typ': 'Časová doba',
      'od': '12:00:00',
      'do': 12,
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject data where do is an object', () => {
    const validator = new OFNCAL.TimeLapseValidator();
    const data = {
      'typ': 'Časová doba',
      'od': '12:00:00',
      'do': {
        'typ': 'Časový okamžik',
        'datum_a_čas': '2021-01-01T12:00:00+01:00',
      },
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  // --------------------------------------------------------------------------
  it('should accept data with čas', () => {
    const validator = new OFNCAL.TimeLapseValidator();
    const data = {
      'typ': 'Časová doba',
      'čas': '08:30:00',
    };
    validator.validate(data);
  });

  it('should accept data with datum_a_čas', () => {
    const validator = new OFNCAL.TimeLapseValidator();
    const data = {
      'typ': 'Časová doba',
      'od': '08:30:00',
      'do': '12:00:00',
    };
    validator.validate(data);
  });

});
