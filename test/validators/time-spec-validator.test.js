describe('TimeSpecValidator', () => {
  it('should reject string data', () => {
    const validator = new OFNCAL.TimeSpecValidator();
    const data = 'data';
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject null', () => {
    const validator = new OFNCAL.TimeSpecValidator();
    const data = null;
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject empty object', () => {
    const validator = new OFNCAL.TimeSpecValidator();
    const data = {};
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  // --------------------------------------------------------------------------
  it('should reject time specification where typ is a number', () => {
    const validator = new OFNCAL.TimeSpecValidator();
    const data = {
      'typ': 42,
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject time specification with incorect typ', () => {
    const validator = new OFNCAL.TimeSpecValidator();
    const data = {
      'typ': 'Časová Specifikace',
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  // --------------------------------------------------------------------------
  it('should reject time specification where počet opakování is a string', () => {
    const validator = new OFNCAL.TimeSpecValidator();
    const data = {
      'typ': 'Časová specifikace',
      'počet_opakování': '3',
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  // --------------------------------------------------------------------------
  it('should reject time specification where den v týdnu is not an array', () => {
    const validator = new OFNCAL.TimeSpecValidator();
    const data = {
      'typ': 'Časová specifikace',
      'den_v_týdnu': 'https://data.mvcr.gov.cz/zdroj/číselníky/dny-v-týdnu/položky/pondělí',
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  // --------------------------------------------------------------------------
  it('should reject time specification with invalid interval', () => {
    const validator = new OFNCAL.TimeSpecValidator();
    const data = {
      'typ': 'Časová specifikace',
      'časový_interval': {
        'typ': 'Časový interval',
        'začátek': {
          'typ': 'Časový okamžik',
          'datum': 5,
        },
        'konec': {
          'typ': 'Časový okamžik',
          'datum': 10,
        },
      },
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  // --------------------------------------------------------------------------
  it('should reject time specification with invalid time lapse', () => {
    const validator = new OFNCAL.TimeSpecValidator();
    const data = {
      'typ': 'Časová specifikace',
      'časová_doba': [
        {
          'typ': 'Časová doba',
          'čas': '12:00:00',
        }, {
          'typ': 'Časová doba',
          'od': '12:00:00',
        },
      ],
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  // --------------------------------------------------------------------------
  it('should reject time specification where time lapses are not in array', () => {
    const validator = new OFNCAL.TimeSpecValidator();
    const data = {
      'typ': 'Časová specifikace',
      'časová_doba': {
        'typ': 'Časová doba',
        'čas': '12:00:00',
      },
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  // --------------------------------------------------------------------------
  it('should reject time specification with invalid specific frequency', () => {
    const validator = new OFNCAL.TimeSpecValidator();
    const data = {
      'typ': 'Časová specifikace',
      'specifická_frekvence': [
        {
          'typ': 'Specifická frekvence',
          'hodina': 8,
        },
        {
          'typ': 'Specifická frekvence',
          'den_v_měsíci': '13',
        },
      ],
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  // --------------------------------------------------------------------------
  it('should reject time specification where specific frequencies are not in array', () => {
    const validator = new OFNCAL.TimeSpecValidator();
    const data = {
      'typ': 'Časová specifikace',
      'specifická_frekvence': {
        'typ': 'Specifická frekvence',
        'hodina': 8,
      },
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  // --------------------------------------------------------------------------
  it('should accept time specification with only typ', () => {
    const validator = new OFNCAL.TimeSpecValidator();
    const data = {
      'typ': 'Časová specifikace',
    };
    validator.validate(data);
  });

  it('should accept time specification with all keys', () => {
    const validator = new OFNCAL.TimeSpecValidator();
    const data = {
      'typ': 'Časová specifikace',
      'časový_interval': {
        'typ': 'Časový interval',
        'začátek': {
          'typ': 'Časový okamžik',
          'datum': '2021-01-01',
        },
        'konec': {
          'typ': 'Časový okamžik',
          'datum': '2021-02-01',
        },
      },
      'časový_okamžik': {
        'typ': 'Časový okamžik',
        'datum': '2021-01-01',
      },
      'časová_doba': [
        {
          'typ': 'Časová doba',
          'čas': '12:00:00',
        }, {
          'typ': 'Časová doba',
          'čas': '12:00:00',
        },
      ],
      'den_v_týdnu': [
        'https://data.mvcr.gov.cz/zdroj/číselníky/dny-v-týdnu/položky/pondělí',
        'https://data.mvcr.gov.cz/zdroj/číselníky/dny-v-týdnu/položky/pátek',
      ],
      'časové období': [
        'http://publications.europa.eu/resource/authority/timeperiod/OCT',
        'http://publications.europa.eu/resource/authority/timeperiod/DEC',
      ],
      'frekvence': [
        'http://publications.europa.eu/resource/authority/frequency/BIWEEKLY',
        'http://publications.europa.eu/resource/authority/frequency/WEEKLY',
      ],
      'jiná_časová_specifikace': [
        'https://data.mvcr.gov.cz/zdroj/číselníky/jiná-časová-specifikace/položky/dobré-počasí',
        'https://data.mvcr.gov.cz/zdroj/číselníky/jiná-časová-specifikace/položky/špatné-počasí',
      ],
      'počet_opakování': 3,
      'specifická_frekvence': [
        {
          'typ': 'Specifická frekvence',
          'hodina': 8,
        },
      ],
    };
    validator.validate(data);
  });

  it('should accept time specification with some keys', () => {
    const validator = new OFNCAL.TimeSpecValidator();
    const data = {
      'typ': 'Časová specifikace',
      'časový_interval': {
        'typ': 'Časový interval',
        'začátek': {
          'typ': 'Časový okamžik',
          'datum': '2021-01-01',
        },
        'konec': {
          'typ': 'Časový okamžik',
          'datum': '2021-02-01',
        },
      },
      'časová_doba': [
        {
          'typ': 'Časová doba',
          'čas': '12:00:00',
        }, {
          'typ': 'Časová doba',
          'čas': '12:00:00',
        },
      ],
      'den_v_týdnu': [
        'https://data.mvcr.gov.cz/zdroj/číselníky/dny-v-týdnu/položky/pondělí',
        'https://data.mvcr.gov.cz/zdroj/číselníky/dny-v-týdnu/položky/pátek',
      ],
      'časové období': [
        'http://publications.europa.eu/resource/authority/timeperiod/OCT',
        'http://publications.europa.eu/resource/authority/timeperiod/DEC',
      ],
    };
    validator.validate(data);
  });

  it('should accept time specification with type and one key', () => {
    const validator = new OFNCAL.TimeSpecValidator();
    const data = {
      'typ': 'Časová specifikace',
      'den_v_týdnu': [
        'https://data.mvcr.gov.cz/zdroj/číselníky/dny-v-týdnu/položky/pondělí',
        'https://data.mvcr.gov.cz/zdroj/číselníky/dny-v-týdnu/položky/pátek',
      ],
    };
    validator.validate(data);
  });

  it('should accept time specification with type and one key', () => {
    const validator = new OFNCAL.TimeSpecValidator();
    const data = {
      'typ': 'Časová specifikace',
      'časový_interval': {
        'typ': 'Časový interval',
        'začátek': {
          'typ': 'Časový okamžik',
          'datum': '2021-01-01',
        },
        'konec': {
          'typ': 'Časový okamžik',
          'datum': '2021-02-01',
        },
      },
    };
    validator.validate(data);
  });

  it('should accept time specification with type and one key', () => {
    const validator = new OFNCAL.TimeSpecValidator();
    const data = {
      'typ': 'Časová specifikace',
      'počet_opakování': 3,
    };
    validator.validate(data);
  });

});
