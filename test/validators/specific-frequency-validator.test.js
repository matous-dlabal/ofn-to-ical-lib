describe('SpecificFrequencyValidator', () => {
  it('should reject non object data', () => {
    const validator = new OFNCAL.SpecificFrequencyValidator();
    const data = 'data';
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject array', () => {
    const validator = new OFNCAL.SpecificFrequencyValidator();
    const data = ['data', 'xxx'];
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject null', () => {
    const validator = new OFNCAL.SpecificFrequencyValidator();
    const data = null;
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject empty object', () => {
    const validator = new OFNCAL.SpecificFrequencyValidator();
    const data = {};
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  // --------------------------------------------------------------------------
  it('should reject data without typ', () => {
    const validator = new OFNCAL.SpecificFrequencyValidator();
    const data = {
      'minuta': 0,
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject data when typ is a number', () => {
    const validator = new OFNCAL.SpecificFrequencyValidator();
    const data = {
      'typ': 42,
      'minuta': 0,
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  // --------------------------------------------------------------------------
  it('should reject data when key is not a number', () => {
    const validator = new OFNCAL.SpecificFrequencyValidator();
    const data = {
      'typ': 'Specifická frekvence',
      'minuta': '0',
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject data when key is not a number', () => {
    const validator = new OFNCAL.SpecificFrequencyValidator();
    const data = {
      'typ': 'Specifická frekvence',
      'hodina': '0',
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject data when key is not a number', () => {
    const validator = new OFNCAL.SpecificFrequencyValidator();
    const data = {
      'typ': 'Specifická frekvence',
      'den_v_měsíci': '0',
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject data when key is not a number', () => {
    const validator = new OFNCAL.SpecificFrequencyValidator();
    const data = {
      'typ': 'Specifická frekvence',
      'týden_v_měsíci': '0',
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject data when key is not a number', () => {
    const validator = new OFNCAL.SpecificFrequencyValidator();
    const data = {
      'typ': 'Specifická frekvence',
      'týden_v_roce': '0',
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject data when key is not a number', () => {
    const validator = new OFNCAL.SpecificFrequencyValidator();
    const data = {
      'typ': 'Specifická frekvence',
      'rok_v_desetiletí': '0',
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  it('should reject data when key is not a number', () => {
    const validator = new OFNCAL.SpecificFrequencyValidator();
    const data = {
      'typ': 'Specifická frekvence',
      'rok_ve_století': '0',
    };
    assert.throws(
        () => validator.validate(data),
        {name: 'InvalidDataStructureError'}
    );
  });

  // --------------------------------------------------------------------------
  it('should accept data with all keys', () => {
    const validator = new OFNCAL.SpecificFrequencyValidator();
    const data = {
      'typ': 'Specifická frekvence',
      'minuta': 0,
      'hodina': 0,
      'den_v_měsíci': 0,
      'týden_v_měsíci': 0,
      'týden_v_roce': 0,
      'rok_v_desetiletí': 0,
      'rok_ve_století': 0,
    };
    validator.validate(data);
  });

  it('should accept data with some keys', () => {
    const validator = new OFNCAL.SpecificFrequencyValidator();
    const data = {
      'typ': 'Specifická frekvence',
      'hodina': 0,
      'týden_v_roce': 0,
      'rok_ve_století': 0,
    };
    validator.validate(data);
  });

  it('should accept data with only typ', () => {
    const validator = new OFNCAL.SpecificFrequencyValidator();
    const data = {
      'typ': 'Specifická frekvence',
    };
    validator.validate(data);
  });

  it('should accept data with only typ and one key', () => {
    const validator = new OFNCAL.SpecificFrequencyValidator();
    const data = {
      'typ': 'Specifická frekvence',
      'minuta': 0,
    };
    validator.validate(data);
  });

  it('should accept data with only typ and one key', () => {
    const validator = new OFNCAL.SpecificFrequencyValidator();
    const data = {
      'typ': 'Specifická frekvence',
      'hodina': 0,
    };
    validator.validate(data);
  });

  it('should accept data with only typ and one key', () => {
    const validator = new OFNCAL.SpecificFrequencyValidator();
    const data = {
      'typ': 'Specifická frekvence',
      'den_v_měsíci': 0,
    };
    validator.validate(data);
  });

  it('should accept data with only typ and one key', () => {
    const validator = new OFNCAL.SpecificFrequencyValidator();
    const data = {
      'typ': 'Specifická frekvence',
      'týden_v_měsíci': 0,
    };
    validator.validate(data);
  });

  it('should accept data with only typ and one key', () => {
    const validator = new OFNCAL.SpecificFrequencyValidator();
    const data = {
      'typ': 'Specifická frekvence',
      'týden_v_roce': 0,
    };
    validator.validate(data);
  });

  it('should accept data with only typ and one key', () => {
    const validator = new OFNCAL.SpecificFrequencyValidator();
    const data = {
      'typ': 'Specifická frekvence',
      'rok_v_desetiletí': 0,
    };
    validator.validate(data);
  });

  it('should accept data with only typ and one key', () => {
    const validator = new OFNCAL.SpecificFrequencyValidator();
    const data = {
      'typ': 'Specifická frekvence',
      'rok_ve_století': 0,
    };
    validator.validate(data);
  });

});
