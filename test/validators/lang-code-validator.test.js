describe('LangCodeValidator', () => {
  it('should accept corect code "cs"', () => {
    const validator = new OFNCAL.LangCodeValidator();
    validator.validate('cs');
  });
  it('should accept corect code "en"', () => {
    const validator = new OFNCAL.LangCodeValidator();
    validator.validate('en');
  });
  it('should accept corect code "ur" (Urdu)', () => {
    const validator = new OFNCAL.LangCodeValidator();
    validator.validate('ur');
  });

  it('should reject code "xx"', () => {
    const validator = new OFNCAL.LangCodeValidator();
    assert.throws(
        () => validator.validate('xx'),
        {name: 'InvalidLangCodeError'}
    );
  });
  it('should reject code "in"', () => {
    const validator = new OFNCAL.LangCodeValidator();
    assert.throws(
        () => validator.validate('in'),
        {name: 'InvalidLangCodeError'}
    );
  });
  it('should reject code "cp"', () => {
    const validator = new OFNCAL.LangCodeValidator();
    assert.throws(
        () => validator.validate('cp'),
        {name: 'InvalidLangCodeError'}
    );
  });
  it('should reject code "cs en"', () => {
    const validator = new OFNCAL.LangCodeValidator();
    assert.throws(
        () => validator.validate('cs en'),
        {name: 'InvalidLangCodeError'}
    );
  });
  it('should reject number 7', () => {
    const validator = new OFNCAL.LangCodeValidator();
    assert.throws(
        () => validator.validate(7),
        {name: 'InvalidLangCodeError'}
    );
  });
});

