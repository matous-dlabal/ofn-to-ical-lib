/**
 * Exportér kalendářů.
 * @interface
 */
OFNCAL.CalendarExporter = class CalendarExporter {

  /**
   * Exportuje obecnou strukturu kalendáře do konkrétního formátu.
   * @param {OFNCAL.Calendar} calendar - Kalendář pro export.
   */
  // eslint-disable-next-line require-jsdoc, space-before-function-paren
  exportCalendar /* istanbul ignore next */ (calendar) {
    throw new Error('CalendarExporter is just an interface.');
  }

};

/**
 * Product identifier -- ID aplikace, která generuje kalendáře
 *  formátu iCalendar.
 * @type {string}
 */
OFNCAL.PRODID = 'https://gitlab.com/matous-dlabal/ofn-to-ical-lib';

/**
 * Exportér kalendářů do formátu iCalendar.
 * @extends OFNCAL.CalendarExporter
 */
OFNCAL.ICalCalendarExporter =
class ICalCalendarExporter extends OFNCAL.CalendarExporter {

  /**
   * Exportuje obecnou strukturu kalendáře do konkrétního formátu iCalendar.
   * @param {OFNCAL.Calendar} calendar - Kalendář pro export.
   * @return {string} Řetězec s kalendářem ve formátu iCalendar.
   */
  exportCalendar(calendar) {
    const vcalendar = new ICAL.Component(['vcalendar', [], []]);
    vcalendar.updatePropertyWithValue('version', '2.0');
    vcalendar.updatePropertyWithValue('prodid', OFNCAL.PRODID);

    for (const e of calendar.events) {
      const vevent = new ICAL.Component('vevent');
      const iCalEvent = new ICAL.Event(vevent);
      vevent.updatePropertyWithValue('dtstamp',
          ICAL.Time.fromJSDate(new Date(), true));

      iCalEvent.uid = e.id;
      iCalEvent.summary = e.title;
      if (e.description !== '') {
        iCalEvent.description = e.description;
      }

      iCalEvent.startDate = e.start;
      if (e.end) {
        iCalEvent.endDate = e.end;
      }
      if (e.recur) {
        vevent.updatePropertyWithValue('rrule', new ICAL.Recur(e.recur));
      }

      vcalendar.addSubcomponent(vevent);
    }

    return vcalendar.toString();
  }

};
