/**
 * Zpracovává časový interval.
 */
OFNCAL.IntervalConverter = class IntervalConverter {

  // eslint-disable-next-line require-jsdoc
  constructor() {
    this.momentConverter = new OFNCAL.MomentConverter();
  }

  /**
   * Zpracuje časový interval.
   * Vždy nastaví hodnotu eventData.start -- pokud chybí, doplní ji hodnotou
   * {@link OFNCAL.TimeSpecConverter.INITIAL_DATE}.
   * Hodnota eventData.end může a nemusí být nastavena.
   *
   * @param {object} interval - Data dle specifikace OFN Časový interval.
   * @param {object} eventData - Data kalendářové události.
   * @return {undefined}
   * @throws {OFNCAL.InvalidTimeSpecificationError}
   *  Pokud je typ hodnoty začátku jiný,
   *  než typ hodnoty konce (datum / datum a čas).
   *  (Typ 'nespecifikovaný' se bere, jako kdyby začátek/konec chyběl).
   *  <br>
   *  Pokud hodnota začátku časově následuje hodnotu konce.
   * @throws {Error} Když se nepodaří načíst hodnota ICAL.Time.
   */
  convert(interval, eventData) {
    let startType;
    let endType;
    if (OFNCAL.Interval.END in interval) {
      endType = this.momentConverter.setEndFromMoment(
          interval[OFNCAL.Interval.END],
          eventData
      );
      // pokud konec chybí nebo je nespecifikovaný, zůstává this.end undefined
    }

    if (OFNCAL.Interval.START in interval) {
      startType = this.momentConverter.setStartFromMoment(
          interval[OFNCAL.Interval.START],
          eventData
      );
    }

    if (!eventData.start) {
      // start chybí nebo je nespecifikovaný
      eventData.start = OFNCAL.TimeSpecConverter.INITIAL_DATE;
      startType = OFNCAL.Moment.DATE;
      if (eventData.end && eventData.start.compare(eventData.end) === 1) {
        // Je nastaven konec, který předchází INITIAL_DATE
        eventData.start.year = eventData.end.year;
      }

      if (endType === OFNCAL.Moment.DATETIME) {
        // typ začátku musí být stejný, jako typ konce
        eventData.start.isDate = false;
        startType = OFNCAL.Moment.DATETIME;
      }
    }

    if (eventData.end) {
      if (startType !== endType) {
        throw new OFNCAL.InvalidTimeSpecificationError(
            'interval - start type must equal end type (date/datetime)'
        );
      }

      // start > end
      if (eventData.start.compare(eventData.end) === 1) {
        throw new OFNCAL.InvalidTimeSpecificationError(
            'interval - end precedes start'
        );
      }
    }
  }

};
