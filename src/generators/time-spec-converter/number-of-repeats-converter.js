/**
 * Zpracovává počet opakování
 */
OFNCAL.NumberOfRepeatsConverter = class NumberOfRepeatsConverter {

  /**
   * Zpracuje číselnou hodnotu počet opakování - přidá popis události.
   * Upraví objekt eventData.
   *
   * @param {number} numberOfRepeats - Počet opakování
   * @param {string} langCode - Kód jazyka
   * @param {object} eventData - Data kalendářové události
   * @return {undefined}
   */
  convert(numberOfRepeats, langCode, eventData) {
    OFNCAL.TimeSpecConverter.setStart(eventData);

    const langDesc = OFNCAL.TimeSpecConverter.getTimeSpecDescriptions(
        OFNCAL.TimeSpec.NUMBER_OF_REPEATS,
        langCode
    );
    eventData.desc.push(langDesc(numberOfRepeats));
  }

};
