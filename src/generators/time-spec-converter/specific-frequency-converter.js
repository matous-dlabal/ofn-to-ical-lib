/**
 * Zpracovává specifickou frekvenci.
 */
OFNCAL.SpecificFrequencyConverter = class SpecificFrequencyConverter {

  /**
   * Zpracuje specifickou frekvenci.
   * Upraví objekt eventData.
   * <br>
   * Pokud je předané pole prázdné, metoda nic nedělá.
   *
   * @param {Array<object>} frequencies - Pole specifických frekvencí.
   * @param {string} langCode - Kód jazyka
   * @param {object} eventData - Data kalendářové události.
   * @return {undefined}
   * @throws {OFNCAL.InvalidTimeSpecificationError}
   *  Pokud je některá hodnota mimo povolený rozsah (např. hodina 30).
   *  <br>
   *  Pokud se zvolený rok desetiletí/století nenachází v intervalu
   *  (pouze pokud se rok zaznamenává jako YEARLY interval, ne jako popis).
   */
  convert(frequencies, langCode, eventData) {
    if (frequencies.length === 0) return;
    OFNCAL.TimeSpecConverter.setStart(eventData);

    const freq = {};
    freq[OFNCAL.SpecificFrequency.MINUTE] = new Set(),
    freq[OFNCAL.SpecificFrequency.HOUR] = new Set(),
    freq[OFNCAL.SpecificFrequency.DAY_OF_MONTH] = new Set(),
    freq[OFNCAL.SpecificFrequency.WEEK_OF_MONTH] = new Set(),
    freq[OFNCAL.SpecificFrequency.WEEK_OF_YEAR] = new Set(),
    freq[OFNCAL.SpecificFrequency.YEAR_OF_DECADE] = new Set(),
    freq[OFNCAL.SpecificFrequency.YEAR_OF_CENTURY] = new Set(),
    // sezbírat všechny hodnoty na jedno místo
    frequencies.forEach((frequency) => {
      Object.keys(freq).forEach((key) => {
        if (key in frequency) {
          freq[key].add(frequency[key]);
        }
      });
    });

    this._processSpecificFreqMinutes(
        freq[OFNCAL.SpecificFrequency.MINUTE],
        eventData
    );
    this._processSpecificFreqHours(
        freq[OFNCAL.SpecificFrequency.HOUR],
        eventData
    );
    this._processSpecificFreqDaysOfMonth(
        freq[OFNCAL.SpecificFrequency.DAY_OF_MONTH],
        eventData
    );
    this._processSpecificFreqWeeksOfMonth(
        freq[OFNCAL.SpecificFrequency.WEEK_OF_MONTH],
        langCode,
        eventData
    );
    this._processSpecificFreqWeeksOfYear(
        freq[OFNCAL.SpecificFrequency.WEEK_OF_YEAR],
        eventData
    );
    this._processSpecificFreqYearsOfDecade(
        freq[OFNCAL.SpecificFrequency.YEAR_OF_DECADE],
        langCode,
        eventData
    );
    const oneYearOfDecade = freq[OFNCAL.SpecificFrequency.YEAR_OF_DECADE].size === 1;
    this._processSpecificFreqYearsOfCentury(
        freq[OFNCAL.SpecificFrequency.YEAR_OF_CENTURY],
        oneYearOfDecade, // pokud platí, už jen přidat popis
        langCode,
        eventData
    );
  }

  // eslint-disable-next-line require-jsdoc
  _processSpecificFreqMinutes(minutes, eventData) {
    minutes = Array.from(minutes);
    if (minutes.length === 0) return;
    if (!minutes.every((min) => (0 <= min && min <= 59))) {
      throw new OFNCAL.InvalidTimeSpecificationError(
          'Specific frequency - illegal minute value'
      );
    }
    OFNCAL.TimeSpecConverter.prepareForRecurrence(eventData);
    eventData.recur.byminute = minutes;
    OFNCAL.TimeSpecConverter.addTimeComponent(eventData);
  }

  // eslint-disable-next-line require-jsdoc
  _processSpecificFreqHours(hours, eventData) {
    hours = Array.from(hours);
    if (hours.length === 0) return;
    if (!hours.every((h) => (0 <= h && h <= 23))) {
      throw new OFNCAL.InvalidTimeSpecificationError(
          'Specific frequency - illegal hour value'
      );
    }
    OFNCAL.TimeSpecConverter.prepareForRecurrence(eventData);
    eventData.recur.byhour = hours;
    OFNCAL.TimeSpecConverter.addTimeComponent(eventData);
  }

  // eslint-disable-next-line require-jsdoc
  _processSpecificFreqDaysOfMonth(daysOfMonth, eventData) {
    daysOfMonth = Array.from(daysOfMonth);
    if (daysOfMonth.length === 0) return;
    if (!daysOfMonth.every((day) => (1 <= day && day <= 31))) {
      throw new OFNCAL.InvalidTimeSpecificationError(
          'Specific frequency - illegal day of month value'
      );
    }
    OFNCAL.TimeSpecConverter.prepareForRecurrence(eventData);
    eventData.recur.bymonthday = daysOfMonth;

    // TODO: shift?
  }

  // eslint-disable-next-line require-jsdoc
  _processSpecificFreqWeeksOfMonth(weeksOfMonth, langCode, eventData) {
    weeksOfMonth = Array.from(weeksOfMonth);
    if (weeksOfMonth.length === 0) return;
    if (!weeksOfMonth.every((week) => (1 <= week && week <= 5))) {
      throw new OFNCAL.InvalidTimeSpecificationError(
          'Specific frequency - illegal week of month value'
      );
    }
    this._addDescription(
        OFNCAL.SpecificFrequency.WEEK_OF_MONTH,
        weeksOfMonth,
        langCode,
        eventData
    );
  }

  // eslint-disable-next-line require-jsdoc
  _processSpecificFreqWeeksOfYear(weeksOfYear, eventData) {
    weeksOfYear = Array.from(weeksOfYear);
    if (weeksOfYear.length === 0) return;
    if (!weeksOfYear.every((week) => (1 <= week && week <= 53))) {
      throw new OFNCAL.InvalidTimeSpecificationError(
          'Specific frequency - illegal week of year value'
      );
    }
    OFNCAL.TimeSpecConverter.prepareForRecurrence(eventData);
    eventData.recur.byweekno = weeksOfYear;
    eventData.recur.freq = 'YEARLY';

    // TODO: shift?
  }

  // eslint-disable-next-line require-jsdoc
  _processSpecificFreqYearsOfDecade(yearsOfDecade, langCode, eventData) {
    yearsOfDecade = Array.from(yearsOfDecade);
    if (yearsOfDecade.length === 0) return;
    if (!yearsOfDecade.every((year) => (0 <= year && year <= 9))) {
      throw new OFNCAL.InvalidTimeSpecificationError(
          'Specific frequency - illegal year of decade value'
      );
    }
    if (yearsOfDecade.length === 1) {
      this._setYearsRecurrence(yearsOfDecade[0], 10, eventData);
    } else {
      this._addDescription(
          OFNCAL.SpecificFrequency.YEAR_OF_DECADE,
          yearsOfDecade,
          langCode,
          eventData
      );
    }
  }

  // eslint-disable-next-line require-jsdoc
  _processSpecificFreqYearsOfCentury(yearsOfCentury, onlyDescription, langCode, eventData) {
    yearsOfCentury = Array.from(yearsOfCentury);
    if (yearsOfCentury.length === 0) return;
    if (!yearsOfCentury.every((year) => (0 <= year && year <= 99))) {
      throw new OFNCAL.InvalidTimeSpecificationError(
          'Specific frequency - illegal year of century value'
      );
    }

    if (yearsOfCentury.length === 1 && !onlyDescription) {
      this._setYearsRecurrence(yearsOfCentury[0], 100, eventData);
    } else {
      this._addDescription(
          OFNCAL.SpecificFrequency.YEAR_OF_CENTURY,
          yearsOfCentury,
          langCode,
          eventData
      );
    }
  }

  /**
   * Nastaví se opakování s roční frekvencí a daným intervalem.
   * Pokud neexistuje ani jedno pravidlo byday/bymonth/bymontday/byweekno,
   * nastaví se byday na všechny dny v týdnu.
   * @param {number} year - rok, kdy má docházet k opakování.
   * @param {number} interval - 10/100 let.
   * @param {object} eventData - Data kalendářové události.
   * @return {undefined}
   * @private
   */
  _setYearsRecurrence(year, interval, eventData) {
    OFNCAL.TimeSpecConverter.prepareForRecurrence(eventData);
    eventData.recur.freq = 'YEARLY';
    eventData.recur.interval = interval;

    if (!eventData.recur.byday &&
        !eventData.recur.bymonth &&
        !eventData.recur.bymonthday &&
        !eventData.recur.byweekno) {
      eventData.recur.byday = new Set(
          ['MO', 'TU', 'WE', 'TH', 'FR', 'SA', 'SU']
      );
    }
    OFNCAL.TimeSpecConverter.shiftYear(year, interval, eventData);
  }

  /**
   * Přidá popis do eventData pro daný typ.
   * Funkci pro tvorbu popisu předá parametr values.
   * @param {string} type - Typ (např. 'rok_v_desetiletí')
   * @param {Array} values - Hodnoty do popisu
   * @param {string} langCode - Kód jazyka
   * @param {object} eventData - Data kalendářové události
   * @return {undefined}
   * @private
   */
  _addDescription(type, values, langCode, eventData) {
    const langDesc = OFNCAL.TimeSpecConverter.getTimeSpecDescriptions(
        OFNCAL.TimeSpec.SPECIFIC_FREQUENCY,
        langCode
    );
    eventData.desc.push(langDesc[type](values));
  }

};
