OFNCAL.TimePeriodConverter = class TimePeriodConverter {

  /**
   * Objekt pro převod hodnot dnů z číselníku time period
   * na hodnoty akceptované formátem iCalendar.
   * <br>
   * Např.: PERIOD_DAY_TO_ICAL_DAY['MON'] vrací 'MO'
   * @const
   * @type {object}
   */
  get PERIOD_DAY_TO_ICAL_DAY() {
    return {
      'MON': 'MO',
      'TUE': 'TU',
      'WED': 'WE',
      'THU': 'TH',
      'FRI': 'FR',
      'SAT': 'SA',
      'SUN': 'SU',
    };
  }

  /**
   * Seznam pracovních dní (Po-Pá)
   * ve formátu číselníku time period (např.: 'MON').
   * @const
   * @type {Array<string>}
   */
  get PERIOD_WORKING_DAYS() {
    return ['MON', 'TUE', 'WED', 'THU', 'FRI'];
  }

  /**
   * Objekt pro převod hodnot měsíců z číselníku time period
   * na hodnoty akceptované formátem iCalendar.
   * <br>
   * Např.: PERIOD_MONTH_TO_ICAL_MONTH['DEC'] vrací 12
   * @const
   * @type {object}
   */
  get PERIOD_MONTH_TO_ICAL_MONTH() {
    return {
      'JAN': 1,
      'FEB': 2,
      'MAR': 3,
      'APR': 4,
      'MAY': 5,
      'JUN': 6,
      'JUL': 7,
      'AUG': 8,
      'SEP': 9,
      'OCT': 10,
      'NOV': 11,
      'DEC': 12,
    };
  }

  /**
   * Zpracuje hodnoty číselníku time period.
   * Upraví objekt eventData.
   *
   * @param {object} timePeriodArray - Pole hodnot pro zpracování.
   * @param {string} langCode - Kód jazyka
   * @param {object} eventData - Data kalendářové události.
   * @return {undefined}
   * @throws {OFNCAL.InvalidTimeSpecificationError}
   *  Pokud tvoří hodnoty s dalšími časovými údaji (interval/okamžik)
   *  prázdný průnik.
   *  <br>
   *  Pokud zadané hodnoty nejsou z číselníku time period.
   */
  convert(timePeriodArray, langCode, eventData) {
    timePeriodArray = timePeriodArray
        .map(OFNCAL.TimeSpecConverter.getValueFromVocabularyUrl)
        // tyto hodnoty se ignorují
        .filter((e) => e !== 'UNKNOWN' && e !== 'UNLIMITED');

    // omezit dny
    this._processTimePeriodDaysOfWeek(timePeriodArray, langCode, eventData);
    // odebrat hodnoty, které omezují dny
    timePeriodArray = timePeriodArray.filter(
        (e) => !(e in this.PERIOD_DAY_TO_ICAL_DAY || e === 'WORKING_DAY')
    );

    // délka trvání
    this._processTimePeriodDuration(timePeriodArray, langCode, eventData);
    // odebrat hodnoty, které určují délku trvání
    const durationKeys = OFNCAL.TimeSpecConverter.getTimeSpecDescriptions(
        OFNCAL.TimeSpec.TIME_PERIOD,
        langCode
    );
    timePeriodArray = timePeriodArray
        .filter((e) => !(e in durationKeys));

    // omezit měsíce
    this._processTimePeriodMonths(timePeriodArray, eventData);
    // odebrat hodnoty, které omezují měsíce
    timePeriodArray = timePeriodArray
        .filter((e) => {
          return !(e in this.PERIOD_MONTH_TO_ICAL_MONTH) &&
                 !(e === 'SPRING' ||
                   e === 'SUMMER' ||
                   e === 'AUTUMN' ||
                   e === 'WINTER');
        });

    // všechny platné hodnoty by už měly být odfiltrovány
    if (timePeriodArray.length > 0) {
      throw new OFNCAL.InvalidTimeSpecificationError(
          'time period - illegal value'
      );
    }
  }

  /**
   * Zpracuje hodnoty číselníku time period určující dny.
   * Přídá je do množiny eventData.recur.byday a pokud je potřeba,
   * posune eventData.start na nejbližší následující den,
   * který je mezi zpracovanými hodnotami.
   *
   * @param {Array<string>} timePeriodArray - Pole hodnot pro zpracování.
   * @param {string} langCode - Kód jazyka
   * @param {object} eventData - Data kalendářové události.
   * @return {undefined}
   * @throws {OFNCAL.InvalidTimeSpecificationError}
   *  Pokud tvoří hodnoty s dalšími časovými údaji (interval/okamžik)
   *  prázdný průnik.
   *  <br>
   *  Pokud jsou zároveň specifikovány pracovní dny a den/dny víkendu.
   * @private
   */
  _processTimePeriodDaysOfWeek(timePeriodArray, langCode, eventData) {
    let daysOfWeek = timePeriodArray
        .filter((e) => e in this.PERIOD_DAY_TO_ICAL_DAY);

    if (timePeriodArray.includes('WORKING_DAY')) {
      daysOfWeek = daysOfWeek.concat(this.PERIOD_WORKING_DAYS);

      // přidat popis
      const langDesc = OFNCAL.TimeSpecConverter.getTimeSpecDescriptions(
          OFNCAL.TimeSpec.TIME_PERIOD,
          langCode
      );
      eventData.desc.push(langDesc['WORKING_DAY']);
    }

    if (timePeriodArray.includes('WORKING_DAY') &&
      (daysOfWeek.includes('SAT') || daysOfWeek.includes('SUN'))) {
      throw new OFNCAL.InvalidTimeSpecificationError(
          'time period (days of week) - working days specified along side SA/SU'
      );
    }

    if (daysOfWeek.length === 0) return;
    OFNCAL.TimeSpecConverter.prepareForRecurrence(eventData);
    eventData.recur.byday = new Set(
        daysOfWeek.map((day) => this.PERIOD_DAY_TO_ICAL_DAY[day])
    );
    OFNCAL.TimeSpecConverter.shiftByDay(eventData);
  }

  /**
   * Zpracuje hodnoty číselníku time period, které vymezují délku trvání.
   * Do eventData.desc přidá jen popis pro nejkratší dobu ze seznamu.
   * <br>
   * Pokud je předané pole prázdné, metoda nic nedělá.
   *
   * @param {Array<string>} timePeriodArray - Pole hodnot pro zpracování.
   * @param {string} langCode - Kód jazyka
   * @param {object} eventData - Data kalendářové události.
   * @return {undefined}
   * @private
   */
  _processTimePeriodDuration(timePeriodArray, langCode, eventData) {
    if (timePeriodArray.length === 0) return;
    OFNCAL.TimeSpecConverter.setStart(eventData);
    const langDesc = OFNCAL.TimeSpecConverter.getTimeSpecDescriptions(
        OFNCAL.TimeSpec.TIME_PERIOD,
        langCode
    );
    if (timePeriodArray.includes('SEC')) {
      eventData.desc.push(langDesc['SEC']);

    } else if (timePeriodArray.includes('MIN')) {
      eventData.desc.push(langDesc['MIN']);

    } else if (timePeriodArray.includes('HOUR')) {
      eventData.desc.push(langDesc['HOUR']);

    } else if (timePeriodArray.includes('CALENDAR_DAY')) {
      eventData.desc.push(langDesc['CALENDAR_DAY']); // 00:00 - 23:59

    } else if (timePeriodArray.includes('DAY')) {
      eventData.desc.push(langDesc['DAY']); // 24h

    } else if (timePeriodArray.includes('WEEK')) {
      eventData.desc.push(langDesc['WEEK']);

    } else if (timePeriodArray.includes('MONTH')) {
      eventData.desc.push(langDesc['MONTH']);

    } else if (timePeriodArray.includes('QUARTER')) {
      eventData.desc.push(langDesc['QUARTER']);

    } else if (timePeriodArray.includes('YEAR_HALF')) {
      eventData.desc.push(langDesc['YEAR_HALF']);

    } else if (timePeriodArray.includes('YEAR')) {
      eventData.desc.push(langDesc['YEAR']);
    }
  }

  /**
   * Zpracuje hodnoty číselníku time period, které vymezují měsíce.
   * Upraví objekt eventData.
   * <br>
   * Pokud je předané pole prázdné, metoda nic nedělá.
   *
   * @param {Array<string>} timePeriodArray - Pole hodnot pro zpracování.
   * @param {object} eventData - Data kalendářové události.
   * @return {undefined}
   * @throws {OFNCAL.InvalidTimeSpecificationError}
   *  Pokud tvoří hodnoty s dalšími časovými údaji (interval/okamžik)
   *  prázdný průnik.
   * @private
   */
  _processTimePeriodMonths(timePeriodArray, eventData) {
    if (timePeriodArray.length === 0) return;
    OFNCAL.TimeSpecConverter.prepareForRecurrence(eventData);

    if (!eventData.recur.bymonth) {
      eventData.recur.bymonth = new Set();
    }
    timePeriodArray
        .forEach((timePeriod) => {
          if (timePeriod in this.PERIOD_MONTH_TO_ICAL_MONTH) {
            eventData.recur.bymonth.add(this.PERIOD_MONTH_TO_ICAL_MONTH[timePeriod]);
          }
          if (timePeriod === 'SPRING') {
            eventData.recur.bymonth.add(3).add(4).add(5);
          }
          if (timePeriod === 'SUMMER') {
            eventData.recur.bymonth.add(6).add(7).add(8);
          }
          if (timePeriod === 'AUTUMN') {
            eventData.recur.bymonth.add(9).add(10).add(11);
          }
          if (timePeriod === 'WINTER') {
            eventData.recur.bymonth.add(12).add(1).add(2);
          }
        });
    if (eventData.recur.bymonth.size > 0) {
      OFNCAL.TimeSpecConverter.shiftByMonth(eventData);
    } else {
      delete eventData.recur.bymonth;
    }
  }

};
