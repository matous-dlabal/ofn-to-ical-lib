/**
 * Zpracovává časovou dobu.
 */
OFNCAL.TimeLapseConverter = class TimeLapseConverter {

  /**
   * Zpracuje časovou dobu (pouze první v seznamu).
   * Upraví objekt eventData.
   * <br>
   * Pokud je předané pole prázdné, metoda nic nedělá.
   *
   * @param {Array<object>} timeLapse - Pole časových dob pro zpracování.
   * @param {object} eventData - Data kalendářové události.
   * @return {undefined}
   * @throws {OFNCAL.InvalidTimeSpecificationError}
   *  Pokud časová doba vůbec není v intervalu (start; end).
   */
  convert(timeLapse, eventData) {
    if (timeLapse.length === 0) return;
    timeLapse = timeLapse[0];
    OFNCAL.TimeSpecConverter.prepareForRecurrence(eventData);
    OFNCAL.TimeSpecConverter.addTimeComponent(eventData);

    let timeFrom;
    let timeTo;
    if (OFNCAL.TimeLapse.FROM in timeLapse) {
      timeFrom = timeLapse[OFNCAL.TimeLapse.FROM];
    } else {
      timeFrom = '00:00:00';
    }
    if (OFNCAL.TimeLapse.TO in timeLapse) {
      timeTo = timeLapse[OFNCAL.TimeLapse.TO];
    } else {
      timeTo = '23:59:59';
    }
    if (OFNCAL.TimeLapse.TIME in timeLapse) {
      timeFrom = timeTo = timeLapse[OFNCAL.TimeLapse.TIME];
    }

    const getDateWithoutTime = OFNCAL.TimeSpecConverter.getDateWithoutTime;
    const parseDateTime = OFNCAL.TimeSpecConverter.parseDateTime;

    const startDateString = getDateWithoutTime(eventData.start).toString();
    const newStart = parseDateTime(`${startDateString}T${timeFrom}`);
    const newEnd = parseDateTime(`${startDateString}T${timeTo}`);
    let newUntil = OFNCAL.TimeSpecConverter.FAKE_DATE;
    if (!eventData.recur.until.isFake) {
      const untilDateString = getDateWithoutTime(eventData.recur.until).toString();
      newUntil = parseDateTime(`${untilDateString}T${timeTo}`);
    }

    // newEnd < start
    if (this._compareTime(eventData.start, newEnd) === 1) {
      newStart.day++;
      newEnd.day++;
    }
    if (!eventData.recur.until.isFake) {
      // recur.until < newStart
      if (this._compareTime(eventData.recur.until, newStart) === -1) {
        newUntil.day--;
      }
      // newUntil < newStart
      if (newStart.compare(newUntil) === 1) {
        throw new OFNCAL.InvalidTimeSpecificationError(
            'time lapse not within interval'
        );
      }
    }

    eventData.start = newStart;
    eventData.end = newEnd;
    eventData.recur.until = newUntil;
    eventData._hasTimeLapse = true;
  }

  /**
   * Porovná dvě instance ICAL.Time na základě času.
   * Pro instance, které jsou typu datum je použit čas 00:00:00.
   * @param {ICAL.Time} date1 - První čas k porovnání.
   * @param {ICAL.Time} date2 - Druhý čas k porovnání.
   * @return {number}
   *  -1 - Když je první < druhý
   *  <br>
   *  0 - Když je první == druhý
   *  <br>
   *  +1 - Když je první > druhý
   * @private
   */
  _compareTime(date1, date2) {
    const time1 = date1.clone();
    const time2 = date2.clone();
    // Pokud šlo o datum bez času, nastaví se 00:00:00
    time1.isDate = time2.isDate = false;
    time1.year = time2.year = 1900;
    time1.month = time2.month = 1;
    time1.day = time2.day = 1;

    return time1.compare(time2);
  }

};
