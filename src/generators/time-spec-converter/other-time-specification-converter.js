/**
 * Zpracovává jinou časovou specifikaci.
 */
OFNCAL.OtherTimeSpecConverter = class OtherTimeSpecConverter {

  /**
   * Zpracuje hodnoty číselníku jiná časová specifikace.
   * Upraví objekt eventData.
   * <br>
   * Pokud je předané pole prázdné, metoda nic nedělá.
   *
   * @param {Array<string>} otherTimeSpecArray - Pole hodnot pro zpracování.
   * @param {string} langCode - Kód jazyka
   * @param {object} eventData - Data kalendářové události.
   * @return {undefined}
   * @throws {OFNCAL.InvalidTimeSpecificationError}
   *  Pokud zadané hodnoty nejsou z číselníku jiná časová specifikace.
   */
  convert(otherTimeSpecArray, langCode, eventData) {
    if (otherTimeSpecArray.length === 0) return;
    OFNCAL.TimeSpecConverter.setStart(eventData);

    const langDesc = OFNCAL.TimeSpecConverter.getTimeSpecDescriptions(
        OFNCAL.TimeSpec.OTHER_TIME_SPECIFICATION,
        langCode
    );
    otherTimeSpecArray
        .map(OFNCAL.TimeSpecConverter.getValueFromVocabularyUrl)
        .forEach((e) => {
          const newDesc = langDesc[e];
          if (!newDesc) {
            throw new OFNCAL.InvalidTimeSpecificationError(
                'other time specification - invalid value (no description found)'
            );
          }
          eventData.desc.push(newDesc);
        });
  }

};
