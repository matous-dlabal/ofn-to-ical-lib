/**
 * Zpracovává časová okamžik.
 */
OFNCAL.MomentConverter = class MomentConverter {

  /**
   * Nastaví hodnotu eventData.start ze zadaného časového okamžiku.
   * Pokud je moment nespecifikovaný, start se nenastaví.
   * Upraví objekt eventData.
   *
   * @param {object} moment - Data dle OFN Časový okamžik.
   * @param {object} eventData - Data kalendářové události.
   * @return {string} Typ časového okamžiku --
   *  {@link OFNCAL.Moment.DATE}/
   *  {@link OFNCAL.Moment.DATETIME}/
   *  {@link OFNCAL.Moment.UNSPECIFIED}.
   * @throws {Error} Když se z okamžiku nepodaří načíst ICAL.Time.
   */
  setStartFromMoment(moment, eventData) {
    return this._setAttrFromMoment(moment, 'start', eventData);
  }

  /**
   * Nastaví hodnotu eventData.end ze zadaného časového okamžiku.
   * Pokud je moment nespecifikovaný, end se nenastaví.
   * Upraví objekt eventData.
   *
   * @param {object} moment - Data dle OFN Časový okamžik.
   * @param {object} eventData - Data kalendářové události.
   * @return {string} Typ časového okamžiku --
   *  {@link OFNCAL.Moment.DATE}/
   *  {@link OFNCAL.Moment.DATETIME}/
   *  {@link OFNCAL.Moment.UNSPECIFIED}.
   * @throws {Error} Když se z okamžiku nepodaří načíst ICAL.Time.
   */
  setEndFromMoment(moment, eventData) {
    return this._setAttrFromMoment(moment, 'end', eventData);
  }

  // eslint-disable-next-line require-jsdoc
  _setAttrFromMoment(moment, attr, eventData) {
    const parseDateTime = OFNCAL.TimeSpecConverter.parseDateTime;

    if (OFNCAL.Moment.DATETIME in moment) {
      eventData[attr] = parseDateTime(moment[OFNCAL.Moment.DATETIME]);
      return OFNCAL.Moment.DATETIME;

    } else if (OFNCAL.Moment.DATE in moment) {
      eventData[attr] = parseDateTime(moment[OFNCAL.Moment.DATE]);
      return OFNCAL.Moment.DATE;

    } else if (OFNCAL.Moment.UNSPECIFIED in moment) {
      // nic se nenastavuje
      return OFNCAL.Moment.UNSPECIFIED;
    }
  }

  /**
   * Zpracuje časový okamžik.
   * Pokud je typu {@link OFNCAL.Moment.UNSPECIFIED}, nic nenastaví.
   * Pokud nastaví eventData.start, nastaví i eventData.end.
   *
   * @param {object} moment - Data dle specifikace OFN Časový okamžik.
   * @param {object} eventData - Data kalendářové události.
   * @return {undefined}
   * @throws {Error} Když se nepodaří načíst hodnota ICAL.Time.
   */
  convert(moment, eventData) {
    this.setStartFromMoment(moment, eventData);
    this.setEndFromMoment(moment, eventData);
  }

};
