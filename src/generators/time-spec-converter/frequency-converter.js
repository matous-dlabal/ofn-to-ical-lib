/**
 * Zpracovává frekvenci.
 */
OFNCAL.FrequencyConverter = class FrequencyConverter {

  /**
   * Zpracuje hodnoty z číselníku frekvencí - přidá popis události.
   * Upraví objekt eventData.
   * <br>
   * Pokud je předané pole prázdné, metoda nic nedělá.
   *
   * @param {Array<string>} frequencies - Pole hodnot pro zpracování.
   * @param {string} langCode - Kód jazyka
   * @param {object} eventData - Data kalendářové události.
   * @return {undefined}
   * @throws {OFNCAL.InvalidTimeSpecificationError}
   *  Pokud zadané hodnoty nejsou z číselníku frekvencí.
   */
  convert(frequencies, langCode, eventData) {
    if (frequencies.length === 0) return;
    OFNCAL.TimeSpecConverter.setStart(eventData);

    const langDesc = OFNCAL.TimeSpecConverter.getTimeSpecDescriptions(
        OFNCAL.TimeSpec.FREQUENCY,
        langCode
    );
    frequencies
        .map(OFNCAL.TimeSpecConverter.getValueFromVocabularyUrl)
        .forEach((frequency) => {
          if (frequency === 'UPDATE_CONT') {
            // ignorovat - informace o frekvenci je zbytečná

          } else {
            const newDesc = langDesc[frequency];
            if (!newDesc) {
              throw new OFNCAL.InvalidTimeSpecificationError(
                  'frequency - invalid value (no description found)'
              );
            }
            eventData.desc.push(newDesc);
          }
        });
  }

};
