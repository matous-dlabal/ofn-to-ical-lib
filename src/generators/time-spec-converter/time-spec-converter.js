/**
 * Převádí časovou specifikaci na kalendářovou událost.
 */
OFNCAL.TimeSpecConverter = class TimeSpecConverter {

  /**
   * Objekt pro převod hodnot z číselníku pro dny v týdnu
   * na hodnoty číselníku time period.
   * <br>
   * Např.: DAY_OF_WEEK_CONVERTER['pondělí'] vrací 'MON'
   * @const
   * @type {object}
   */
  get DAY_OF_WEEK_CONVERTER() {
    return {
      'pondělí': 'MON',
      'úterý': 'TUE',
      'středa': 'WED',
      'čtvrtek': 'THU',
      'pátek': 'FRI',
      'sobota': 'SAT',
      'neděle': 'SUN',
    };
  }

  /**
   * Falešné datum (ICAL.Time),
   * které je při porovnávání (metodou ICAL.Time#compare) větší,
   * než kteréholiv jiné.
   * <br>
   * Testovat se dá pomocí vlastnosti isFake, která v ICAL.Time není definovaná
   * a tedy vrací undefined, které se vyhodnotí na false.
   * @const
   * @type {object}
   */
  static get FAKE_DATE() {
    return {
      toUnixTime: () => Infinity,
      isFake: true,
    };
  }

  /**
   * Počáteční datum. Používané v přápadech, kdy nebyl začátek stanoven jinak.
   * @const
   * @type {ICAL.Time}
   */
  static get INITIAL_DATE() {
    const date = OFNCAL.TimeSpecConverter.getDateWithoutTime(ICAL.Time.now());
    date.month = date.day = 1;
    return date;
  }

  /**
   * Jazyk, který se má použít pro popis, pokud neexistuje ve zvoleném jazyku.
   * Je nutné, aby každý popis obsahoval alespoň tento jazyk.
   * @const
   * @type {string}
   */
  static get DEFAULT_LANGUAGE() {
    return 'en';
  }

  /**
   * Vrátí hodnotu (objekt, nebo funkci) popisu daného typu pro zvolený jazyk.
   * Pokud hodnota pro zvolený jazyk neexistuje,
   * použije se jazyk {@link OFNCAL.TimeSpecConverter.DEFAULT_LANGUAGE}.
   * @param {string} type - Typ (např. 'frekvence')
   * @param {string} langCode - Kód jazyka
   * @return {object | function}
   */
  static getTimeSpecDescriptions(type, langCode) {
    const typeDesc = OFNCAL.TimeSpecDescriptions[type];
    return typeDesc[langCode] || typeDesc[this.DEFAULT_LANGUAGE];
  }

  /**
   * Vrátí jen část za posledním lomítkem (pokud žádné není, tak celý vstup).
   * @param {string} url - URL hodnoty z číselníku.
   * @return {string}
   */
  static getValueFromVocabularyUrl(url) {
    return url.split('/').pop();
  }

  // eslint-disable-next-line require-jsdoc
  constructor() {
    this.intervalConverter = new OFNCAL.IntervalConverter();
    this.momentConverter = new OFNCAL.MomentConverter();
    this.timeLapseConverter = new OFNCAL.TimeLapseConverter();
    this.timePeriodConverter = new OFNCAL.TimePeriodConverter();
    this.frequencyConverter = new OFNCAL.FrequencyConverter();
    this.otherTimeSpecConverter = new OFNCAL.OtherTimeSpecConverter();
    this.numberOfRepeatsConverter = new OFNCAL.NumberOfRepeatsConverter();
    this.specificFrequencyConverter = new OFNCAL.SpecificFrequencyConverter();

    this.timeSpecValidator = new OFNCAL.TimeSpecValidator();
  }

  /**
   * Zpracuje data dle OFN Časová specifikace pro použití s formátem iCalendar.
   * @param {object} timeSpec - JSON objekt dle OFN Časová specifikace.
   * @param {string} langCode - Kód jazyka dle normy ISO 639-1 ("cs", "en", ...)
   * @return {object}
   * @throws {OFNCAL.InvalidTimeSpecificationError}
   *  V případě problému s převodem.
   */
  convert(timeSpec, langCode) {
    this.timeSpecValidator.validate(timeSpec);

    const eventData = new OFNCAL.Event();
    eventData._hasTimeLapse = false;

    if (OFNCAL.TimeSpec.INTERVAL in timeSpec) {
      this.intervalConverter.convert(
          timeSpec[OFNCAL.TimeSpec.INTERVAL],
          eventData
      );
    }

    if (OFNCAL.TimeSpec.MOMENT in timeSpec) {
      const moment = timeSpec[OFNCAL.TimeSpec.MOMENT];

      if (OFNCAL.TimeSpec.INTERVAL in timeSpec) {
        this._intersectIntervalAndMoment(moment, eventData);
      } else {
        this.momentConverter.convert(moment, eventData);
      }

      if (OFNCAL.Moment.DATETIME in moment) {
        // DateTime moment už nejde víc upřesnit
        return this._finalizeOutput(eventData);
      }
    }

    if (OFNCAL.TimeSpec.TIME_LAPSE in timeSpec) {
      // Potřeba zpracovat před dny v týdnu a měsíci, aby nedešlo ke zbytečnému
      // odříznutí prvního dne, který kvůli posunu na jiný den v týdnu / měsíc
      // vlastně není první.
      this.timeLapseConverter.convert(
          timeSpec[OFNCAL.TimeSpec.TIME_LAPSE],
          eventData
      );
    }

    let timePeriods = timeSpec[OFNCAL.TimeSpec.TIME_PERIOD] || [];
    // Přidat k časovému období dny v týdnu
    if (OFNCAL.TimeSpec.DAY_OF_WEEK in timeSpec) {
      // Omezení pro dny v týdnu je potřeba zpracovat naráz
      // (dny v týdnu + časové období), jinak by mohlo dojít
      // k předčasnému posunutí startu a přeskočení dne v týdnu,
      // který by byl přídán později.
      const daysOfWeek = timeSpec[OFNCAL.TimeSpec.DAY_OF_WEEK]
          .map(OFNCAL.TimeSpecConverter.getValueFromVocabularyUrl)
          .map((e) => this.DAY_OF_WEEK_CONVERTER[e]);

      if (daysOfWeek.includes(undefined)) {
        throw new OFNCAL.InvalidTimeSpecificationError(
            'days of week - illegal value'
        );
      }
      timePeriods = timePeriods.concat(daysOfWeek);
    }
    this.timePeriodConverter.convert(timePeriods, langCode, eventData);

    if (OFNCAL.TimeSpec.FREQUENCY in timeSpec) {
      this.frequencyConverter.convert(
          timeSpec[OFNCAL.TimeSpec.FREQUENCY],
          langCode,
          eventData
      );
    }

    if (OFNCAL.TimeSpec.OTHER_TIME_SPECIFICATION in timeSpec) {
      this.otherTimeSpecConverter.convert(
          timeSpec[OFNCAL.TimeSpec.OTHER_TIME_SPECIFICATION],
          langCode,
          eventData
      );
    }

    if (OFNCAL.TimeSpec.NUMBER_OF_REPEATS in timeSpec) {
      this.numberOfRepeatsConverter.convert(
          timeSpec[OFNCAL.TimeSpec.NUMBER_OF_REPEATS],
          langCode,
          eventData
      );
    }

    if (OFNCAL.TimeSpec.SPECIFIC_FREQUENCY in timeSpec) {
      this.specificFrequencyConverter.convert(
          timeSpec[OFNCAL.TimeSpec.SPECIFIC_FREQUENCY],
          langCode,
          eventData
      );
    }

    // TODO

    return this._finalizeOutput(eventData);
  }

  /**
   * Pokud tak ještě nebylo učiněno, připraví předaný objekt pro opakování
   * (přidá podobjekt recur s frekvencí DAILY).
   * Pokud není nastaven end, je until nastaveno
   * na {@link OFNCAL.TimeSpecConverter.FAKE_DATE}.
   * @param {object} eventData - objekt s daty události.
   * @return {undefined}
   */
  static prepareForRecurrence(eventData) {
    if (!eventData.recur) {
      this.setStart(eventData);

      eventData.recur = {
        freq: 'DAILY',
      };
      if (eventData.end) {
        eventData.recur.until = eventData.end;
        delete eventData.end;
      } else {
        eventData.recur.until = OFNCAL.TimeSpecConverter.FAKE_DATE;
      }
    }
  }

  /**
   * Upravý předaný objekt tak, aby byl vhodný pro vrácení metodou
   *  {@link OFNCAL.TimeSpecConverter#convert}.
   * @param {object} eventData - Objekt k úpravě.
   * @return {object} Upravený objekt.
   * @private
   */
  _finalizeOutput(eventData) {
    if (!eventData.start) {
      throw new OFNCAL.InvalidTimeSpecificationError(
          'finalizeOutput - start not defined'
      );
    }
    if (!eventData.end) {
      delete eventData.end;
    }
    // if (eventData.desc.length === 0) {
    //   delete eventData.desc;
    // }
    delete eventData._hasTimeLapse;

    if (eventData.recur) {
      if (eventData.recur.until && eventData.recur.until.isFake) {
        delete eventData.recur.until;
      }
      for (const recurKey in eventData.recur) {
        if (eventData.recur[recurKey] instanceof Set) {
          eventData.recur[recurKey] = Array.from(eventData.recur[recurKey]);
        }
      }
    }
    return eventData;
  }

  /**
   * Pokusí se z řetězce načíst informaci o datu a čase.
   * Odebere desetinná místa sekund.
   * @param {string} dateTimeString - Řetězec s datem / datem a časem
   * @return {ICAL.Time} Datum / datum a čas
   * @throws {Error} Pokud {@link ICAL.Time#fromString} nezvládne zpracovat
   *  vstupní řetězec.
   */
  static parseDateTime(dateTimeString) {
    // Oříznout sekundy na celé číslo
    // (xsd:dateTimeStamp umožňuje zadávat sekundy jako desetinné číslo)
    dateTimeString = dateTimeString.replace(/\.\d+/, '');
    return ICAL.Time.fromString(dateTimeString);
  }

  /**
   * Udělá kopii objektu {@link ICAL.Time} a odebere z ní časovou složku.
   * @param {ICAL.Time} datetime - Datum / datum a čas
   * @return {ICAL.Time} Kopii datumu, bez časové složky.
   */
  static getDateWithoutTime(datetime) {
    const date = datetime.clone();
    date.isDate = true;
    return date;
  }

  /**
   * Nastaví předanému datu čas 00:00:00.
   * @param {ICAL.Time} date - Datum / datum a čas k úpravě.
   * @return {undefined}
   */
  static setStartOfDay(date) {
    date.isDate = false;
    date.hour = date.minute = date.second = 0;
  }

  /**
   * Nastaví předanému datu čas 23:59:59.
   * @param {ICAL.Time} date - Datum / datum a čas k úpravě.
   * @return {undefined}
   */
  static setEndOfDay(date) {
    date.isDate = false;
    date.hour = 23;
    date.minute = date.second = 59;
  }

  /**
   * Pokud zatím není, přidá časovou složku pro start, end a until daných dat.
   * Start nastaví na začátek zde, end a until na konec dne.
   * @param {object} eventData - Objekt s daty kalendářové události.
   * @return {undefined}
   */
  static addTimeComponent(eventData) {
    if (eventData.start.isDate) {
      OFNCAL.TimeSpecConverter.setStartOfDay(eventData.start);
      if (eventData.end) {
        OFNCAL.TimeSpecConverter.setEndOfDay(eventData.end);
      }
      if (!eventData.recur.until.isFake) {
        OFNCAL.TimeSpecConverter.setEndOfDay(eventData.recur.until);
      }
    }
  }

  /**
   * Upraví existující hodnoty eventData.start a eventData.end tak,
   * aby obsahovaly průnik intervalu [start; end] se zvoleným okamžikem.
   * Metoda počítá i s jednostranným intervalem [start; inf) -- v případě,
   * kdy není end definován.
   * <br>
   * Pokud má okamžik hodnotu nespecifikovaný, metoda nic nedělá.
   *
   * @param {object} moment - Data dle OFN Časový okamžik.
   * @param {object} eventData - Data kalendářové události.
   * @return {undefined}
   *
   * @throws {OFNCAL.InvalidTimeSpecificationError} Pokud je průnik prázdný.
   * @throws {Error} Pokud se nepodaří načíst datum / datum a čas z řetězců.
   */
  _intersectIntervalAndMoment(moment, eventData) {
    if (OFNCAL.Moment.UNSPECIFIED in moment) return;
    const parseDateTime = OFNCAL.TimeSpecConverter.parseDateTime;
    const getDateWithoutTime = OFNCAL.TimeSpecConverter.getDateWithoutTime;

    if (!eventData.end) {
      eventData.end = OFNCAL.TimeSpecConverter.FAKE_DATE;
    }

    if (OFNCAL.Moment.DATETIME in moment) {
      const momentDateTime = parseDateTime(moment[OFNCAL.Moment.DATETIME]);

      // date / dateTime
      let momentDT = momentDateTime;
      if (eventData.start.isDate) {
        momentDT = getDateWithoutTime(momentDateTime);
      }

      // eventData.start <= momentDT <= eventData.end
      if (momentDT.compare(eventData.start) !== -1 &&
          momentDT.compare(eventData.end) !== 1) {
        // okamžik je uvnitř intervalu
        eventData.start = momentDateTime;
        eventData.end = momentDateTime;
      } else {
        throw new OFNCAL.InvalidTimeSpecificationError(
            'intersectIntervalAndMoment - empty intersection'
        );
      }
    } else if (OFNCAL.Moment.DATE in moment) {
      const momentDate = parseDateTime(moment[OFNCAL.Moment.DATE]);

      const startDate = getDateWithoutTime(eventData.start);
      const endDate =
        eventData.end.isFake ? eventData.end : getDateWithoutTime(eventData.end);

      // momentDate < startDate || endDate < momentDate
      if (momentDate.compare(startDate) === -1 || momentDate.compare(endDate) === 1) {
        throw new OFNCAL.InvalidTimeSpecificationError(
            'intersectIntervalAndMoment - empty intersection'
        );
      }

      // startDate < momentDate
      if (momentDate.compare(startDate) === 1) {
        // oříznout začátek intervalu
        if (eventData.start.isDate) {
          eventData.start = momentDate.clone();
        } else {
          // momentDate + čas 00:00:00
          eventData.start = momentDate.clone();
          OFNCAL.TimeSpecConverter.setStartOfDay(eventData.start);
        }
      }

      // momentDate < endDate
      if (momentDate.compare(endDate) === -1) {
        // oříznout konec intervalu
        if (eventData.start.isDate) {
          eventData.end = momentDate.clone();
        } else {
          // momentDate + čas 23:59:59
          eventData.end = momentDate.clone();
          OFNCAL.TimeSpecConverter.setEndOfDay(eventData.end);
        }
      }
    }
  }

  /**
   * Posunout start tak, aby začínal dnem týdne z množiny recur.byday.
   * Pokud množina neexistuje, nebo je prázdná, metoda nic nedělá.
   * Pokud byla definovaná časová doba, posune se i konec.
   * @param {object} eventData - Data kalendářové události.
   * @return {undefined}
   * @throw {OFNCAL.InvalidTimeSpecificationError}
   *  Pokud se v zadané události nejde posunout na požadovaný den.
   */
  static shiftByDay(eventData) {
    if (!eventData.recur.byday || eventData.recur.byday.size === 0) return;

    const icalDayOfWeek = (date) => {
      const weekStart = ICAL.Time.MONDAY;
      return ICAL.Recur.numericDayToIcalDay(
          date.dayOfWeek(weekStart), weekStart
      );
    };

    while (!eventData.recur.byday.has(icalDayOfWeek(eventData.start))) {
      eventData.start.day++;
      if (!eventData._hasTimeLapse && !eventData.start.isDate) {
        OFNCAL.TimeSpecConverter.setStartOfDay(eventData.start);
      }

      // until < start
      if (eventData.start.compare(eventData.recur.until) === 1) {
        throw new OFNCAL.InvalidTimeSpecificationError(
            'shiftByDay - cannot shift within interval'
        );
      }
    }
    if (eventData._hasTimeLapse) {
      // posunout i end
      eventData.end.year = eventData.start.year;
      eventData.end.month = eventData.start.month;
      eventData.end.day = eventData.start.day;
    }
  }

  /**
   * Posunout start tak, aby začínal měsícem z množiny recur.bymonth.
   * Pokud množina neexistuje, nebo je prázdná, metoda nic nedělá.
   * Pokud byla definovaná časová doba, posune se i konec.
   * Metoda vždy před skončením zavolá {@link OFNCAL.TimeSpecConverter.shiftByDay}.
   * @param {object} eventData - Data kalendářové události.
   * @return {undefined}
   * @throw {OFNCAL.InvalidTimeSpecificationError}
   *  Pokud se v zadané události nejde posunout na požadovaný měsíc.
   */
  static shiftByMonth(eventData) {
    if (!eventData.recur.bymonth || eventData.recur.bymonth.size === 0) {
      OFNCAL.TimeSpecConverter.shiftByDay(eventData);
      return;
    }

    while (!eventData.recur.bymonth.has(eventData.start.month)) {
      eventData.start.month++;
      eventData.start.day = 1;
      if (!eventData._hasTimeLapse && !eventData.start.isDate) {
        OFNCAL.TimeSpecConverter.setStartOfDay(eventData.start);
      }

      // until < start
      if (eventData.start.compare(eventData.recur.until) === 1) {
        throw new OFNCAL.InvalidTimeSpecificationError(
            'shiftByMonth - cannot shift within interval'
        );
      }
    }
    if (eventData._hasTimeLapse) {
      // posunout i end
      eventData.end.year = eventData.start.year;
      eventData.end.month = eventData.start.month;
      eventData.end.day = eventData.start.day;
    }

    OFNCAL.TimeSpecConverter.shiftByDay(eventData);
  }

  /**
   * Posunout start tak, aby rok končil zadaným číslem (jedno-/dvouciferným).
   * Pokud byla definovaná časová doba, posune se i konec.
   * Metoda vždy před skončením zavolá {@link OFNCAL.TimeSpecConverter.shiftByMonth}
   * (pokud jsou zadané správné hodnoty yearEnd a interval).
   *
   * @param {number} yearEnd - Očekávaná koncovka roku (0-9 | 0-99).
   * @param {number} interval - Určuje, jestli chceme ovlivnit
   *  jednu, nebo dvě poslední číslice (10 | 100).
   * @param {object} eventData - Data kalendářové události.
   * @return {undefined}
   * @throw {OFNCAL.InvalidTimeSpecificationError}
   *  Pokud se v zadané události nejde posunout na požadovaný měsíc.
   */
  static shiftYear(yearEnd, interval, eventData) {
    if (interval === 10) {
      if (yearEnd < 0 || 9 < yearEnd) return;
    } else if (interval === 100) {
      if (yearEnd < 0 || 99 < yearEnd) return;
    } else {
      return;
    }

    if (yearEnd < (eventData.start.year % interval)) {
      eventData.start.year += interval;
    }
    eventData.start.year += yearEnd - (eventData.start.year % interval);
    eventData.start.month = eventData.start.day = 1;
    if (!eventData._hasTimeLapse && !eventData.start.isDate) {
      OFNCAL.TimeSpecConverter.setStartOfDay(eventData.start);
    }

    // until < start
    if (eventData.start.compare(eventData.recur.until) === 1) {
      throw new OFNCAL.InvalidTimeSpecificationError(
          'shiftYear - cannot shift within interval'
      );
    }

    if (eventData._hasTimeLapse) {
      // posunout i end
      eventData.end.year = eventData.start.year;
      eventData.end.month = eventData.start.month;
      eventData.end.day = eventData.start.day;
    }
    OFNCAL.TimeSpecConverter.shiftByMonth(eventData);
  }

  /**
   * Pokud není start nastaven, nastaví ho na hodnotu
   * {@link OFNCAL.TimeSpecConverter.INITIAL_DATE}.
   * @param {object} eventData - Data kalendářové události.
   * @return {undefined}
   */
  static setStart(eventData) {
    if (!eventData.start) {
      eventData.start = this.INITIAL_DATE;
    }
  }

};
