/**
 * Generátor kalendářů ({@link OFNCAL.Calendar}) z dat dle OFN.
 * @interface
 */
OFNCAL.CalendarsGenerator = class CalendarsGenerator {

  /**
   * Generuje kalendáře ({@link OFNCAL.Calendar}) z dat dle OFN.
   * @param {string} data - Data dle OFN ve formátu JSON-LD.
   * @param {string} langCode - Kód jazyka dle normy ISO-639-1
   *  ("cs", "en" apod.)
   *
   * @return {Array<OFNCAL.Calendar>} Seznam kalendářů.
   *
   * @throws {OFNCAL.LanguageNotFoundError} Když data neobsahují
   *  požadovaný jazyk.
   * @throws {OFNCAL.TimeSpecificationNotFoundError} Když data neobsahují
   *  časovou specifikaci.
   */
  // eslint-disable-next-line require-jsdoc, space-before-function-paren
  generate /* istanbul ignore next */ (data, langCode) {
    throw new Error('CalendarsGenerator is just an interface.');
  }

};


/**
 * Generátor kalendářů ({@link OFNCAL.Calendar}) z dat dle OFN.
 * @extends OFNCAL.CalendarsGenerator
 */
OFNCAL.BasicCalendarsGenerator =
class BasicCalendarsGenerator extends OFNCAL.CalendarsGenerator {

  // eslint-disable-next-line require-jsdoc
  constructor() {
    super();
    this.eventsGenerator = new OFNCAL.BasicEventsGenerator();
  }

  // Stačí jsdoc z rozhraní
  // eslint-disable-next-line require-jsdoc
  generate(data, langCode) {
    let dataArray = data;
    if (!Array.isArray(data)) {
      dataArray = [data];
    }
    return dataArray.map(
        (e) => new OFNCAL.Calendar(this.eventsGenerator.generate(e, langCode))
    );
  }

};
