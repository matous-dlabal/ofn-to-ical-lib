/**
 * Generátor kalendářových událostí ({@link OFNCAL.Event}).
 * @interface
 */
OFNCAL.EventsGenerator = class EventsGenerator {

  /**
   * Generuje události ({@link OFNCAL.Event}) z dat dle OFN.
   * @param {string} data - Data dle OFN ve formátu JSON-LD.
   * @param {string} langCode - Kód jazyka dle normy ISO 639-1
   *  ("cs", "en" apod.)
   *
   * @return {Array<OFNCAL.Event>} Seznam událostí.
   *
   * @throws {OFNCAL.LanguageNotFoundError} Když data neobsahují
   *  požadovaný jazyk.
   * @throws {OFNCAL.TimeSpecificationNotFoundError} Když data neobsahují
   *  časovou specifikaci.
   */
  // eslint-disable-next-line require-jsdoc, space-before-function-paren
  generate /* istanbul ignore next */ (data, langCode) {
    throw new Error('EventsGenerator is just an interface.');
  }

};

/**
 * Generátor kalendářových událostí ({@link OFNCAL.Event}).
 * @extends OFNCAL.EventsGenerator
 */
OFNCAL.BasicEventsGenerator =
class BasicEventsGenerator extends OFNCAL.EventsGenerator {

  // Stačí jsdoc z rozhraní
  // eslint-disable-next-line require-jsdoc
  generate(data, langCode) {
    const timeSpecList = this._findTimeSpecifications(data);

    if (timeSpecList.length == 0) {
      throw new OFNCAL.TimeSpecificationNotFoundError();
    }

    const events = [];
    for (const i of Object.keys(timeSpecList)) {
      const timeSpec = timeSpecList[i];

      const id = data.iri + '#' + langCode + i;
      const title = data['název'][langCode];
      let desc = '';
      if ('popis' in data) {
        desc = data['popis'][langCode];
      }
      let timeSpecDesc = '';
      if ('popis' in timeSpec) {
        timeSpecDesc = timeSpec['popis'][langCode];
      }

      // throw an error if langCode key does not exist
      if (title === undefined ||
          desc === undefined ||
          timeSpecDesc === undefined) {
        throw new OFNCAL.LanguageNotFoundError(langCode);
      }

      const timeSpecConverter = new OFNCAL.TimeSpecConverter();
      const newEvent = timeSpecConverter.convert(timeSpec, langCode);

      newEvent.id = id;
      newEvent.title = title;
      if (timeSpecDesc !== '') {
        newEvent.desc.push(timeSpecDesc);
      }
      if (desc !== '') {
        newEvent.desc.push(desc);
      }
      events.push(newEvent);
    }
    return events;
  }


  /**
   * Rekurzivně pojde JSON data a vrátí všechny časové specifikace.
   * @param {object} data - Data dle OFN.
   * @return {object[]} Seznam časových specifikací.
   * @private
   */
  _findTimeSpecifications(data) {
    return this._findTimeSpecificationsRec(data, []);
  }

  // eslint-disable-next-line require-jsdoc
  _findTimeSpecificationsRec(data, acc) {
    if (!(data instanceof Object) ||
       (!Array.isArray(data) && data.typ == undefined)) {
      // nothing to do
      return acc;
    }

    if (data.typ == 'Časová specifikace') {
      // found one
      acc.push(data);
      return acc;
    }

    // search subcomponents
    for (const key in data) {
      if (key == '@context' || key == 'typ' || key == 'iri') {
        continue;
      }
      this._findTimeSpecificationsRec(data[key], acc);
    }
    return acc;
  }

};
