/**
 * Generátor kalendářů z dat dle OFN.
 * Výslendé kalendáře jsou v konkrétním formátu (např. iCalendar).
 */
OFNCAL.OfnToCalGenerator = class OfnToCalGenerator {

  /**
   * Vytvoří nový generátor kalendářů.
   * @constructor
   * @param {OFNCAL.CalendarsGenerator} generator - Generátor kalendářů
   *  (objektů {@link OFNCAL.Calendar}).
   * @param {OFNCAL.CalendarExporter} exporter -
   *  Exportér {@link OFNCAL.Calendar} na kalendář konkrétního formátu.
   */
  constructor(generator, exporter) {
    this.generator = generator;
    this.exporter = exporter;
    this.langCodeValidator = new OFNCAL.LangCodeValidator();
    this.dataValidator = new OFNCAL.OfnDataValidator();
  }

  /**
   * Generuje kalendář (jeden, nebo více) z dat dle OFN.
   * @param {string} data - Data dle OFN ve formátu JSON-LD.
   * @param {string} langCode - Kód jazyka dle normy ISO 639-1
   *  ("cs", "en" apod.)
   * @param {boolean} asOneCalendar -
   *  True, pokud chceme všechny události součit do jednoho kalendáře.<br>
   *  False, pokud chceme zachovat rozčlenění danné strukturou
   *  vstupních dat -- seznam dat dle OFN.
   *
   * @return {Array<string>} Seznam vygenerovaných kalendářů.
   *
   * @throws {OFNCAL.InvalidLangCodeError} Pokud zadaný jazykový kód
   *  není platný (dle ISO 639-1).
   * @throws {SyntaxError} Pokud zadaná data nejsou validní JSON data.
   *  (Vyhazuje metoda JSON#parse)
   * @throws {OFNCAL.InvalidDataStructureError} Pokud struktura zadaných dat
   *  (částí využívaných pro generování kalendářových událostí)
   *  neodpovídá specifikaci OFN.
   * @throws {OFNCAL.LanguageNotFoundError} Když data neobsahují
   *  požadovaný jazyk.
   * @throws {OFNCAL.TimeSpecificationNotFoundError} Když data neobsahují
   *  časovou specifikaci.
   */
  generate(data, langCode, asOneCalendar) {
    // validátory vyhazují výjimky
    this.langCodeValidator.validate(langCode);
    const dataJson = JSON.parse(data);
    this.dataValidator.validate(dataJson);

    let calendars = this.generator.generate(dataJson, langCode);

    if (asOneCalendar) {
      const mergedEvents = calendars.reduce(
          (acc, cal) => acc.concat(cal.events),
          []
      );
      calendars = [new OFNCAL.Calendar(mergedEvents)];
    }

    return calendars.map((cal) => this.exporter.exportCalendar(cal));
  }

};

/**
 * Funkce pro generování kalendářů formátu iCalendar z dat dle OFN.
 * @see {@link OFNCAL.OfnToCalGenerator#generate}
 */
// eslint-disable-next-line require-jsdoc
OFNCAL.generate = (data, langCode, asOneCalendar) => {
  const ofnToICal = new OFNCAL.OfnToCalGenerator(
      new OFNCAL.BasicCalendarsGenerator(),
      new OFNCAL.ICalCalendarExporter()
  );

  return ofnToICal.generate(data, langCode, asOneCalendar);
};

