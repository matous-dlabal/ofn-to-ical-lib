/**
 * Error, který indikuje, že struktura dat neodpovídá specifikaci OFN.
 * @extends Error
 */
OFNCAL.InvalidDataStructureError =
class InvalidDataStructureError extends Error {

  // eslint-disable-next-line require-jsdoc
  constructor(msg) {
    super(msg || 'Invalid data structure for calendar events.');
    this.name = 'InvalidDataStructureError';
  }

};

/**
 * Error, který indikuje, že data neobsahují časovou specifikaci.
 * @extends Error
 */
OFNCAL.TimeSpecificationNotFoundError =
class TimeSpecificationNotFoundError extends Error {

  // eslint-disable-next-line require-jsdoc
  constructor() {
    super('Cannot find time specification ("časová specifikace").');
    this.name = 'TimeSpecificationNotFoundError';
  }

};

/**
 * Error, který indikuje, že jazykový kód není platná (dle ISO 639-1).
 * @extends Error
 */
OFNCAL.InvalidLangCodeError = class InvalidLangCodeError extends Error {

  /**
   * @param {string} langCode - Neplatný jazykový kód.
   */
  constructor(langCode) {
    super(`Language code "${langCode}" not conformant with ISO 639-1.`);
    this.name = 'InvalidLangCodeError';
  }

};

/**
 * Error, který indikuje, že data neobsahují zadaný jazykový kód.
 * @extends Error
 */
OFNCAL.LanguageNotFoundError = class LanguageNotFoundError extends Error {

  /**
   * @param {string} langCode - Nenalezený jazykový kód.
   */
  constructor(langCode) {
    super(`Language "${langCode}" not found in data.`);
    this.name = 'LanguageNotFoundError';
  }

};

/**
 * Error, který indikuje, že časová specifikace není platná.
 * @extends Error
 */
OFNCAL.InvalidTimeSpecificationError =
class InvalidTimeSpecificationError extends Error {

  // eslint-disable-next-line require-jsdoc
  constructor(msg) {
    super(msg || 'Invalid Time Specification');
    this.name = 'InvalidTimeSpecificationError';
  }

};
