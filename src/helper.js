/**
 * Zapouzdřuje třídy, konstanty a funkce knihovny.
 * @namespace OFNCAL
 */

/* istanbul ignore next */
if (typeof module === 'object') {
  OFNCAL = module.exports;
  // eslint-disable-next-line no-unused-vars
  const ICAL = require('ical.js');

} else if (typeof OFNCAL !== 'object') {
  // eslint-disable-next-line no-invalid-this
  this.OFNCAL = {};
}

OFNCAL.TimeSpecDescriptions = {};

