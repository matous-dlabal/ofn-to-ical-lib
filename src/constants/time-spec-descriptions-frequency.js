OFNCAL.TimeSpecDescriptions[OFNCAL.TimeSpec.FREQUENCY] =
{
  'en': {
    'HOURLY': 'The event repeats every hour.',
    'BIHOURLY': 'The event repeats every two hours.',
    'TRIHOURLY': 'The event repeats every three hours.', // každé 3 hodiny
    'DAILY_2': 'The event repeats twice a day.', // dvakrát denně
    'DAILY': 'The event repeats every day.',
    'WEEKLY_3': 'The event repeats three times a week.', // třikrát týdně
    'WEEKLY_2': 'The event repeats twice a week.', // dvakrát týdně
    'WEEKLY': 'The event repeats every week.',
    'MONTHLY_3': 'The event repeats three times a month.', // třikrát za měsíc
    'MONTHLY_2': 'The event repeats twice a month.', // dvakrát za měsíc
    'BIWEEKLY': 'The event repeats every other week.',
    'MONTHLY': 'The event repeats every month.',
    'BIMONTHLY': 'The event repeats every two month.',
    'QUARTERLY': 'The event repeats every quarter of a year.', // čtvrtletní
    'ANNUAL_3': 'The event repeats three times a year.', // třikrát do roka
    'ANNUAL_2': 'The event repeats twice a year.', // dvakrát do roka
    'ANNUAL': 'The event repeats every year.',
    'BIENNIAL': 'The event repeats every two years.', // dvouletý
    'TRIENNIAL': 'The event repeats every three years.', // tříletý
    'QUADRENNIAL': 'The event repeats every four years.', // čtyřletý
    'QUINQUENNIAL': 'The event repeats every five years.', // pětiletý
    'DECENNIAL': 'The event repeats every ten years.', // každých 10 let
    'BIDECENNIAL': 'The event repeats every 20 years', // každých 20 let
    'TRIDECENNIAL': 'The event repeats every 30 years.', // každých 30 let

    'CONT': 'The event repeats more than once a day.', // průběžný
    'IRREG': 'The event repeats with irregural frequency.', // nepravidelný
    'UPDATE_CONT': 'The event repeats without interruption.',
    'OTHER': 'The event repeats with unknown frequency.',
    'UNKNOWN': 'The event repeats with unknown frequency.',
    'NEVER': 'The event never repeats.',
  },
  'cs': {
    'HOURLY': 'Událost se opakuje každou hodinu.',
    'BIHOURLY': 'Událost se opakuje každé dvě hodiny.',
    'TRIHOURLY': 'Událost se opakuje každé tři hodiny.',
    'DAILY_2': 'Událost se opakuje dvakrát denně.',
    'DAILY': 'Událost se opakuje každý den.',
    'WEEKLY_3': 'Událost se opakuje třikát týdně',
    'WEEKLY_2': 'Událost se opakuje dvakrát týdně.',
    'WEEKLY': 'Událost se opakuje každý týden.',
    'MONTHLY_3': 'Událost se opakuje třikát za měsíc.',
    'MONTHLY_2': 'Událost se opakuje dvakrát za měsíc.',
    'BIWEEKLY': 'Událost se opakuje jednou za dva týdny.',
    'MONTHLY': 'Událost se opakuje každý měsíc.',
    'BIMONTHLY': 'Událost se opakuje jednou za dva měsíce.',
    'QUARTERLY': 'Událost se opakuje jednou za čtvrt roku.',
    'ANNUAL_3': 'Událost se opakuje třikrát za rok.',
    'ANNUAL_2': 'Událost se opakuje dvakrát za rok.',
    'ANNUAL': 'Událost se opakuje jednou za rok.',
    'BIENNIAL': 'Událost se opakuje jednou za dva roky.',
    'TRIENNIAL': 'Událost se opakuje jednou za tři roky.',
    'QUADRENNIAL': 'Událost se opakuje jednou za čtyři roky.',
    'QUINQUENNIAL': 'Událost se opakuje jednou za pět let.',
    'DECENNIAL': 'Událost se opakuje každých 10 let.',
    'BIDECENNIAL': 'Událost se opakuje každých 20 let.',
    'TRIDECENNIAL': 'Událost se opakuje každých 30 let.',

    'CONT': 'Událost se opakuje častěji než jednou za den.',
    'IRREG': 'Událost se opakuje s nepravidelnou frekvencí.',
    'UPDATE_CONT': 'Událost se koná bez přerušení.',
    'OTHER': 'Událost se opakuje s neznámou frekvencí.',
    'UNKNOWN': 'Událost se opakuje s neznámou frekvencí.',
    'NEVER': 'Událost se nikdy neopakuje.',
  },
};
