OFNCAL.TimeSpecDescriptions[OFNCAL.TimeSpec.SPECIFIC_FREQUENCY] =
{
  'en': {
    'týden_v_měsíci': (weeks) => {
      return `The event repeats every ${weeks.join('., ')}. week of month.`;
    },
    'rok_v_desetiletí': (years) => {
      return `The event repeats every ${years.join('., ')}. year of decade.`;
    },
    'rok_ve_století': (years) => {
      return `The event repeats every ${years.join('., ')}. year of century.`;
    },
  },
  'cs': {
    'týden_v_měsíci': (weeks) => {
      return `Událost se opakuje každý ${weeks.join('., ')}. týden měsíce.`;
    },
    'rok_v_desetiletí': (years) => {
      return `Událost se opakuje každý ${years.join('., ')}. rok desetiletí.`;
    },
    'rok_ve_století': (years) => {
      return `Událost se opakuje každý ${years.join('., ')}. rok století.`;
    },
  },
};
