OFNCAL.TimeSpecDescriptions[OFNCAL.TimeSpec.TIME_PERIOD] =
{
  'en': {
    'YEAR': 'The event lasts a year.',
    'YEAR_HALF': 'The event lasts half a year.',
    'QUARTER': 'The event lasts quarter of a year.',
    'MONTH': 'The event lasts a month.',
    'WEEK': 'The event lasts a week.',
    'DAY': 'The event lasts 24 hours.',
    'CALENDAR_DAY': 'The event lasts a day (00:00 - 23:59).',
    'HOUR': 'The event lasts a hour.',
    'MIN': 'The event lasts a minute.',
    'SEC': 'The event lasts a second.',
    'WORKING_DAY': 'Event occures on working days.',
  },
  'cs': {
    'YEAR': 'Událost trvá rok.',
    'YEAR_HALF': 'Událost trvá půl roku.',
    'QUARTER': 'Událost trvá čtvrt roku.',
    'MONTH': 'Událost trvá měsíc.',
    'WEEK': 'Událost trvá týden.',
    'DAY': 'Událost trvá 24 hodin.',
    'CALENDAR_DAY': 'Událost trvá den (00:00 - 23:59).',
    'HOUR': 'Událost trvá hodinu.',
    'MIN': 'Událost trvá minutu.',
    'SEC': 'Událost trvá sekundu.',
    'WORKING_DAY': 'Událost se koná v pracovní dny.',
  },
};
