OFNCAL.TimeSpecDescriptions[OFNCAL.TimeSpec.NUMBER_OF_REPEATS] =
{
  'en': (n) => `The event takes place ${n} times during the indicated time.`,
  'cs': (n) => `Událost se uskuteční ${n}x během vyznačené doby.`,
};
