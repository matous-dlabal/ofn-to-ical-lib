/**
 * Třída obsahující konstanty, které jsou použíté v OFN Časová specifikace.
 */
OFNCAL.TimeSpec = class TimeSpec {

  /** Časový okamžik */
  static get MOMENT() {
    return 'časový_okamžik';
  }

  /** Časový interval */
  static get INTERVAL() {
    return 'časový_interval';
  }

  /** Den v týdnu */
  static get DAY_OF_WEEK() {
    return 'den_v_týdnu';
  }

  /** Časové období */
  static get TIME_PERIOD() {
    return 'časové_období';
  }

  /** Frekvence */
  static get FREQUENCY() {
    return 'frekvence';
  }

  /** Jiná časová specifikace */
  static get OTHER_TIME_SPECIFICATION() {
    return 'jiná_časová_specifikace';
  }

  /** počet opakování */
  static get NUMBER_OF_REPEATS() {
    return 'počet_opakování';
  }

  /** časová_doba */
  static get TIME_LAPSE() {
    return 'časová_doba';
  }

  /** Specifická frekvence */
  static get SPECIFIC_FREQUENCY() {
    return 'specifická_frekvence';
  }

};

/**
 * Třída obsahující konstanty, které jsou použíté v OFN Časový okamžik.
 */
OFNCAL.Moment = class Moment {

  /** Datum */
  static get DATE() {
    return 'datum';
  }

  /** Datum a čas */
  static get DATETIME() {
    return 'datum_a_čas';
  }

  /** Nespecifikovaný */
  static get UNSPECIFIED() {
    return 'nespecifikovaný';
  }

};

/**
 * Třída obsahující konstanty, které jsou použíté v OFN Časový interval.
 */
OFNCAL.Interval = class Interval {

  /** začátek */
  static get START() {
    return 'začátek';
  }

  /** konec */
  static get END() {
    return 'konec';
  }

};

/**
 * Třída obsahující konstanty, které jsou použíté v OFN Časová doba.
 */
OFNCAL.TimeLapse = class TimeLapse {

  /** od */
  static get FROM() {
    return 'od';
  }

  /** do */
  static get TO() {
    return 'do';
  }

  /** čas */
  static get TIME() {
    return 'čas';
  }

};

/**
 * Třída obsahující konstanty, které jsou použíté v OFN Specifická frekvence.
 */
OFNCAL.SpecificFrequency = class SpecificFrequency {

  /** minuta */
  static get MINUTE() {
    return 'minuta';
  }

  /** hodina */
  static get HOUR() {
    return 'hodina';
  }

  /** den_v_měsíci */
  static get DAY_OF_MONTH() {
    return 'den_v_měsíci';
  }

  /** týden_v_měsíci */
  static get WEEK_OF_MONTH() {
    return 'týden_v_měsíci';
  }

  /** týden_v_roce */
  static get WEEK_OF_YEAR() {
    return 'týden_v_roce';
  }

  /** rok_v_desetiletí */
  static get YEAR_OF_DECADE() {
    return 'rok_v_desetiletí';
  }

  /** rok_ve_století */
  static get YEAR_OF_CENTURY() {
    return 'rok_ve_století';
  }

};
