OFNCAL.TimeSpecDescriptions[OFNCAL.TimeSpec.OTHER_TIME_SPECIFICATION] =
{
  'en': {
    'dobré-počasí': 'Valid in good weather.',
    'špatné-počasí': 'Valid in bad weather.',
  },
  'cs': {
    'dobré-počasí': 'Platí za dobrého počasí.',
    'špatné-počasí': 'Platí za špatného počasí.',
  },
};
