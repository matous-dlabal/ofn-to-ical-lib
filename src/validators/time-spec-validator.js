/**
 * Validátor dat dle OFN Časová specifikace.
 * @extends OFNCAL.Validator
 */
OFNCAL.TimeSpecValidator =
class TimeSpecValidator extends OFNCAL.Validator {

  // eslint-disable-next-line require-jsdoc
  constructor() {
    super();
    this.momentValidator = new OFNCAL.MomentValidator();
    this.intervalValidator = new OFNCAL.IntervalValidator();
    this.daysOfWeekValidator = new OFNCAL.DaysOfWeekValidator();
    this.timePeriodsValidator = new OFNCAL.TimePeriodsValidator();
    this.frequenciesValidator = new OFNCAL.FrequenciesValidator();
    this.otherTimeSpecsValidator = new OFNCAL.OtherTimeSpecsValidator();
    this.timeLapseValidator = new OFNCAL.TimeLapseValidator();
    this.specificFrequencyValidator = new OFNCAL.SpecificFrequencyValidator();
  }

  /**
   * Ověří, zda má časová specifikace validní strukturu.
   * @param {object} timeSpec - Časová specifikace k ověření.
   * @return {undefined}
   * @throw {OFNCAL.InvalidDataStructureError}
   *  Pokud časová specifikace není validní.
   */
  validate(timeSpec) {
    if (!OFNCAL.Validator.isObject(timeSpec)) {
      throw new OFNCAL.InvalidDataStructureError(
          'Časová specifikace not an object'
      );
    }

    if (timeSpec['typ'] === undefined) {
      throw new OFNCAL.InvalidDataStructureError(
          'Časová specifikace - key "typ" not found'
      );
    }
    if (timeSpec['typ'] !== 'Časová specifikace') {
      throw new OFNCAL.InvalidDataStructureError(
          'Časová specifikace - invalid key "typ"'
      );
    }

    if (OFNCAL.TimeSpec.MOMENT in timeSpec) {
      this.momentValidator.validate(timeSpec[OFNCAL.TimeSpec.MOMENT]);
    }
    if (OFNCAL.TimeSpec.INTERVAL in timeSpec) {
      this.intervalValidator.validate(timeSpec[OFNCAL.TimeSpec.INTERVAL]);
    }
    if (OFNCAL.TimeSpec.DAY_OF_WEEK in timeSpec) {
      this.daysOfWeekValidator.validate(timeSpec[OFNCAL.TimeSpec.DAY_OF_WEEK]);
    }
    if (OFNCAL.TimeSpec.TIME_PERIOD in timeSpec) {
      this.timePeriodsValidator.validate(timeSpec[OFNCAL.TimeSpec.TIME_PERIOD]);
    }
    if (OFNCAL.TimeSpec.FREQUENCY in timeSpec) {
      this.frequenciesValidator.validate(timeSpec[OFNCAL.TimeSpec.FREQUENCY]);
    }
    if (OFNCAL.TimeSpec.OTHER_TIME_SPECIFICATION in timeSpec) {
      this.otherTimeSpecsValidator.validate(
          timeSpec[OFNCAL.TimeSpec.OTHER_TIME_SPECIFICATION]
      );
    }
    if (OFNCAL.TimeSpec.TIME_LAPSE in timeSpec) {
      const timeLapseArray = timeSpec[OFNCAL.TimeSpec.TIME_LAPSE];
      if (!Array.isArray(timeLapseArray)) {
        throw new OFNCAL.InvalidDataStructureError(
            'Časová specifikace - key "časová_doba" is not an array'
        );
      }
      timeLapseArray.forEach((e) => this.timeLapseValidator.validate(e));
    }
    if (OFNCAL.TimeSpec.SPECIFIC_FREQUENCY in timeSpec) {
      const specFreqArray = timeSpec[OFNCAL.TimeSpec.SPECIFIC_FREQUENCY];
      if (!Array.isArray(specFreqArray)) {
        throw new OFNCAL.InvalidDataStructureError(
            'Časová specifikace - key "specifická_frekvence" is not an array'
        );
      }
      specFreqArray.forEach((e) => this.specificFrequencyValidator.validate(e));
    }
    if (OFNCAL.TimeSpec.NUMBER_OF_REPEATS in timeSpec) {
      if (typeof timeSpec[OFNCAL.TimeSpec.NUMBER_OF_REPEATS] !== 'number') {
        throw new OFNCAL.InvalidDataStructureError(
            'Časová specifikace - key "počet_opakování" not a number'
        );
      }
    }

  }

};
