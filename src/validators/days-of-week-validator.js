/**
 * Validátor dat číselníku pro dny v týdnu.
 * @extends OFNCAL.VocabularyArrayValidator
 */
OFNCAL.DaysOfWeekValidator =
class DaysOfWeekValidator extends OFNCAL.VocabularyArrayValidator {

  // eslint-disable-next-line require-jsdoc
  constructor() {
    super(
        'Dny v týdnu',
        'https://data.mvcr.gov.cz/zdroj/číselníky/dny-v-týdnu/položky/'
    );
  }

};
