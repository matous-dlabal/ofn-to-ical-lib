/**
 * Validátor dat dle OFN.
 * @extends OFNCAL.Validator
 */
OFNCAL.OfnDataValidator = class OfnDataValidator extends OFNCAL.Validator {

  /**
   * Ověří, zda data obsahují potřebné informace pro tvorbu
   *  kalendářových událostí.
   * @param {object} data - Data k ověření.
   * @return {undefined}
   * @throw {OFNCAL.InvalidDataStructureError}
   *  Pokud data neobsahují potřebné informace.
   */
  validate(data) {
    if (Array.isArray(data)) {
      data.forEach((e) => {
        this._validateObject(e);
      });
      return;
    }
    this._validateObject(data);
  }

  /**
   * Validuje, že jsou předaná data objekt (ne array) a ten potom validuje.
   * @param {object} data - Data k ověření.
   * @return {undefined}
   * @throw {OFNCAL.InvalidDataStructureError}
   *  Pokud data neobsahují potřebné informace.
   * @private
   */
  _validateObject(data) {
    if (!OFNCAL.Validator.isObject(data)) {
      throw new OFNCAL.InvalidDataStructureError('data not of type object (or array of objects)');
    }

    if (data['typ'] === undefined) {
      throw new OFNCAL.InvalidDataStructureError('key "typ" not found');
    }
    if (typeof data['typ'] !== 'string') {
      throw new OFNCAL.InvalidDataStructureError('key "typ" not of type string');
    }

    if (data['iri'] === undefined) {
      throw new OFNCAL.InvalidDataStructureError('key "iri" not found');
    }
    if (typeof data['iri'] !== 'string') {
      throw new OFNCAL.InvalidDataStructureError('key "iri" not of type string');
    }

    if (data['název'] === undefined) {
      throw new OFNCAL.InvalidDataStructureError('key "název" not found');
    }
    if (!OFNCAL.Validator.isObject(data['název'])) {
      throw new OFNCAL.InvalidDataStructureError('key "název" not of type object');
    }

    if (data['popis'] !== undefined) {
      if (!OFNCAL.Validator.isObject(data['popis'])) {
        throw new OFNCAL.InvalidDataStructureError('key "popis" not of type object');
      }
    }
  }

};
