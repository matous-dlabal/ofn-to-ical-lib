/**
 * Validátor dat číselníku time period.
 * @extends OFNCAL.VocabularyArrayValidator
 */
OFNCAL.TimePeriodsValidator =
class TimePeriodsValidator extends OFNCAL.VocabularyArrayValidator {

  // eslint-disable-next-line require-jsdoc
  constructor() {
    super(
        'Časové období',
        'http://publications.europa.eu/resource/authority/timeperiod/'
    );
  }

};
