/**
 * Validátor dat číselníku frequency.
 * @extends OFNCAL.VocabularyArrayValidator
 */
OFNCAL.FrequenciesValidator =
class FrequenciesValidator extends OFNCAL.VocabularyArrayValidator {

  // eslint-disable-next-line require-jsdoc
  constructor() {
    super(
        'Frekvence',
        'http://publications.europa.eu/resource/authority/frequency/'
    );
  }

};
