/**
 * Validátor dat číselníku pro jinou časovou specifikaci.
 * @extends OFNCAL.VocabularyArrayValidator
 */
OFNCAL.OtherTimeSpecsValidator =
class OtherTimeSpecsValidator extends OFNCAL.VocabularyArrayValidator {

  // eslint-disable-next-line require-jsdoc
  constructor() {
    super(
        'Jiná časová specifikace',
        'https://data.mvcr.gov.cz/zdroj/číselníky/jiná-časová-specifikace/položky/'
    );
  }

};
