/**
 * Validátor dat dle OFN Časový okamžik.
 * @extends OFNCAL.Validator
 */
OFNCAL.MomentValidator =
class MomentValidator extends OFNCAL.Validator {

  /**
   * Ověří, zda má časový okamžik validní strukturu.
   * @param {object} moment - Časový okamžik k ověření.
   * @return {undefined}
   * @throw {OFNCAL.InvalidDataStructureError}
   *  Pokud okamžik není validní.
   */
  validate(moment) {
    if (!OFNCAL.Validator.isObject(moment)) {
      throw new OFNCAL.InvalidDataStructureError(
          'Časový okamžik not an object'
      );
    }

    if (moment['typ'] === undefined) {
      throw new OFNCAL.InvalidDataStructureError(
          'Časový okamžik - key "typ" not found'
      );
    }
    if (moment['typ'] !== 'Časový okamžik') {
      throw new OFNCAL.InvalidDataStructureError(
          'Časový okamžik - invalid key "typ"'
      );
    }

    if (OFNCAL.Moment.DATE in moment) {
      if (typeof moment[OFNCAL.Moment.DATE] !== 'string') {
        throw new OFNCAL.InvalidDataStructureError(
            'Časový okamžik - key "datum" not a string'
        );
      }
      if (OFNCAL.Moment.DATETIME in moment) {
        throw new OFNCAL.InvalidDataStructureError(
            'Časový okamžik - both keys "datum" and "datum_a_čas" in one moment'
        );
      }
      if (OFNCAL.Moment.UNSPECIFIED in moment) {
        throw new OFNCAL.InvalidDataStructureError(
            'Časový okamžik - both keys "datum" and "nespecifikovaný" in one moment'
        );
      }
      return;
    }
    if (OFNCAL.Moment.DATETIME in moment) {
      if (typeof moment[OFNCAL.Moment.DATETIME] !== 'string') {
        throw new OFNCAL.InvalidDataStructureError(
            'Časový okamžik - key "datum_a_čas" not a string'
        );
      }
      if (OFNCAL.Moment.UNSPECIFIED in moment) {
        throw new OFNCAL.InvalidDataStructureError(
            'Časový okamžik - both keys "datum_a_čas" and "nespecifikovaný" in one moment'
        );
      }
      return;
    }
    if (OFNCAL.Moment.UNSPECIFIED in moment) {
      if (typeof moment[OFNCAL.Moment.UNSPECIFIED] !== 'boolean') {
        throw new OFNCAL.InvalidDataStructureError(
            'Časový okamžik - key "nespecifikovaný" not a boolean'
        );
      }
      return;
    }
    throw new OFNCAL.InvalidDataStructureError(
        'Časový okamžik - missing key "datum" or "datum_a_čas" or "nespecifikovaný"'
    );
  }

};
