/**
 * Validátor dat dle OFN Časový interval.
 * @extends OFNCAL.Validator
 */
OFNCAL.IntervalValidator =
class IntervalValidator extends OFNCAL.Validator {

  // eslint-disable-next-line require-jsdoc
  constructor() {
    super();
    this.momentValidator = new OFNCAL.MomentValidator();
  }

  /**
   * Ověří, zda má časový interval validní strukturu.
   * @param {object} interval - Časový interval k ověření.
   * @return {undefined}
   * @throw {OFNCAL.InvalidDataStructureError}
   *  Pokud interval není validní.
   */
  validate(interval) {
    if (!OFNCAL.Validator.isObject(interval)) {
      throw new OFNCAL.InvalidDataStructureError(
          'Časový interval not an object'
      );
    }

    if (interval['typ'] === undefined) {
      throw new OFNCAL.InvalidDataStructureError(
          'Časový interval - key "typ" not found'
      );
    }
    if (interval['typ'] !== 'Časový interval') {
      throw new OFNCAL.InvalidDataStructureError(
          'Časový interval - invalid key "typ"'
      );
    }

    if (OFNCAL.Interval.START in interval) {
      this.momentValidator.validate(interval[OFNCAL.Interval.START]);
    }
    if (OFNCAL.Interval.END in interval) {
      this.momentValidator.validate(interval[OFNCAL.Interval.END]);
    }

    if (!(OFNCAL.Interval.START in interval) && !(OFNCAL.Interval.END in interval)) {
      throw new OFNCAL.InvalidDataStructureError(
          'Časový interval - missing key "začátek" or "konec"'
      );
    }

  }

};
