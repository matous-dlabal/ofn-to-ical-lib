/**
 * Rozhraní pro validování dat.
 * @interface
 */
OFNCAL.Validator = class Validator {

  /**
   * Validuje data.<br>
   * @param {*} data - Data k validaci.
   * @return {undefined}
   * @throw {Error} Pokud data nejsou validní.
   */
  // eslint-disable-next-line require-jsdoc, space-before-function-paren
  validate /* istanbul ignore next */ (data) {
    throw new Error('Validator is just an interface.');
  }

  /**
   * Ověří, zda je předaný parametr nenulový objekt, který není pole.
   * @param {*} obj - parametr ke kontrole
   * @return {boolean}
   *  False, pokud parametr není objekt, nebo je parametr null, nebo pole.
   */
  static isObject(obj) {
    return typeof obj === 'object' && !Array.isArray(obj) && obj !== null;
  }

};
