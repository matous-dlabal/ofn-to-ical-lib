/**
 * Validátor dat typu časová doba.
 * @extends OFNCAL.Validator
 */
OFNCAL.TimeLapseValidator =
class TimeLapseValidator extends OFNCAL.Validator {

  /**
   * Ověří, zda má časová doba validní strukturu.
   * @param {object} timeLapse - Časová doba k ověření.
   * @return {undefined}
   * @throw {OFNCAL.InvalidDataStructureError}
   *  Pokud časová doba není validní.
   */
  validate(timeLapse) {
    if (!OFNCAL.Validator.isObject(timeLapse)) {
      throw new OFNCAL.InvalidDataStructureError(
          'Časová doba is not an object'
      );
    }

    if (timeLapse['typ'] === undefined) {
      throw new OFNCAL.InvalidDataStructureError(
          'Časová doba - key "typ" not found'
      );
    }
    if (timeLapse['typ'] !== 'Časová doba') {
      throw new OFNCAL.InvalidDataStructureError(
          'Časová doba - invalid key "typ"'
      );
    }

    if (OFNCAL.TimeLapse.FROM in timeLapse &&
        OFNCAL.TimeLapse.TO in timeLapse &&
        !(OFNCAL.TimeLapse.TIME in timeLapse)) {

      if (typeof timeLapse[OFNCAL.TimeLapse.FROM] !== 'string') {
        throw new OFNCAL.InvalidDataStructureError(
            'Časová doba - key "od" not a string'
        );
      }
      if (typeof timeLapse[OFNCAL.TimeLapse.TO] !== 'string') {
        throw new OFNCAL.InvalidDataStructureError(
            'Časová doba - key "do" not a string'
        );
      }
      return;
    }
    if (OFNCAL.TimeLapse.TIME in timeLapse &&
        !(OFNCAL.TimeLapse.FROM in timeLapse) &&
        !(OFNCAL.TimeLapse.TO in timeLapse)) {

      if (typeof timeLapse[OFNCAL.TimeLapse.TIME] !== 'string') {
        throw new OFNCAL.InvalidDataStructureError(
            'Časová doba - key "čas" not a string'
        );
      }
      return;
    }

    if (OFNCAL.TimeLapse.TIME in timeLapse &&
     (OFNCAL.TimeLapse.FROM in timeLapse || OFNCAL.TimeLapse.TO in timeLapse)) {
      throw new OFNCAL.InvalidDataStructureError(
          'Časová doba - combining key "čas" with keys "od" or "do"'
      );
    }

    throw new OFNCAL.InvalidDataStructureError(
        'Časová doba - missing keys "čas" or "od" or "do"'
    );
  }

};
