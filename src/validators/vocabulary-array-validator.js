/**
 * Validátor pole dat číselníku.
 * @extends OFNCAL.Validator
 */
OFNCAL.VocabularyArrayValidator =
class VocabularyArrayValidator extends OFNCAL.Validator {

  /**
   * @param {string} name - Názav číselníku pro zprávu erroru.
   * @param {string} valuePrefix - Prefix každé hodnoty číselníku.
   */
  constructor(name, valuePrefix) {
    super();
    this.name = name;
    this.valuePrefix = valuePrefix;
  }

  /**
   * Ověří, zda jde o seznam textových řetězců, které mohou být z číselníku.
   * Nekontroluje jednotlivé hodnoty, kontroluje prefix.
   * @param {Array<string>} valueArray - Seznam hodnot číselníku.
   * @return {undefined}
   * @throw {OFNCAL.InvalidDataStructureError}
   *  Pokud nejde o pole hodnot číselníku.
   */
  validate(valueArray) {
    if (!Array.isArray(valueArray)) {
      throw new OFNCAL.InvalidDataStructureError(
          `${this.name} - not an array`
      );
    }
    if (!valueArray.every((day) => typeof day === 'string')) {
      throw new OFNCAL.InvalidDataStructureError(
          `${this.name} - value not a string`
      );
    }
    if (!valueArray.every((day) => day.startsWith(this.valuePrefix))) {
      throw new OFNCAL.InvalidDataStructureError(
          `${this.name} - value does not start with "${this.valuePrefix}"`
      );
    }
  }

};
