/**
 * Validátor dat typu specifická frekvence.
 * @extends OFNCAL.Validator
 */
OFNCAL.SpecificFrequencyValidator =
class SpecificFrequencyValidator extends OFNCAL.Validator {

  /**
   * Ověří, zda má specifická frekvence validní strukturu.
   * @param {object} specificFreq - Specifická frekvence k ověření.
   * @return {undefined}
   * @throw {OFNCAL.InvalidDataStructureError}
   *  Pokud specifická frekvence není validní.
   */
  validate(specificFreq) {
    if (!OFNCAL.Validator.isObject(specificFreq)) {
      throw new OFNCAL.InvalidDataStructureError(
          'Specifická frekvence is not an object'
      );
    }

    if (specificFreq['typ'] === undefined) {
      throw new OFNCAL.InvalidDataStructureError(
          'Specifická frekvence - key "typ" not found'
      );
    }
    if (specificFreq['typ'] !== 'Specifická frekvence') {
      throw new OFNCAL.InvalidDataStructureError(
          'Specifická frekvence - invalid key "typ"'
      );
    }

    if (OFNCAL.SpecificFrequency.MINUTE in specificFreq) {
      if (typeof specificFreq[OFNCAL.SpecificFrequency.MINUTE] !== 'number') {
        throw new OFNCAL.InvalidDataStructureError(
            'Specifická frekvence - key "minuta" is not a number'
        );
      }
    }
    if (OFNCAL.SpecificFrequency.HOUR in specificFreq) {
      if (typeof specificFreq[OFNCAL.SpecificFrequency.HOUR] !== 'number') {
        throw new OFNCAL.InvalidDataStructureError(
            'Specifická frekvence - key "hodina" is not a number'
        );
      }
    }
    if (OFNCAL.SpecificFrequency.DAY_OF_MONTH in specificFreq) {
      if (typeof specificFreq[OFNCAL.SpecificFrequency.DAY_OF_MONTH] !== 'number') {
        throw new OFNCAL.InvalidDataStructureError(
            'Specifická frekvence - key "den_v_měsíci" is not a number'
        );
      }
    }
    if (OFNCAL.SpecificFrequency.WEEK_OF_MONTH in specificFreq) {
      if (typeof specificFreq[OFNCAL.SpecificFrequency.WEEK_OF_MONTH] !== 'number') {
        throw new OFNCAL.InvalidDataStructureError(
            'Specifická frekvence - key "týden_v_měsíci" is not a number'
        );
      }
    }
    if (OFNCAL.SpecificFrequency.WEEK_OF_YEAR in specificFreq) {
      if (typeof specificFreq[OFNCAL.SpecificFrequency.WEEK_OF_YEAR] !== 'number') {
        throw new OFNCAL.InvalidDataStructureError(
            'Specifická frekvence - key "týden_v_roce" is not a number'
        );
      }
    }
    if (OFNCAL.SpecificFrequency.YEAR_OF_DECADE in specificFreq) {
      if (typeof specificFreq[OFNCAL.SpecificFrequency.YEAR_OF_DECADE] !== 'number') {
        throw new OFNCAL.InvalidDataStructureError(
            'Specifická frekvence - key "rok_v_desetiletí" is not a number'
        );
      }
    }
    if (OFNCAL.SpecificFrequency.YEAR_OF_CENTURY in specificFreq) {
      if (typeof specificFreq[OFNCAL.SpecificFrequency.YEAR_OF_CENTURY] !== 'number') {
        throw new OFNCAL.InvalidDataStructureError(
            'Specifická frekvence - key "rok_ve_století" is not a number'
        );
      }
    }
  }

};
