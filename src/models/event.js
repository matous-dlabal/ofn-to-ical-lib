/**
 * Třída reprezentující kalendářovou událost.
 */
OFNCAL.Event = class Event {

  /**
   * Vytváří novou kalendářovou událost.
   */
  constructor() {
    /*
     * Unikátní ID události
     * @type {string}
     */
    this.id;
    /**
     * Název události
     * @type {string}
     */
    this.title;
    /**
     * Počátek události
     * @type {ICAL.Time}
     */
    this.start;
    /**
     * Konec události
     * @type {ICAL.Time}
     */
    this.end;
    /**
     * Popisy události.
     * @type {Array<string>}
     */
    this.desc = [];
    /**
     * Informace o opakování události.
     * @type {object}
     */
    this.recur;
  };

  /**
   * Popis události.
   * Prvky {@link OFNCAL.Event#desc} oddělené novým řádkem.
   * @type {string}
   */
  get description() {
    return this.desc.join('\n');
  }

};
