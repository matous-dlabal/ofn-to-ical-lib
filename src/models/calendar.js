/**
 * Třída reprezentující kalendář.
 * Drží seznam událostí ({@link OFNCAL.Event}).
 */
OFNCAL.Calendar = class Calendar {

  /**
   * Vytváří nový kalendář.
   * @constructor
   * @param {OFNCAL.Event[]} events - Události, ze kterých je kalendář složený.
   */
  constructor(events = []) {
    this.events = events;
  }

};
