# Generování událostí iCal z dat dle OFN - knihovna

Knihovna pro generování událostí ve formátu iCalendar z dat dle OFN.
Součást bakalářské práce
*Generování kalendářových událostí z dat dle Otevřených formálních norem*
vytvořené v rámci studia
na [Fakultě Informačních Technologií ČVUT](https://fit.cvut.cz).

## Vyzkoušení funkcí knihovny

V rámci práce byla vytvořena
i [webová stránka](https://matous-dlabal.gitlab.io/ofn-to-ical-web/),
která prezentuje funkce knihovny.

## Vygenerované artefakty

Sestavené soubory knihovny (složka `build`), JSDoc dokumentace
a report pokrytí kódu testy se generují v rámci CI pro každou větev.

[JSDoc](https://matous-dlabal.gitlab.io/ofn-to-ical-lib/doc/) a
[Coverage report](https://matous-dlabal.gitlab.io/ofn-to-ical-lib/coverage/),
vygenerované z aktuální verze na master větvi,
jsou dostupné online v rámci GitLab Pages tohoto repozitáře.

Sestavené soubory knihovny jsou dostupné ke stažení na GitLab Pages
[ofn-to-ical-lib.js](https://matous-dlabal.gitlab.io/ofn-to-ical-lib/build/ofn-to-ical-lib.js)
a
[ofn-to-ical-lib.min.js](https://matous-dlabal.gitlab.io/ofn-to-ical-lib/build/ofn-to-ical-lib.min.js).


## Dokumentace

Programátorská dokumentace se nachází v textu práce v kapitole
[7.1.1](https://matous-dlabal.gitlab.io/ofn-to-ical-web/bakalarska-prace-matous-dlabal.pdf#51).

