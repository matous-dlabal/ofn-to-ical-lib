const srcFilesList = require('./src-files-list.json');

module.exports = function(grunt) {
  grunt.initConfig({
    concat: {
      build: {
        src: srcFilesList,
        dest: 'build/script-concat.js',
      },
      add_license_concat: {
        options: {
          banner: '/*@license\n',
          separator: '*/\n',
        },
        src: ['LICENSE', 'build/script-concat.js'],
        dest: 'build/script-concat.js',
      },
      add_license_babel: {
        options: {
          banner: '/*@license\n',
          separator: '*/\n',
        },
        src: ['LICENSE', 'build/ofn-to-ical-lib.js'],
        dest: 'build/ofn-to-ical-lib.js',
      },
    },
    uglify: {
      options: {
        sourceMap: true,
        compress: {},
        output: {
          comments: 'some',
        },
      },
      dist: {
        files: {
          'build/ofn-to-ical-lib.min.js': ['build/ofn-to-ical-lib.js'],
        },
      },
    },
    jsdoc: {
      dist: {
        src: ['src/**/*.js', 'test/**/*.js', 'README.md'],
        options: {
          destination: 'doc',
        },
      },
    },
    eslint: {
      src: {
        options: {
          fix: grunt.option('fix'), // this will get params from the flags
        },
        src: ['src/**/*.js', 'Gruntfile.js'],
      },
      tests: {
        options: {
          fix: grunt.option('fix'), // this will get params from the flags
          rules: {
            'max-len': 'off',
          },
        },
        src: ['test/**/*.js'],
      },

      all: {
        options: {
          fix: grunt.option('fix'), // this will get params from the flags
        },
        src: ['src/**/*.js', 'test/**/*.js', 'Gruntfile.js'],
      },
      all_without_maxlen: {
        options: {
          fix: grunt.option('fix'), // this will get params from the flags
          rules: {
            'max-len': 'off',
          },
        },
        src: ['src/**/*.js', 'test/**/*.js', 'Gruntfile.js'],
      },
    },
    mochaTest: {
      test: {
        options: {
          reporter: 'spec',
          require: 'test/helper.js',
        },
        src: ['test/**/*.test.js'],
      },
    },
    mocha_istanbul: {
      coverage: {
        src: ['test/**/*.test.js'],
        options: {
          root: 'src/',
          require: ['test/helper.js'],
          reporter: 'dot',
        },
      },
    },
    babel: {
      options: {
        sourceMap: true,
        presets: ['@babel/preset-env'],
        sourceType: 'script',
      },
      dist: {
        files: {
          'build/ofn-to-ical-lib.js': 'build/script-concat.js',
        },
      },
    },
  });

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-jsdoc');
  grunt.loadNpmTasks('grunt-eslint');
  grunt.loadNpmTasks('grunt-mocha-test');
  grunt.loadNpmTasks('grunt-mocha-istanbul');
  grunt.loadNpmTasks('grunt-babel');

  grunt.registerTask('default', ['all']);
  grunt.registerTask('all', ['build', 'lint', 'doc', 'coverage']);

  grunt.registerTask('build', ['concat:build', 'babel',
    'concat:add_license_concat', 'concat:add_license_babel', 'uglify']);
  grunt.registerTask('lint', ['eslint:src', 'eslint:tests']);
  grunt.registerTask('lintwml', ['eslint:all_without_maxlen']);
  grunt.registerTask('doc', ['jsdoc']);
  grunt.registerTask('test', ['mochaTest']);
  grunt.registerTask('coverage', ['mocha_istanbul']);
};
